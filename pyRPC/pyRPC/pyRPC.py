#-*- coding:latin1-*-
from __future__ import print_function
import socket
from time import sleep
from threading import Thread, Lock, Event
from thread import start_new_thread
from Queue import Queue, Empty
from re import match
from PhantomNode import PhantomNode


class RPCServer(Thread):
    """
    Servidor RPC para conexión con OMNET++
    """
    def __init__(self, IP_ADDR_IN, IP_ADDR_OUT, buflen = 10, debug = False):
        """

        """
        Thread.__init__(self)
        self.__PhantomNodes = {}
        self.__Tx_Lock = Lock()
        self.__BufIN = Queue(buflen)

        self.__SSocket_IN  = None
        self.__SIP_ADDR_IN = IP_ADDR_IN
        self.__CSocket_IN = None
        self.__CIP_ADDR_IN = None

        self.__SSocket_OUT = None
        self.__SIP_ADDR_OUT = IP_ADDR_OUT
        self.__CSocket_OUT = None
        self.__CIP_ADDR_OUT = None

        if debug: self.__debug = print
        else:     self.__debug = lambda *arg: None

        try:
            self.__debug("pyRPC Service init... ")
            self.__SSocket_IN  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.__SSocket_OUT = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.__SSocket_IN.bind(self.__SIP_ADDR_IN)
            self.__SSocket_OUT.bind(self.__SIP_ADDR_OUT)
            self.__debug("OK")
        except Exception, e:
            self.__debug("Error:",e.message)
            raise e

    def Tx(self, data):
        """
        Método thread-safe para envío a OmNet++.
        NOTA: Es invocado desde los PhantomNodes,
        desde su hilo de ejecución.
        """
        self.__Tx_Lock.acquire()
        self.__CSocket_OUT.send(data)
        self.__debug("->", data)
        self.__Tx_Lock.release()

    def RegisterPhantomNode(self, PNode):
        """
        Añade un PhantomNode a la lista
        """
        try:
            if not match('^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$', PNode.GetIP()): raise Exception('Wrong IP Address: %s' % PNode.GetIP())
            if not issubclass(type(PNode),PhantomNode): raise Exception('Wrong type for PhantomNode instance for %s'%PNode.GetIP())
            PNode.SetTxHandler(self.Tx)
            self.__PhantomNodes[PNode.GetIP()] = PNode
        except Exception, e:
            self.__debug("Error:", e.message)
            raise e

    def __RPC_Thread_IN(self, lock, sync, end):
        """
        Conexión de socket de salida
        """
        lock.acquire()
        try:
            self.__SSocket_IN.listen(1)
            self.__CSocket_IN, self.__CIP_ADDR_IN = self.__SSocket_IN.accept()
        except: return
        finally: lock.release()
        sync.wait()
        # //////////////////////////////////////// SYNC_1
        # Atención a la cola de recepción
        self.__debug("Serving RX queue...")
        self.__CSocket_IN.settimeout(1)
        while not end.isSet():
            try:
                data = self.__CSocket_IN.recv(4096)
                if not data: break
                ip = data[:data.index(';')]
                self.__PhantomNodes[ip].Put(data[data.index(';') + 1:])
                self.__debug("<-", data)
            except socket.timeout: pass

        self.__CSocket_IN.close()
        sleep(1)
        lock.release()

    def __RPC_Thread_OUT(self, lock, sync, end):
        """
        Conexión de socket de entrada
        """
        lock.acquire()
        try:
            self.__SSocket_OUT.listen(1)
            self.__CSocket_OUT, self.__CIP_ADDR_OUT = self.__SSocket_OUT.accept()
        except: return
        finally: lock.release()

        end.wait()  # /////////////////////////////////// END
        self.__CSocket_OUT.close()
        sleep(1)
        lock.release()

    def LaunchRPC(self):
        """
        Lanza el servicio RPC
        """
        # Conexión de sockets de entrada y salida
        self.__debug("Waiting for OmNET++ ...")
        l_IN  = Lock()
        l_OUT = Lock()
        sync = Event()
        end  = Event()

        start_new_thread(RPCServer.__RPC_Thread_IN, (self, l_IN, sync, end))
        start_new_thread(RPCServer.__RPC_Thread_OUT, (self, l_OUT,sync, end))
        sleep(1)
        l_IN.acquire()
        l_OUT.acquire()

        if not self.__CIP_ADDR_IN or not self.__CIP_ADDR_OUT:
            raise Exception("Error. Socket connection problem")

        self.__debug("Connection IN established through %s" % str(self.__CIP_ADDR_IN))
        self.__debug("Connection OUT established through %s" % str(self.__CIP_ADDR_OUT))

        self.__debug("Expanding phantom nodes...")
        # Expansión de los hilos PhantomNode y sincronización
        for ip in self.__PhantomNodes.keys():
            self.__debug(ip, '...')
            self.__PhantomNodes[ip].Sync(sync)
            self.__PhantomNodes[ip].start()
            self.__debug("OK")

        sleep(1)
        sync.set() # //////////////////////////////////// SYNC1
        sync.clear()

        for ip in self.__PhantomNodes.keys():
            if self.__PhantomNodes[ip].isAlive():
                self.__PhantomNodes[ip].join()

        self.__debug("Stopping service...")
        end.set()
        l_IN.acquire()
        l_OUT.acquire()
        self.__debug("OK")
