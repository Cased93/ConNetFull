#-*- coding:latin1-*-
from __future__ import print_function
from threading import Thread
from Queue import Queue


class PhantomNode(Thread):
    """
    Clase que encapsula la parte de python de cada nodo fantasma.
    En esta parte, esta clase contiene la capa de aplicación del
    nodo, con lo cual tiene dos métodos virtuales imprescindibles:
    El proceso de aplicación del nodo (método run), y un callback
    de recepción de mensaje (método recv)
    """
    def __init__(self, IP, buflen=10, debug=False):
        Thread.__init__(self)
        self.__IP = IP
        self.__BufIN = Queue(10)
        self.__tx = None
        self.__csync = None

        if debug: self.__debug = print
        else:     self.__debug = lambda *arg: None

    def SetTxHandler(self, tx):
        self.__tx = tx

    def GetIP(self):
        return self.__IP

    def Sync(self, csync):
        self.__csync = csync

    def Tx(self, IP, data):
        self.__tx("%s;%s;%s"%(IP, self.__IP, data))

    def Put(self, data):
        self.__BufIN.put(data)

    def Rx(self):
        return self.__BufIN.get()

    def AppLevel(self):
        pass

    def run(self):
        self.__debug(self.__IP, "Waiting for synchronization...")
        self.__csync.wait()
        self.__debug(self.__IP, "Start")
        self.AppLevel()
        self.__debug(self.__IP, "End")
