#-*-coding:latin1-*-
from pyRPC import PhantomNode, RPCServer
from time import sleep
from random import randint

if __name__ == '__main__':

    RX__IP_ADDR = ('localhost', 3444)
    TX__IP_ADDR = ('localhost', 3445)

    class PN_Generator(PhantomNode):
        def __init__(self, *arg, **kw):
            PhantomNode.__init__(self, *arg, **kw)

        def DataPacket(self):
            """
            Prepara un paquete de dato aleatorio
            """
            return "%d" %randint(1, 1E10)

        def AppLevel(self):
            """
            Generador
            """
            for i in range(10):
                sleep(1)
                data = self.DataPacket()
                self.Tx('192.168.1.2', data )
                print "->",data
            print "End generator"
            sleep(1)


    class PN_Sink(PhantomNode):
        def __init__(self, *arg, **kw):
            PhantomNode.__init__(self, *arg, **kw)

        def AppLevel(self):
            """
            Sumidero
            """
            for i in range(10):
                data = self.Rx()
                print "<-", data
            print "End sink"
            sleep(1)


    # Capa de aplicación. La capa de aplicación de los nodos
    pngen = PN_Generator('192.168.1.1', debug=False)
    pnsink = PN_Sink('192.168.1.2', debug=False)
    RPC = RPCServer(RX__IP_ADDR, TX__IP_ADDR, debug=False)
    RPC.RegisterPhantomNode(pngen)
    RPC.RegisterPhantomNode(pnsink)
    RPC.LaunchRPC()