//
// Copyright (C) 2015 J.M. Castillo-Secilla
// Universidad de Córdoba (Spain)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include <algorithm>
#include "CommunicationGate.h"

namespace test{
    #include <sys/socket.h> // Needed for the socket functions
    #include <sys/types.h> // Needed for the socket functions
    #include <netdb.h>      // Needed for the socket functions
}

#include "INETDefs.h"

#include "ModuleAccess.h"
#include "INotifiable.h"
#include "NotifierConsts.h"


Define_Module(CommunicationGate);

void CommunicationGate::initialize()
{
    CommunicationGate port;
    port.send();
}

void CommunicationGate::handleMessage(cMessage *msg)
{
    error("NotificationBoard doesn't handle messages, it can be accessed via direct method calls");
}

void CommunicationGate::send()
{

    int status;
    struct test::addrinfo host_info;       // The struct that getaddrinfo() fills up with data.
    struct test::addrinfo *host_info_list; // Pointer to the to the linked list of host_info's.

    // The MAN page of getaddrinfo() states "All  the other fields in the structure pointed
    // to by hints must contain either 0 or a null pointer, as appropriate." When a struct
    // is created in c++, it will be given a block of memory. This memory is not nessesary
    // empty. Therefor we use the memset function to make sure all fields are NULL.
    memset(&host_info, 0, sizeof host_info);

    std::cout << "Setting up the structs..."  << std::endl;

    host_info.ai_family = AF_UNSPEC;     // IP version not specified. Can be both.
    host_info.ai_socktype = test::SOCK_STREAM; // Use SOCK_STREAM for TCP or SOCK_DGRAM for UDP.

    // Now fill up the linked list of host_info structs with google's address information.
    status = test::getaddrinfo("www.google.com", "80", &host_info, &host_info_list);
    // getaddrinfo returns 0 on succes, or some other value when an error occured.
    // (translated into human readable text by the gai_gai_strerror function).
    if (status != 0)  std::cout << "getaddrinfo error" << test::gai_strerror(status) ;

    std::cout << "Creating a socket..."  << std::endl;
    int socketfd ; // The socket descripter
    socketfd = test::socket(host_info_list->ai_family, host_info_list->ai_socktype,
                      host_info_list->ai_protocol);
    if (socketfd == -1)  std::cout << "socket error " ;


    std::cout << "Connect()ing..."  << std::endl;
    status = test::connect(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
    if (status == -1)  std::cout << "connect error" ;


    std::cout << "send()ing message..."  << std::endl;
    char *msg = "GET / HTTP/1.1\nhost: www.google.com\n\n";
    int len;
    ssize_t bytes_sent;
    len = strlen(msg);
    bytes_sent = test::send(socketfd, msg, len, 0);

    std::cout << "Waiting to receive data..."  << std::endl;
    ssize_t bytes_received;
    char incomming_data_buffer[1000];
    bytes_received = test::recv(socketfd, incomming_data_buffer,1000, 0);
    // If no data arrives, the program will just wait here until some data arrives.
    if (bytes_received == 0) std::cout << "host shut down." << std::endl ;
    if (bytes_received == -1)std::cout << "receive error!" << std::endl ;
    std::cout << bytes_received << " bytes received :" << std::endl ;
    incomming_data_buffer[bytes_received] = '\0' ;
    std::cout << incomming_data_buffer << std::endl;
    std::cout << "Receiving complete. Closing socket..." << std::endl;
    freeaddrinfo(host_info_list);
    close(socketfd);
}



