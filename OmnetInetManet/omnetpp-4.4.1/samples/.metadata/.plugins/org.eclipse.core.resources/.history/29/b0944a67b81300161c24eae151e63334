
//
// Copyright (C) 2016 J.M. Castillo-Secilla
// Universidad de Córdoba (Spain)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
#include "gatewayNode.h"
#include <string>

#define PORT 8051
#define SERVER "127.0.0.1"

// The module class needs to be registered with OMNeT++
Define_Module(GatewayNode);
using namespace std;
using namespace test;

int sockfTX,sockfRX, portno, n;
struct sockaddr_in serv_addr1, serv_addr2;
struct hostent *server1, *server2;
char buffer1[1024], buffer2[1024];

int openTCPSocketWithpyRPC(sockaddr_in &serv_addr, hostent *(&server),int port, char * servIP);

void GatewayNode::initialize()
{
    // Initialize is called at the beginning of the simulation.
    // To bootstrap the tic-toc-tic-toc process, one of the modules needs
    // to send the first message. Let this be `tic'.

    // Am I Tic or Toc?
    if (strcmp("mainnode", getName()) == 0)
    {
        // create and send first message on gate "out". "tictocMsg" is an
        // arbitrary string which will be the name of the message object.
        cMessage *msg = new cMessage("tictocMsg");
        send(msg, "out");
        sockfTX = openTCPSocketWithpyRPC(serv_addr1, server1,3444,"127.0.0.1");
        sockfRX = openTCPSocketWithpyRPC(serv_addr2, server2,3445,"127.0.0.1");
    }
}

void GatewayNode::handleMessage(cMessage *msg)
{
    // The handleMessage() method is called whenever a message arrives
    // at the module. Here, we just send it to the other module, through
    // gate `out'. Because both `tic' and `toc' does the same, the message
    // will bounce between the two.
    if (strcmp(msg->getName(), "tictocMsg") != 0)
    {
        n = write(sockfTX, msg->getName(), strlen(msg->getName()));
        if (n < 0)
             error("ERROR writing to socket");
    }
    sendToWorld(msg);
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int openTCPSocketWithConNet(sockaddr_in &serv_addr, hostent *(&server), string name){

    int portno, sockfd, n;
    char buffer[1024];
    portno = PORT;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    server = gethostbyname(SERVER);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        error("ERROR connecting");
        return 0;
    }else
        cout << "Socket open successfully" << endl;
        string msgstring = "127.0.0.1,127.0.0.1,hello," + name;
        const char * msg = msgstring.c_str();
        n = write(sockfd, msg, strlen(msg));
        if (n < 0)
             error("ERROR writing to socket");
        bzero(buffer,1024);
        n = read(sockfd, buffer, 1024);
        if (n < 0)
             error("ERROR reading from socket");
        printf("%s\n", buffer);
        close(sockfd);

        string parsed,input = buffer;
        stringstream input_stringstream(input);
        vector<string> data;

        while(getline(input_stringstream,parsed,','))
        {
             data.push_back(parsed);
        }

        if (strcmp(data[2].c_str(),"hello") == 0){
            char buffer[1024];
            portno = atoi(data[5].c_str());
            sockfd = socket(AF_INET, SOCK_STREAM, 0);
            if (sockfd < 0)
                error("ERROR opening socket");
            server = gethostbyname(SERVER);
            if (server == NULL) {
                fprintf(stderr,"ERROR, no such host\n");
                exit(0);
            }
            bzero((char *) &serv_addr, sizeof(serv_addr));
            serv_addr.sin_family = AF_INET;
            bcopy((char *)server->h_addr,
                 (char *)&serv_addr.sin_addr.s_addr,
                 server->h_length);
            serv_addr.sin_port = htons(portno);
            if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
                error("ERROR connecting");
            else
                cout << "Now you are connect with ConNet system" << endl;
        }
}

int openTCPSocketWithpyRPC(sockaddr_in &serv_addr, hostent *(&server),int port, char * servIP){

    int portno, sockfd, n;
    char buffer[1024];
    portno = port;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    server = gethostbyname(servIP);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        error("ERROR connecting");
        return 0;
    }else{
        cout << "Socket open successfully" << endl;
        return sockfd;
    }

}

void GatewayNode::sendToWorld(cMessage *msg)
{

    bzero(buffer1,1024);
    n = read(sockfRX, buffer1, 1024);
    printf("%s\n", buffer1);

    cMessage *msg2 = new cMessage(buffer1);
    send(msg2, "out");


    /*
    char *msg = "127.0.0.1,127.0.0.1,Probando,Service Extern 2";
    n = write(sockfd, msg, strlen(msg));
    if (n < 0)
         error("ERROR writing to socket");
    bzero(buffer,1024);
    n = read(sockfd, buffer, 1024);
    if (n < 0)
         error("ERROR reading from socket");
    cout << buffer << endl;
    close(sockfd);*/
}


void GatewayNode::receiveFromWorld()
{
    std::cout << "Receiving..."  << std::endl;
}
