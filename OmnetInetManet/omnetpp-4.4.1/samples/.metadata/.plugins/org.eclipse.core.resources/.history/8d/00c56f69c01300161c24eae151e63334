//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 2003 Ahmet Sekercioglu
// Copyright (C) 2003-2008 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

package inet.examples.wpan.tictocCommunicationGate;

import inet.gatewaynodes.GatewayNode;
import inet.gatewaynodes.Node; 
import inet.gatewaynodes.PhantomNode; 



// Two instances (tic and toc) of Txc1 connected both ways.
// Tic and toc will pass messages to one another.
//
network Tictoc1
{
    submodules:
        gatewayNode: GatewayNode {
            @display("p=459,340");
        }
        phantomNode1: PhantomNode {
            parameters:
				ip = "192.168.1.1";
                @display("p=656,240");
        }
        phantomNode2: PhantomNode {
            @display("p=656,540");
        }
        node1: Node{
            @display("p=656,340");
        }
        node2: Node{
            @display("p=756,340");
        }
        node3: Node{
            @display("p=756,440");
        }
    connections:
        gatewayNode.outPN1 --> {  delay = 0ms; } --> phantomNode1.inGateway;
        gatewayNode.inPN1 <-- {  delay = 0ms; } <-- phantomNode1.outGateway;
        gatewayNode.outPN2 --> {  delay = 0ms; } --> phantomNode2.inGateway;
        gatewayNode.inPN2 <-- {  delay = 0ms; } <-- phantomNode2.outGateway;
        phantomNode1.outNetwork --> {  delay = 100ms; } --> node1.in1;
        phantomNode1.inNetwork <-- {  delay = 100ms; } <-- node1.out1;
        node1.out2 --> {  delay = 100ms; } --> node2.in1;
        node1.in2 <-- {  delay = 100ms; } <-- node2.out1;
        node2.out2 --> {  delay = 100ms; } --> node3.in1;
        node2.in2 <-- {  delay = 100ms; } <-- node3.out1;
        node3.out2 --> {  delay = 100ms; } --> phantomNode2.inNetwork;
        node3.in2 <-- {  delay = 100ms; } <-- phantomNode2.outNetwork;
}

