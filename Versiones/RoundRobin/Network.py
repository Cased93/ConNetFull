# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 18:46:04 2015

@author: cased93
"""

class Network:
    """
    Clase Network, representa el modelo de las redes del sistema con los métodos de acceso a sus parámetros.
    """
    
    def __init__(self,name,IP,inPort,outPort,receivedPackets = 0,sendedPackets = 0):
        """
        Constructor de la clase Network
        @param name: nombre de la red
        @type: str
        @param IP: IP origen de la red
        @type: str
        @param inPort: puerto de entrada de la red
        @type: int
        @param outPort: puerto de salida de la red
        @type: int
        @return: None
        """
        
        self.data = []
        self.name = name
        self.IP = IP
        self.inPort = inPort
        self.outPort = outPort
        self.receivedPackets = str(receivedPackets)
        self.sendedPackets = str(sendedPackets)
        
    def getName(self):
        """
        Getter del parametro name
        @return: parametro name
        """
        return self.name
        
    def getIP(self):
        """
        Getter del parametro IP
        @return: parametro IP
        """
        return self.IP
        
    def getInPort(self):
        """
        Getter del parametro inPort
        @return: parametro inPort
        """
        return self.inPort
        
    def getOutPort(self):
        """
        Getter del parametro outPort
        @return: parametro outPort
        """
        return self.outPort

    def getReceivedPackets(self):
        return self.receivedPackets

    def getSendedPackets(self):
        return self.sendedPackets

    def setName(self,name):
        """
        Setter del parametro name
        @return: None
        """
        self.name = name
            
    def setIP(self,IP):
        """
        Setter del parametro IP
        @return: None
        """
        self.IP = IP
        
    def setInPort(self, inPort):
        """
        Setter del parametro inPort
        @return: None
        """
        self.inPort = inPort
        
    def setOutPort(self, outPort):
        """
        Setter del parametro outPort
        @return: None
        """
        self.outPort = outPort

    def setReceivedPackets(self,receivedPackets):

        self.receivedPackets = receivedPackets

    def setSendedPackets(self,sendedPackets):

        self.sendedPackets = sendedPackets
        
    