# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 16:43:52 2015

@author: cased93
"""
import socket
import pickle
from Network import *
import random
from XMLDataBase import XMLDataBase


class Protocol:
    
    """
    Clase Protocol, encargada de gestionar el tanto la conexion por UDP como por TCP
    """  
    
    def __init__(self,typeOf, IPHost, inPort, outPort):
        
        """
        Constructor de la clase Protocol
        @param typeOf: tipo de conexion UDP/TCP
        @type: int
        @param IPHost: direccion IP del servidor
        @type: str
        @param inPort: puerto de entrada a conectar
        @type: int
        @param outPort: puerto de salida para enviar
        @type: int
        @return: None
        """
        
        self.data = []
        self.typeOf = typeOf
        self.IPHost = IPHost
        self.inPort = inPort 
        self.outPort = outPort
        
        
    def getTypeOf(self):
        """
        Getter del parametro typeOf
        @return: parametro typeOf
        """
        return self.typeOf
        
    def getInPort(self):
        """
        Getter del parametro inPort
        @return: parametro inPort
        """
        return self.inPort
        
    def getOutPort(self):
        """
        Getter del parametro outPort
        @return: parametro outPort
        """
        return self.outPort

    def setTypeOf(self,typeOf):
        """
        Setter del parametro typeOf
        @return: parametro None
        """
        self.typeOf = typeOf
        
    def setInPort(self, inport):
        """
        Setter del parametro inPort
        @return: parametro None
        """
        self.inPort = inport
        
    def setOutPort(self,outport):
        """
        Setter del parametro outPort
        @return: parametro None
        """
        self.outPort = outport
        
    def openMainConnection(self):
        """
        Función dedicada a abrir conexion con los puertos generales para las redes nuevas
        @return: None
        """
        if self.typeOf == 0: 
            self.sockExtern = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
            self.sockExtern.bind((self.IPHost, self.outPort))
        elif self.typeOf == 1:
            self.sockExtern = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
            self.sockExtern.bind((self.IPHost, self.outPort))
        
    def openConnection(self,newNetwork):
        """
        Función dedicada a abrir conexion con los puertos especificos de cada red
        @param newNetwork: red con la que abrir la conexion
        @type: Network
        @return: None
        """
        if self.typeOf == 0: 
            self.newSockExtern = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) 
            self.newSockExtern.bind((newNetwork.getIP(), int(newNetwork.getOutPort())))

        elif self.typeOf == 1:
            self.newSockExtern = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
            self.newSockExtern.bind((self.IPHost, self.outPort))
            self.newSockExtern.listen(1)
            self.newConnection, client_address2 = self.newSockExtern.accept()        
            
    def closeConnection(self):
        """
        Función para cerrar la conexión con sockets
        @return: None
        """
        self.sockExtern.close()
        
    def receiveFromMain(self):
        """
        Función que recibe desde el puerto de entrada del socket general para las redes nuevas
        @return: Mensaje entrante del puerto
        """
        if self.typeOf == 1:
            self.sockExtern.listen(10)
            self.connection, client_address = self.sockExtern.accept()
            return self.connection.recv(1024)
            
        elif self.typeOf == 0:
            return self.sockExtern.recvfrom(1024)
        
    def receiveFrom(self):
        """
        Función que recibe desde el puerto de entrada del socket especifico de cada red
        @return: Mensaje entrante del puerto
        """
        if self.typeOf == 1:
            return self.newConnection.recv(1024)
            
        elif self.typeOf == 0:
            return self.newSockExtern.recvfrom(1024)
        
    def sendFromQueue(self, newArray, newNetwork, IPDstn):
        """
        Función que envia a traves de la cola a la red destino del paquete
        @param newArray: array con el mensaje
        @type: array
        @param newNetwork: red origen con su informacion
        @type: Network
        @param IPDstn: IP destino del paquete
        @type: str
        @return: None
        """
        if self.typeOf == 1:
            self.newConnection.send(newArray)
            
        elif self.typeOf == 0:
            self.newSockExtern.sendto(newArray,(IPDstn,int(newNetwork.getInPort())))


    def sendToHello(self, newArray, generalPort):
        """
        Función que envia el paquete hello a las redes entrantes para asignarles puertos
        @param newArray: array con el mensaje
        @type: array
        @param generalPort: puerto general de la comunicación
        @type: int
        @return: None
        """
        if self.typeOf == 1:
            self.connection.send(newArray)
            
        elif self.typeOf == 0:
            self.sockExtern.sendto(newArray,(self.IPHost,generalPort))
        
    def generateInPort(self, portList):
        """
        Función que genera puertos de entrada libres aleatorios
        @param portList: lista de puertos en uso
        @type: list
        @return: None
        """
        
        inPort = random.randint(1024, 65535)
        while True:
            if inPort in portList:
                print("Generando otro puerto")
                inPort = random.randint(1024, 65535)
            else:
                return inPort
                
    def generateOutPort(self, portList):
        """
        Función que genera puertos de salida libres aleatorios
        @param portList: lista de puertos en uso
        @type: list
        @return: None
        """
        
        outPort = random.randint(1024, 65535)
        while True:
            if outPort in portList:
                print("Generando otro puerto")
                outPort = random.randint(1024, 65535)
            else:
                return outPort