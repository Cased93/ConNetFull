# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 19:46:32 2015

@author: cased93
"""
import numpy as np

class VectorParser:
    """
    Clase VectorParser, encargada de pasar las string de las peticiones a un vector
    """ 
    def __init__(self):
        """
        Constructor de la clase VectorParser
        @return: None
        """
        self.data = []
    
    def stringToVector(self,data):
        """
        Funcion que pasa la string entrante a un vector
        @param data: mensaje a convertir
        @type: str
        @return: vector con los datos
        """
        dataSplit = data.split(',')
        dataArray = np.asarray(dataSplit)
        
        return dataArray