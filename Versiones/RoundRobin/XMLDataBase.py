# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 19:58:37 2015

@author: cased93
"""
from lxml import etree
from Network import *

class XMLDataBase:
    """
    Clase XMLDataBase, encargada de gestionar el fichero XML que almacena las redes del sistema
    """  
    
    def __init__(self):
        """
        Constructor de la clase Protocol
        @return: None
        """
        self.data = []
        self.nombreFichero = "Networks.xml"
        self.networks=etree.Element("Networks")
        self.doc=etree.ElementTree(self.networks)        
        
        
        # Save to XML file
        try:
            outFile = open(self.nombreFichero , 'r')
            outFile.close()
        except:
            outFile = open(self.nombreFichero , 'w')
            self.doc.write(outFile, xml_declaration=True, encoding='utf-8') 
            outFile.close()


    def saveNetwork(self,newNetwork):
        
        """
        Función dedicada a guardar una nueva red en el sistema
        @param newNetwork: red nueva
        @type: Network
        @return: None
        """
        
        self.doc = etree.parse(self.nombreFichero)
        if self.doc.find('network') != None:
            element = self.doc.find('network')
            elementName = element.find('name')
            compare = elementName.get('name')

            if compare == newNetwork.getName():
                print "El elemento ya existe"
            else:
                idNet = str(self.getNumNetworks()+1)
                network = etree.SubElement(self.doc.getroot(),"network", name=idNet)
                ip = etree.Element('ip',name=newNetwork.getIP())
                name = etree.Element('name',name=newNetwork.getName())
                inport = etree.Element('inport',name=newNetwork.getInPort())
                outport = etree.Element('outport',name=newNetwork.getOutPort())
                receivedPackets = etree.Element('receivedpackets',name=newNetwork.getReceivedPackets())
                sendedPackets = etree.Element('sendedpackets',name=newNetwork.getSendedPackets())

                
                network.append(ip)
                network.append(name)
                network.append(inport)
                network.append(outport)
                network.append(receivedPackets)
                network.append(sendedPackets)
                outFile = open(self.nombreFichero , 'w')
                self.doc.write(outFile, xml_declaration=True, encoding='utf-8') 
                outFile.close()
                
        else:
            idNet = str(self.getNumNetworks()+1)
            network = etree.SubElement(self.doc.getroot(),"network", name=idNet)
            ip = etree.Element('ip',name=newNetwork.getIP())
            name = etree.Element('name',name=newNetwork.getName())
            inport = etree.Element('inport',name=newNetwork.getInPort())
            outport = etree.Element('outport',name=newNetwork.getOutPort())
            receivedPackets = etree.Element('receivedpackets',name=newNetwork.getReceivedPackets())
            sendedPackets = etree.Element('sendedpackets',name=newNetwork.getSendedPackets())


            
            network.append(ip)
            network.append(name)
            network.append(inport)
            network.append(outport)
            network.append(receivedPackets)
            network.append(sendedPackets)
            outFile = open(self.nombreFichero , 'w')
            self.doc.write(outFile, xml_declaration=True, encoding='utf-8') 
            outFile.close()
            
    def getAllNetworks(self):
        """
        Función dedicada a devolver todas las redes almacenadas
        @return: lista de redes almacenadas
        """
        
        self.doc = etree.parse(self.nombreFichero)
        networksList = list()
        
        for network in self.doc.getroot().getchildren():
            name = network.find('name').get('name')
            ip = network.find('ip').get('name')
            inport = network.find('inport').get('name')            
            outport = network.find('outport').get('name')
            receivedPackets = network.find('receivedpackets').get('name')
            sendedPackets = network.find('sendedpackets').get('name')
            networksList.append(Network(name,ip,inport,outport,receivedPackets,sendedPackets))
            
        return networksList
        
    def getNetworkByName(self, name):            
            """
            Función que devuelve la red con el nombre buscado si existe
            @param name: nombre de la red
            @type: str
            @return: red encontrada
            """
        
            allNetworks = self.getAllNetworks()

            for network in allNetworks:
                if network.getName() == name:
                    newNetwork = Network(network.getName(),network.getIP(),network.getInPort(),network.getOutPort(),network.getReceivedPackets(),network.getSendedPackets())
                    return newNetwork
                

    def getNumNetworks(self):
        """
        Función que devuelve el numero de redes almacenadas
        @return: numero de redes almacenadas
        """
        self.doc = etree.parse(self.nombreFichero)
        num = 0
        
        for network in self.doc.getroot().getchildren():
            num = num + 1
            
        return num
            
            
    def setNewPorts(self, network):
        """
        Función que modifica los puertos almacenados de una red para actualizarlos
        @param network: red con los puertos nuevos
        @type: Network
        @return: None
        """
        
        self.doc = etree.parse(self.nombreFichero)
        
        for network2 in self.doc.getroot().getchildren():
            name = network2.find('name').get('name')
            if network.getName() == name:
                inport = network2.find('inport').set('name',network.getInPort())            
                outport = network2.find('outport').set('name', network.getOutPort())       
        
        outFile = open(self.nombreFichero , 'w')                        
        self.doc.write(outFile, xml_declaration=True, encoding='utf-8') 
        outFile.close()

    def addReceivedSendedPackets(self, network,num,num2):
        """
        Función que modifica los puertos almacenados de una red para actualizarlos
        @param network: red con los puertos nuevos
        @type: Network
        @return: None
        """
        
        for network2 in self.doc.getroot().getchildren():
            name = network2.get('name')
            if str(network) == str(name):
                receivedPackets = network2.find('receivedpackets').set('name',str(num))
                sendedPackets = network2.find('sendedpackets').set('name',str(num2))


        outFile = open(self.nombreFichero , 'w')
        self.doc.write(outFile, xml_declaration=True, encoding='utf-8')
        outFile.close()

    def addSendedPackets(self, network):
        """
        Función que modifica los puertos almacenados de una red para actualizarlos
        @param network: red con los puertos nuevos
        @type: Network
        @return: None
        """

        self.doc = etree.parse(self.nombreFichero)

        for network2 in self.doc.getroot().getchildren():
            name = network2.find('name').get('name')
            if network.getName() == name:
                sendedPackets = network2.find('sendedpackets').get('name')
                network.setSendedPackets(int(sendedPackets) + 1)
                sendedPackets = network2.find('sendedpackets').set('name',str(network.getSendedPackets()))

        outFile = open(self.nombreFichero , 'w')
        self.doc.write(outFile, xml_declaration=True, encoding='utf-8')
        outFile.close()

    def deleteNetwork(self, network):
        """
        Función que borra una red del fichero de redes
        @param network: red a borrar
        @type: Network
        @return: None
        """
        self.doc = etree.parse(self.nombreFichero)
        
        for network2 in self.doc.getroot().getchildren():
            name = network2.find('name').get('name')
            if network.getName() == name:
                self.doc.getroot().remove(network2)
        
        outFile = open(self.nombreFichero , 'w')                        
        self.doc.write(outFile, xml_declaration=True, encoding='utf-8') 
        outFile.close()


    def getAllPacketsReceived(self):
        networksList = self.getAllNetworks()

        packetsReceived = 0

        for network in networksList:
            packetsReceived = packetsReceived + int(network.getReceivedPackets())

        return packetsReceived

    def getAllPacketsSended(self):
        networksList = self.getAllNetworks()

        packetsSended = 0

        for network in networksList:
            packetsSended = packetsSended + int(network.getSendedPackets())

        return packetsSended
            
            
            
            
            
            