# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 12:56:56 2015

@author: cased93
"""

class ColorStringClass:
    """
    Clase ColorStringClass, contiene los métodos gestionar los colores de las String de la consola
    """
    
    def __init__(self):
        self.data = []
    
    def messageFormatRed(self,message):
        """
        Función para formatear a rojo el mensaje pasado
        @param self: Instancia de la clase padre
        @type: Tipo de su clase base
        @param message: Mensaje que queremos formatear
        @return: String message rojo
        """     
            
        return "<span style=color:#ff0000;>" +  message + "</span"
        
    def messageFormatBlue(self,message):
        """
        Función para formatear a azul el mensaje pasado
        @param self: Instancia de la clase padre
        @type: Tipo de su clase base
        @param message: Mensaje que queremos formatear
        @return: String message azul
        """     
        return "<span style=color:#0099FF;>" +  message + "</span"
        
    def messageFormatGreen(self,message):
        """
        Función para formatear a verde el mensaje pasado
        @param self: Instancia de la clase padre
        @type: Tipo de su clase base
        @param message: Mensaje que queremos formatear
        @return: String message verde
        """     
        return "<span style=color:#00cc00;>" +  message + "</span"
        
    def messageFormatGray(self,message):
        """
        Función para formatear a gris el mensaje pasado
        @param self: Instancia de la clase padre
        @type: Tipo de su clase base
        @param message: Mensaje que queremos formatear
        @return: String message gris
        """     
        return "<span style=color:#FFFFFF;>" +  message + "</span"