# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 18:46:04 2015

@author: cased93
"""

class Network:
    """
    Clase Network, representa el modelo de las redes del sistema con los métodos de acceso a sus parámetros.
    """
    
    def __init__(self,name,IP,inPort,outPort,serial = 0, receivedPackets = 0,sendedPackets = 0):
        """
        Constructor de la clase Network
        @param name: nombre de la red
        @type: str
        @param IP: IP origen de la red
        @type: str
        @param inPort: puerto de entrada de la red
        @type: int
        @param outPort: puerto de salida de la red
        @type: int
        @return: None
        """
        
        self.data = []
        self.name = name
        self.IP = IP
        self.inPort = inPort
        self.outPort = outPort
        self.serial = serial
        self.receivedPackets = str(receivedPackets)
        self.sendedPackets = str(sendedPackets)
        
    def getName(self):
        """
        Getter del parametro name
        @return: parametro name
        """
        return self.name
        
    def getIP(self):
        """
        Getter del parametro IP
        @return: parametro IP
        """
        return self.IP
        
    def getInPort(self):
        """
        Getter del parametro inPort
        @return: parametro inPort
        """
        return self.inPort
        
    def getOutPort(self):
        """
        Getter del parametro outPort
        @return: parametro outPort
        """
        return self.outPort
        
    def getSerial(self):
        return self.serial

    def getReceivedPackets(self):
        """
        Getter del parametro receivedPackets
        @return: parametro receivedPackets
        """
        return self.receivedPackets

    def getSendedPackets(self):
        """
        Getter del parametro sendedPackets
        @return: parametro sendedPackets
        """
        return self.sendedPackets

    def setName(self,name):
        """
        Setter del parametro name
        @return: None
        """
        self.name = name
            
    def setIP(self,IP):
        """
        Setter del parametro IP
        @return: None
        """
        self.IP = IP
        
    def setInPort(self, inPort):
        """
        Setter del parametro inPort
        @return: None
        """
        self.inPort = inPort
        
    def setOutPort(self, outPort):
        """
        Setter del parametro outPort
        @return: None
        """
        self.outPort = outPort
        
    def setSerial(self,serial):
        self.serial = serial

    def setReceivedPackets(self,receivedPackets):
        """
        Setter del parametro receivedPackets
        @return: None
        """
        self.receivedPackets = receivedPackets

    def setSendedPackets(self,sendedPackets):
        """
        Setter del parametro sendedPackets
        @return: None
        """
        self.sendedPackets = sendedPackets
        
    