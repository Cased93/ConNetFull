# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 14:03:15 2016

@author: cased93
"""

class ThreadsHandler:
    """
    Clase ThreadsHandler, permite manejar el estado de los hilos para poder deternerlos si es necesario.
    """
    
    def __init__(self,threadToHandler):
        """
        Constructor de la clase ThreadsHandler
        """
        
        self.threadToHandler = threadToHandler
        
    def getThreadToHandler(self):
        return self.theadToHandler
        
    def setThreadToHandler(self,threadToHandler):
        self.threadToHandler = threadToHandler
        
    def removeDaemon(self):
        self.threadToHandler.setDaemon(False)
        
    def joinThread(self):
        self.threadToHandler.join()
