__author__ = 'cased93'
#0.05 0.025 0.01 0.005 0.0025 0.001
#1.54 ms

TIMEBETWEENPACKETS = 0.001 #0.05 0.025 0.01 0.0025 0.001 
TIMETODESPATCHQUEUE =  0.05 #0.05 0.01 0.005 0.0025 0.001 

NUMPACKETS = 1000
NUMTHREAD = 5
NUMNETWORKS = 5
TIMEBETWEENNETWORKS = 0.5


#Tiempos totales de funcionamiento por medicion

#0.05 --> 8,43 minutos
#0.025 --> 4,26 minutos
#0.01 --> 1,76 minutos
#0.005 --> 55,54 segundos
#0.0025 --> 33,44 segundos
#0.001 --> 15.4 segundos