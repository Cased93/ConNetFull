# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 13:16:33 2015

@author: cased93
"""
import sys,os
from PyQt4 import QtCore,QtGui
from PyQt4.QtCore import pyqtSlot,SIGNAL
from XMLDataBase import *
from ColorStringClass import *

class NetworkDetail(QtGui.QDialog):
    """
    Clase NetworkDetail, Dialogo emergente con la informacion de la red seleccionada
    """    
    
    def __init__(self,parent=None,networkStr=None, networkList=None, console=None):
        super(NetworkDetail, self).__init__(parent)
        
        """
        Constructor de la clase NetworkDetail
        @param parent: posible padre del que heredar
        @param networkStr: informacion de la red
        @type: str
        @param networkList: lista de redes conectadas
        @type: QtGui.QListWidget
        @param console: consola del programa principal
        @type: QtGui.QTextEdit
        @return: None
        """

        end = str(networkStr).find("In: ")
        nameStr = networkStr[6:end-1]
        self.networkList = networkList
        self.console = console
        print nameStr
        self.networksDataBase = XMLDataBase()
        self.network = self.networksDataBase.getNetworkByName(nameStr)
        self.formatString = ColorStringClass()
        self.resize(150, 322)
        self.gridlayout = QtGui.QGridLayout(self)
        name = QtGui.QLabel('Network name: '+ self.network.getName())
        name.setStyleSheet('color: #049DBF; font-size: 16pt; font-family: Georgia;')
        ip = QtGui.QLabel('Network IP: '+ self.network.getIP())
        ip.setStyleSheet('color: #049DBF; font-size: 16pt; font-family: Georgia;')
        inport = QtGui.QLabel('Network InPort: ' + self.network.getInPort())
        inport.setStyleSheet('color: #049DBF; font-size: 16pt; font-family: Georgia;')
        outport = QtGui.QLabel('Network OutPort: ' + self.network.getOutPort())
        outport.setStyleSheet('color: #049DBF; font-size: 16pt; font-family: Georgia;')

        self.gridlayout.setObjectName("gridlayout")
        self.gridlayout.addWidget(name, 0, 2, 1, 1)
        self.gridlayout.addWidget(ip, 1, 0, 1, 1)
        self.gridlayout.addWidget(inport, 2, 0, 1, 1)
        self.gridlayout.addWidget(outport, 3, 0, 1, 1)
        label = QtGui.QLabel() 
        pixmap = QtGui.QPixmap('node.png')
        label.setPixmap(pixmap)
        self.gridlayout.addWidget(label, 0, 0, 1, 1)
        delete = QtGui.QPushButton('Delete', self)
        self.gridlayout.addWidget(delete, 4, 1, 1, 1)
        delete.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        QtCore.QMetaObject.connectSlotsByName(self)
        
        delete.clicked.connect(self.on_pushButtonSetBase_toggled)
        
    @pyqtSlot()
    def on_pushButtonSetBase_toggled(self):
        """
         Funcion definición del funcionamiento del boton Delete, borra la red seleccionada
         @return: None
         """
        reply = QtGui.QMessageBox.question(self, 'Warning',
             "Do you want to delete network " + self.network.getName() +"?", QtGui.QMessageBox.Yes | 
             QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            self.console.append(self.formatString.messageFormatRed("Network deleted: " + self.network.getName()))
            self.networksDataBase.deleteNetwork(self.network)
            item = self.networkList.takeItem(self.networkList.currentRow())
            item = None
            self.close()
         