# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 12:56:56 2015
 
@author: cased93


"""
import sys,os
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
from config import *
from Queue import *
from PyQt4.QtGui import * 
from PyQt4.QtCore import * 
import threading
from ColorStringClass import *
from Protocol import *
from XMLDataBase import *
from VectorParser import *
import gc
import random
from ResponseMessage import *
from time import sleep
from config import *
import gtk, pygtk
from collections import deque
from itertools import cycle
from NetworkDetail import *
from SplashScreen import *
from Plots import *
import time
import os.path as osp
import serial

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)
        
class MainWindow(QtGui.QMainWindow):
    """
    Clase MainWindow, contiene los métodos para abrir y gestionar la ventana principal del programa así como su funcionamiento
    """
    
    def __init__(self, parent=None):

        """
        Función crear controles y añadirlos a la parte inicial
        @param parent: Posible padre del que heredar
        @return: None
        """        
        
        super(MainWindow, self).__init__()
        path = osp.join(osp.dirname(sys.modules[__name__].__file__), 'CONNET.png')
        os.system("rm /home/cased93/ConNetFull/Versiones/Normal/Networks.xml")
         
        """ Llamadas a funciones """
        self.createControler()

        self.move(300, 150)
        self.setWindowTitle("ConNet")
        self.showMaximized()
        self.setWindowIcon(QtGui.QIcon(path))
        self.center()
        """Dirección IP del servidor"""
        self.SERVER_IP = "127.0.0.1"

        self.data = ""
        """Puerto para la comunicación"""
        self.mainInPort = 8050
        self.mainOutPort = 8051

        self.protocol = Protocol(0, self.SERVER_IP, self.mainInPort,self.mainOutPort)
        
        self.networksDataBase = XMLDataBase()
        self.portList = [ ] #Lista de puertos en uso
        self.connectionList = [ ] #Lista de puertos en uso
        self.packetsReceived = 0 #Paquetes totales recibidos
        self.packetsSend = 0 #Paquetes totales enviados
        self.nThreads = 0 #Numero de redes totales conectadas 
        self.totalPackets = 0
        self.queue = Queue()
        self.semReceived = threading.Semaphore(1)
        self.semSended = threading.Semaphore(1)
        self.sendedList = [ ] #Lista con paquetes enviados por red
        self.receivedList = [ ] #lista con paquetes recibidos por red
        self.netList = [ ] #Lista de redes
        self.firstTime = 0 
        self.secondTime = 0
        self.conditionReceived = threading.Condition()
        self.conditionPackets = threading.Condition()
        self.pulsado = True


    def createControler(self):
            
        """
        Función crear controles y añadirlos a la parte inicial
        @return: None
        """
        '''Creación de Controles'''

        window = gtk.Window()
        screen = window.get_screen()
    
        self.pulsado = False
        self.centralwidget = QtGui.QWidget(self)
        self.centralwidget2 = QtGui.QWidget(self)
        self.horizontalLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(0, 0, screen.get_width() - 100, 46))
        self.horizontalLayout = QtGui.QHBoxLayout(self.horizontalLayoutWidget)
        self.udp = QtGui.QRadioButton(self.horizontalLayoutWidget)
        self.udp.setStyleSheet("color: #049DBF;font: bold 14px;min-width: 7em;padding: 3px;")
        self.udp.setChecked(True)
        self.udp.toggled.connect(self.udp_clicked)
        self.horizontalLayout.addWidget(self.udp)
        self.tcp = QtGui.QRadioButton(self.horizontalLayoutWidget)
        self.tcp.toggled.connect(self.tcp_clicked)
        self.tcp.setStyleSheet("color: #049DBF;font: bold 14px;min-width: 7em;padding: 3px;")
        self.horizontalLayout.addWidget(self.tcp)
        self.btnSerial = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.btnSerial.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        self.btnSerial.clicked.connect(self.openSerialConnection)
        self.horizontalLayout.addWidget(self.btnSerial)
        self.lineEditComboBox = QtGui.QLineEdit()    
        self.lineEditComboBox.setPlaceholderText("Select serial port...")
        self.lineEditComboBox.setReadOnly(True)
        self.lineEditComboBox.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 0px;border-radius: 0px;border-color: black;font: bold 5px;min-width: 2em;padding: 0px;")
        self.comboBox = QtGui.QComboBox(self.horizontalLayoutWidget)
        self.comboBox.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        self.comboBox.setLineEdit(self.lineEditComboBox)
    
        self.horizontalLayout.addWidget(self.comboBox)
        self.btn = QtGui.QPushButton(self.horizontalLayoutWidget)
        self.btn.setCheckable(True)
        self.btn.clicked.connect(self.on_pushButtonSetBase_toggled)
        self.btn.setToolTip('Click to connect/disconnet networks')
        self.btn.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        self.horizontalLayout.addWidget(self.btn)
        self.txtEdit = QtGui.QTextEdit(self.centralwidget)
        self.txtEdit.setGeometry(QtCore.QRect(10, 90, screen.get_width()/1.5, screen.get_height() - 200))
        self.txtEdit.setReadOnly(True)
        sb = self.txtEdit.verticalScrollBar()
        sb.setValue(sb.maximum())
        font = self.txtEdit.font()
        font.setFamily("Courier")
        font.setPointSize(12)
    
        self.txtEdit.setCurrentFont(font)
    
        self.txtEdit.setReadOnly(True)
        self.txtEdit.setLineWrapMode(QtGui.QTextEdit.NoWrap);
        self.txtEdit.setStyleSheet("QTextEdit{color: gray;}")
        
        self.listarchivos = QtGui.QListWidget(self.centralwidget)
        self.listarchivos.itemClicked.connect(self.PrintClick)
        self.listarchivos.setGeometry(QtCore.QRect(screen.get_width()/1.5 + 50, 90, 300, screen.get_height() - 200))
        self.consola = QtGui.QLabel(self.centralwidget)
        self.consola.setGeometry(QtCore.QRect(screen.get_width() /4 , 50, 151, 41))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.consola.setFont(font)
        self.consola.setStyleSheet('color: #049DBF; font-size: 14pt; font-family: Georgia;')
        self.networkList = QtGui.QLabel(self.centralwidget)
        self.networkList.setGeometry(QtCore.QRect(screen.get_width() - 340, 50, 800, 41))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.networkList.setFont(font)
        self.networkList.setStyleSheet('color: #049DBF; font-size: 14pt; font-family: Georgia;')
        self.inPortTextExtern = QtGui.QLineEdit(self.centralwidget)
        self.inPortTextExtern.setGeometry(QtCore.QRect(180, screen.get_height() - 90, 104, 31))
        self.inPortTextExtern.setPlaceholderText("8050")
        self.inPortTextExtern.setStyleSheet("background-color: white; color: gray;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 5em;padding: 6px;")
        self.outPortTextExtern = QtGui.QLineEdit(self.centralwidget)
        self.outPortTextExtern.setGeometry(QtCore.QRect(410, screen.get_height() - 90, 104, 31))
        self.outPortTextExtern.setPlaceholderText("8051")
        self.outPortTextExtern.setStyleSheet("background-color: white; color: gray;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 5em;padding: 6px;")
        self.inPortExtern = QtGui.QLabel(self.centralwidget)
        self.inPortExtern.setGeometry(QtCore.QRect(100, screen.get_height() -85 , 64, 17))
        self.inPortExtern.setStyleSheet("color: #049DBF;font: bold 14px;min-width: 7em;padding: 3px;")
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.inPortExtern.setFont(font)
        self.outPortExtern = QtGui.QLabel(self.centralwidget)
        self.outPortExtern.setGeometry(QtCore.QRect(320, screen.get_height() - 85, 64, 17))
        self.outPortExtern.setStyleSheet("color: #049DBF;font: bold 14px;min-width: 7em;padding: 3px;")
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.outPortExtern.setFont(font)
        self.setCentralWidget(self.centralwidget2)
        self.menubar = QtGui.QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 838, 27))
        self.setMenuBar(self.menubar)
    
        self.retranslateUi()
        self.comboBox.setCurrentIndex(-1)
        
        formatString = ColorStringClass()
        self.txtEdit.append(formatString.messageFormatGray("Welcome to ConNet, to start choose between UDP or TCP and click the connect button"))
    #        self.btnSerial.hide()
    #        self.comboBox.hide()
        
        l = QtGui.QGridLayout()
    
        self.sc = ReceivedPacketPerNetworkPlot('Paquetes recibidos por Red',"Paquetes ( p)","Nombre de la red",1,self)
        self.navi_toolbar = NavigationToolbar(self.sc, self)
        self.sc3 = SendedPacketPerNetworkPlot('Paquetes enviados por Red',"Paquetes ( p)","Nombre de la red",1,self)
        self.navi_toolbar3 = NavigationToolbar(self.sc3, self)
        self.sc2 = RealTimePlot('Paquetes totales recibidos/Segundo',"Paquetes (p)","Segundos (t)",'go-','Paquetes recibidos',self, width=5, height=4, dpi=100)
        self.navi_toolbar2 = NavigationToolbar(self.sc2, self)
        self.sc4 = RealTimePlot('Paquetes totales enviados/Segundo',"Paquetes (p)","Segundos (t)",'rs-','Paquetes enviados',self, width=5, height=4, dpi=100)
        self.navi_toolbar4 = NavigationToolbar(self.sc4, self)
    
    
        l.addWidget(self.sc,0,1)
        l.addWidget(self.navi_toolbar,1,1)
        l.addWidget(self.sc3,0,2)
        l.addWidget(self.navi_toolbar3,1,2)
        l.addWidget(self.sc2,2,1)
        l.addWidget(self.navi_toolbar2,3,1)
        l.addWidget(self.sc4,2,2)
        l.addWidget(self.navi_toolbar4,3,2)
    
        
        self.estadisticas = QtGui.QListWidget()
        self.estadisticas.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
        self.estadisticas.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.estadisticas.show()
        self.estadisticas.setMinimumSize(600,800)
    
        txtStatistics = QtGui.QLabel('Networks Statistics')
        txtStatistics.setStyleSheet('color: #049DBF; font-size: 20pt; font-family: Georgia;')
    
        csv = QtGui.QPushButton('Export to CSV', self)
        csv.setCheckable(True)
        csv.setMaximumSize(200,200)
        csv.setToolTip('Click to export to CSV')
        csv.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        csv.clicked.connect(self.exportToCSV)
        
        tabs = QTabWidget(self.centralwidget2);
        tabs.setFixedSize(screen.get_width(), screen.get_height());
        tab1 = QtGui.QWidget()
        tab2 = QtGui.QWidget()
        tab3 = QtGui.QWidget()
    
        vBoxlayout	= QtGui.QVBoxLayout()
        vBoxlayout.addWidget(txtStatistics)
        vBoxlayout.addWidget(self.estadisticas)
        vBoxlayout.addWidget(csv)
        vBoxlayout.setAlignment(txtStatistics,Qt.AlignCenter)
        vBoxlayout.setAlignment(csv,Qt.AlignCenter)
        vBoxlayout.setAlignment(self.estadisticas,Qt.AlignCenter)
    
    
        #Set Layout for Third Tab Page
        tab3.setLayout(vBoxlayout)
        tab2.setLayout(l)
        tabs.addTab(self.centralwidget,"Network Connections")
        tabs.addTab(tab2,"Plots")
        tabs.addTab(tab3,"Statistics")
    
        self.managedStatistics()
        
        QtCore.QMetaObject.connectSlotsByName(self)
        
    
    def retranslateUi(self):
        self.setWindowTitle(_translate("ConNet", "ConNet", None))
        self.udp.setText(_translate("ConNet", "UDP", None))
        self.tcp.setText(_translate("ConNet", "TCP", None))
        self.btnSerial.setText(_translate("ConNet", "Open Serial Communication", None))
        self.btn.setText(_translate("ConNet", "Connect", None))
        self.consola.setText(_translate("ConNet", "Console/LOG", None))
        self.networkList.setText(_translate("ConNet", "Networks List", None))
        self.inPortExtern.setText(_translate("ConNet", "UDP In", None))
        self.outPortExtern.setText(_translate("ConNet", "UDP Out", None))
        
    def tcp_clicked(self, enabled):
        if enabled:
            self.inPortExtern.setVisible(False)
            self.inPortTextExtern.setVisible(False)
            self.outPortExtern.setText("TCP Port")
            
    def udp_clicked(self, enabled):
        if enabled:
            self.inPortExtern.setVisible(True)
            self.inPortTextExtern.setVisible(True)
            self.outPortExtern.setText("UDP Out")

    @pyqtSlot()
    def openSerialConnection(self):
        formatString = ColorStringClass()
        try:
            self.btnSerial.setText("Close Serial Communication")
            self.serialPort = serial.Serial('/dev/ttyUSB0', 115200 )
            self.newSerialConnection = threading.Thread(target=self.catchNewSerialNetwork,args=())
            self.newSerialConnection.start()   
            
        except:
            self.btnSerial.setText("Open Serial Communication")
            self.txtEdit.append(formatString.messageFormatRed("Error opening serial port communication, check the connection..."))


    def catchNewSerialNetwork(self):
        formatString = ColorStringClass()
        serialNet = self.serialPort.readline()
        vectorParser = VectorParser()
        newVector = vectorParser.stringToVector(serialNet)
        newNetwork = Network(newVector[0], '0', newVector[1], newVector[1])
        newNetwork.setSerial(1)
        items = self.listarchivos.findItems("TelosB",QtCore.Qt.MatchContains)
        
        if len(items) > 0:
            self.txtEdit.append(formatString.messageFormatRed("Already exist a network with this name connected: " + newNetwork.getName()))
        else:
            self.netList.append(newNetwork)
            name = str(newNetwork.getName())
            msg = "Name: TelosB" + "\nBauds: 115200" + "\nPort: " + newNetwork.getInPort()
            item = QtGui.QListWidgetItem(str(msg))
            self.listarchivos.addItem(item)
            self.txtEdit.append(formatString.messageFormatGreen("New Serial network added called TelosB"))
            t = threading.Thread(target=self.receivedFromSerial)
            t.daemon = True                    
            t.start()
            
            
    @pyqtSlot()
    def on_pushButtonSetBase_toggled(self):
        
         """
         Funcion definición del funcionamiento del boton Connect
         @return: None
         """
         formatString = ColorStringClass()    
         if self.pulsado == True:
            self.btn.setText("Disconnect")
            self.pulsado = False
            if self.udp.isChecked() == True:
                self.connected = True
                self.txtEdit.append(formatString.messageFormatGray("Connected by UDP, waiting Datagrams..."))
                self.mainPort = threading.Thread(target=self.connectionMainPort,args=())
                self.mainPort.start()                                                
            
            elif self.tcp.isChecked() == True:
                self.txtEdit.append(formatString.messageFormatGray("Connected by TCP, waiting for a connection..."))
                self.protocol.setTypeOf(1)
                self.connected = True
                self.mainPort = threading.Thread(target=self.connectionMainPort,args=())
                self.mainPort.start()                                             
            
            self.btnSerial.show()
                
         else:
             reply = QtGui.QMessageBox.question(self, 'Warning',
             "Do you want to close all connections?", QtGui.QMessageBox.Yes | 
             QtGui.QMessageBox.No, QtGui.QMessageBox.No)

             if reply == QtGui.QMessageBox.Yes:
                self.btn.setText("Connect")
                self.listarchivos.clear()
                self.pulsado = True
                self.txtEdit.append(formatString.messageFormatGray("Connection disconnected and socket closed"))
                self.protocol.closeConnection()
         
             self.btnSerial.hide()
            
            


    def center(self):
        """
        Funcion para centrar la interfaz grafica en la pantalla
        """
        
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())
        
        
    def closeEvent(self, event):
        
         """
         Función llamada cuando detecta el evento de cerrado
         @param event: Evento que recoge el cierre
         @type: Event
         @return: None
         """
         reply = QtGui.QMessageBox.question(self, 'Warning',
            "Do you want to close this program?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

         if reply == QtGui.QMessageBox.Yes:
            self.protocol.closeConnection()
            gc.collect()
            event.accept()
         else:
            event.ignore()

    def NewNetworkConnection(self,nthread):
        
        """
         Función que abre el puerto UDP/TCP para establecer comunicación con un servicio o red externa
         @return: None
         """            
        count = 0
        formatString = ColorStringClass()
        if self.protocol.getTypeOf() == 0:
            returnData = self.data
        elif self.protocol.getTypeOf() == 1:
            returnData = self.dataTCP
        vectorParser = VectorParser()
        newVector = vectorParser.stringToVector(returnData)
        inport = self.protocol.generateInPort(self.portList)
        outPort = self.protocol.generateOutPort(self.portList)
        newNetwork = Network(newVector[3],newVector[0], str(inport), str(outPort))
        self.netList.append(newNetwork)
        items = self.listarchivos.findItems(newNetwork.getName(),QtCore.Qt.MatchContains)
        gc.collect()

        if len(items) > 0:
            self.txtEdit.append(formatString.messageFormatRed("Already exist a network with this name connected: " + newNetwork.getName()))
        else:
            with self.conditionReceived:
                comprobar = self.networksDataBase.getNetworkByName(newNetwork.getName())
                if comprobar == None:
                    print "Guardando red"
                    self.networksDataBase.saveNetwork(newNetwork)
                else:
                    print "Cambiando puertos"
                    self.networksDataBase.setNewPorts(newNetwork)

            checkSourceIP = newNetwork.getIP().split('.')
            correctIP = True
            for octeto in checkSourceIP:
                if(int(octeto) < 0 or int(octeto) > 255):
                    correctIP = False
            if correctIP == False:
                self.txtEdit.append(formatString.messageFormatRed("IP has to be in range 0.0.0.0 255.255.255.255"))
                
            else:
                item = QtGui.QListWidgetItem("Name: " + newNetwork.getName() + "\nIn: "+newNetwork.getInPort()+"\nOut: "+newNetwork.getOutPort())
                self.listarchivos.addItem(item)
                self.txtEdit.append(formatString.messageFormatGreen("New network added called " + newNetwork.getName()))
                self.txtEdit.append(formatString.messageFormatBlue("Received message: " + repr(newVector)))
                helloResponse= newNetwork.getIP()+","+newNetwork.getIP()+",hello,"+ newNetwork.getName()+","+newNetwork.getInPort()+","+newNetwork.getOutPort()
                self.protocol.sendToHello(helloResponse, 8050)
            
                finalSockExtern = Protocol(self.protocol.getTypeOf(), newNetwork.getIP(), int(newNetwork.getInPort()),int(newNetwork.getOutPort()))
                self.connectionList.insert(nthread-1,finalSockExtern)
                try:
                    finalSockExtern.openConnection(newNetwork)
                except:
                    print "Ya conectado"
                finally:
                    t = threading.Thread(target=self.receivedFrom,args=(finalSockExtern,newNetwork,nthread,))
                    t.daemon = True                    
                    t.start()
        
    def receivedFrom(self, finalSockExtern, newNetwork,nthread):
       """
       Función de recepción de cada red individual
       """
       formatString = ColorStringClass()

       while True:
           
           if self.protocol.getTypeOf() == 0:
               data, addr = finalSockExtern.receiveFrom() # bufer size is 1024 bytes
               data = data +  "," + str(nthread)
           elif self.protocol.getTypeOf() == 1:
               data = finalSockExtern.receiveFrom() # bufer size is 1024 bytes
               data = data +  "," + str(nthread)
               
           with self.conditionPackets:
               self.totalPackets = self.totalPackets + 1               
               try:
                    # Connect to server and send data
                    #sock.bind((HOST, PORT))
                    vectorParser = VectorParser()
                    newVector = vectorParser.stringToVector(data)
                    print newVector
                    i = 0
                    for net in self.netList:
                        if net.getName() == newVector[3]:
                            if net.getSerial() == 1:       
                             
                                self.serialPort.write(data + "\n")
                                
                            elif self.protocol.getTypeOf() == 0 and net.getSerial() == 0:
                                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                                sock.sendto(data,(net.getIP(),int(net.getInPort())))
                                
                            elif self.protocol.getTypeOf() == 1 and net.getSerial() == 0:
                                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                                self.connectionList[i].send(newVector[2])
                                
                            self.sendedList[nthread-1] = self.sendedList[nthread-1] + 1

                        i = i + 1
                    self.receivedList[nthread-1] = self.receivedList[nthread-1] + 1
               except:
                   print "Exception handler, closing connection"
                   self.connectionList[i].closeConnection()
                   self.txtEdit.append(formatString.messageFormatRed("Connection refused from client: " + newNetwork.getName()))
                   self.networksDataBase.deleteNetwork(newNetwork)
                   item = self.listarchivos.takeItem(nthread-1)
                   self.listarchivos.removeItemWidget(item)
                   self.listarchivos.repaint()
                   item = None
                   break
               finally:
                    gc.collect()
    
    def receivedFromSerial(self):
       """
       Función de recepción de cada red individual
       """
       
       while True:
           data = self.serialPort.readline()
           print ("Data: " + data)
           with self.conditionPackets:
               self.totalPackets = self.totalPackets + 1               
               vectorParser = VectorParser()
               newVector = vectorParser.stringToVector(data)
               
               i = 0
               
               for net in self.netList:
                   if net.getName() == (newVector[3])[0:len(newVector[3])-1]:
                       if net.getSerial() == 1:
                           self.serialPort.write(data  + "\n")
                           
                       elif self.protocol.getTypeOf() == 0 and net.getSerial() == 0:
                           sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                           sock.sendto(data,(net.getIP(),int(net.getInPort())))
                            
                       elif self.protocol.getTypeOf() == 1 and net.getSerial() == 0:
                           sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                           self.connectionList[i].send(newVector[2])
                            
                   else:
                       i = i + 1

        
              
    def connectionMainPort(self):
        """
        Función para conectar con el puerto general al que van a llegar todas las nuevas redes.
        @return: None
        """
        formatString = ColorStringClass()
        self.mainInPort = self.inPortTextExtern.text()
        self.mainOutPort = self.outPortTextExtern.text()
        if self.mainInPort == "":
            self.mainInPort = 8050
        if self.mainOutPort == "":
            self.mainOutPort = 8051

        #try:
        self.protocol.setInPort(int(self.mainInPort))
        self.protocol.setOutPort(int(self.mainOutPort))
        self.protocol.openMainConnection()

        self.packetsSecond = threading.Thread(target=self.packetsPerSecond,args=())
        self.packetsSecond.start()
        while True:
            if self.protocol.getTypeOf() == 0:
                gc.collect()
                self.data, addr = self.protocol.receiveFromMain() # buffer size is 1024 bytes
            elif self.protocol.getTypeOf() == 1:
                gc.collect()
                self.dataTCP = self.protocol.receiveFromMain()

            self.nThreads = self.nThreads + 1
            num = self.nThreads
            self.n = threading.Thread(target=self.NewNetworkConnection,args=(num,))
            self.n.start()
            self.receivedList.append(0)
            self.sendedList.append(0)


        #except:
            #self.txtEdit.append(formatString.messageFormatRed("Error connecting with sockets, try to change the ports"))
        

    def packetsPerSecond(self):
        """
        Funcion encargada de calcular el numero de paquetes por segundo del sistema
        """
        i = 1
        for net in self.receivedList:
            self.packetsReceived = self.packetsReceived + net
            self.packetsSend = self.packetsSend + self.sendedList[i-1]
            with self.conditionReceived:
                self.networksDataBase.addReceivedSendedPackets(i,net,self.sendedList[i-1])
            i = i + 1

        self.sc2.update_data(self.packetsReceived)
        self.sc4.update_data(self.packetsSend)
        self.packetsReceived = 0
        self.packetsSend = 0
        threading.Timer(1, self.packetsPerSecond).start()

    def PrintClick(self):
        """
        Función que abre una instancia de la clase NetworkDetail donde se nos muestran los detalles de de la red seleccionada 
        @return: None
        """
        formatString = ColorStringClass()
        self.txtEdit.append(formatString.messageFormatGray("Network selected: " + self.listarchivos.selectedItems()[0].text()))
        self.dialogTextBrowser = NetworkDetail(self,self.listarchivos.selectedItems()[0].text(),self.listarchivos,self.txtEdit)
        self.dialogTextBrowser.exec_()


    def managedStatistics(self):
        """
        Funcion encargada de actualizar y calcular las estadisticas del sistema
        """
        dataBase = XMLDataBase()
        networkList = dataBase.getAllNetworks()
        for newNetwork in networkList:

            packetReceivedPercent = (float(newNetwork.getReceivedPackets())/dataBase.getAllPacketsReceived())*100
            packetSendedPercent = (float(newNetwork.getSendedPackets())/dataBase.getAllPacketsSended())*100
            lostReceivedPackets = NUMPACKETS - int(newNetwork.getReceivedPackets())
            lostSendedPackets = int(newNetwork.getReceivedPackets()) - int(newNetwork.getSendedPackets())
            lostReceivedPacketsPercent = (float(lostReceivedPackets)/NUMPACKETS)*100
            lostSendedPacketsPercent = (float(lostSendedPackets)/float(newNetwork.getReceivedPackets()))*100

            item = QtGui.QListWidgetItem("\n" + newNetwork.getName() + "\n\nReceived Packets: "+newNetwork.getReceivedPackets()+"\n\nSended Packets: "+newNetwork.getSendedPackets() + "\n\nLost Received Packets: " + str(lostReceivedPackets) + "\n\nLost Sended Packets: " + str(lostSendedPackets) + "\n\n Percent of Total Received Packets: " + str(round(packetReceivedPercent,2)).replace('.', ',') + "%"  + "\n\n Percent of Total Sended Packets: " + str(round(packetSendedPercent,2)).replace('.', ',')+ "%" + "\n\n Percent of Total Lost Received Packets: " + str(round(lostReceivedPacketsPercent,2)).replace('.', ',') + "%"  + "\n\n Percent of Total Lost Sended Packets: " + str(round(lostSendedPacketsPercent,2)).replace('.', ',') + "%"  + "\n")
            self.estadisticas.addItem(item)


    def exportToCSV(self):
        """
        Funcion que permite exportar a CSV las estadisticas
        """
        dataBase = XMLDataBase()
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save File',"networksStatistics", os.getenv('HOME'))
        filename = unicode(filename)
        networkList = dataBase.getAllNetworks()
        archi=open(filename + ".csv",'a')
        archi.write('"Network Name","Received Packets","Sended Packets","Lost Received Packets","Lost Sended Packets","Percent of Total Received Packets","Percent of Total Sended Packets","Percent of Total Lost Received Packets","Percent of Total Lost Sended Packets"\n')
        for newNetwork in networkList:
            packetReceivedPercent = (float(newNetwork.getReceivedPackets())/dataBase.getAllPacketsReceived())*100
            packetSendedPercent = (float(newNetwork.getSendedPackets())/dataBase.getAllPacketsSended())*100
            lostReceivedPackets = NUMPACKETS - int(newNetwork.getReceivedPackets())
            lostSendedPackets = int(newNetwork.getReceivedPackets()) - int(newNetwork.getSendedPackets())
            lostReceivedPacketsPercent = (float(lostReceivedPackets)/NUMPACKETS)*100
            lostSendedPacketsPercent = (float(lostSendedPackets)/float(newNetwork.getReceivedPackets()))*100
            archi.write('"'+newNetwork.getName()+'";"'+newNetwork.getReceivedPackets()+'";"'+newNetwork.getSendedPackets()+'";"'+str(lostReceivedPackets)+'";"'+str(lostSendedPackets)+'";"'+str(round(packetReceivedPercent,2)).replace('.', ',')+'";"'+str(round(packetSendedPercent,2)).replace('.', ',')+'";"' + str(round(lostReceivedPacketsPercent,2)).replace('.', ',')+'";"' + str(round(lostSendedPacketsPercent,2)).replace('.', ',') + '"\n')


        archi.close()


"""
Main de la aplicación
"""
if __name__ == '__main__':
 
    """
    Función main
    @return: None
    """
    
    app = QtGui.QApplication(sys.argv)
    css = """
    
    QWidget
    {
    font:12px bold;
    font-weight:bold;
    border-radius: 1px;
    height: 11px;
    
    }
    QDialog{
    Background-image:url('img/titlebar bg.png');
    font-size:12px;
    color: black;
    
    }
    QToolButton:hover{
    Background:n #049DBF;
    border: 1px solid black;
    font-size:11px;
    }
    
    QTextEdit{
        border: 3px solid #049DBF;
        background-color: #1A2326;
    }
    
    QPushButton{
        border: 1px solid black;    
    }
    
    QListWidget::item {
     border-style: solid;
     border-width:1px;
     border-color:#ffffff;
     color: #ffffff;
    }
    QListWidget{
        border: 3px solid #049DBF;
        background-color: #1A2326;
    
    }
    QTabWidget{background-color: #FFFFFF;border: 1px solid black;}
    """
    
    app.setStyleSheet(css)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())