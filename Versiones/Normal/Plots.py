from __future__ import unicode_literals
import sys
import os
import random
from matplotlib.backends import qt4_compat
from numpy.random.mtrand import normal, uniform, rand
import threading

use_pyside = qt4_compat.QT_API == qt4_compat.QT_API_PYSIDE
if use_pyside:
    from PySide import QtGui, QtCore
else:
    from PyQt4 import QtGui, QtCore

from numpy import arange, sin, pi
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import pylab as P
import numpy as np
from XMLDataBase import *
import random

class Plots(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self,title,xlabel,ylabel,style=None,legend=None,parent=None, width=5, height=4, dpi=100):
        self.figure = plt.figure(figsize=(width, height), dpi=dpi)
        self.figure.suptitle(title)
        plt.rc('lines', linewidth=3)
        self.figure.text(0.5, 0.01, ylabel, ha='center')
        self.figure.text(0.03, 0.5, xlabel, va='center', rotation='vertical')
        # We want the axes cleared every time plot() is called
        x = np.linspace(0, 1)
        self.ax1 = plt.subplot(111,axisbg='#1A2326',label="A")

        self.compute_initial_figure()
        #
        FigureCanvas.__init__(self, self.figure)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)


    def compute_initial_figure(self):
        pass



class ReceivedPacketPerNetworkPlot(Plots):
    """Simple canvas with a sine plot."""
    def __init__(self, *args, **kwargs):
        Plots.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)

    def compute_initial_figure(self):

        dataBase = XMLDataBase()
        list = dataBase.getAllNetworks()
        self.name = ()
        self.rects = ()
        self.N = dataBase.getNumNetworks()
        self.height = 0
        self.width = 0.15

        for network in list:
            ## necessary variables
            ## the bars
            rects1 = self.ax1.bar(self.height + self.width, int(network.getReceivedPackets()), self.width,
                            color=rand(3,1),
                            yerr=1,
                            error_kw=dict(elinewidth=2,ecolor='black'))

            self.rects = self.rects + tuple(rects1)

            self.name = self.name + (network.getName(),)

             # axes and labels

            self.ax1.set_xlim(0,self.N)
            xTickMarks = []
            self.ax1.set_xticks(np.arange(self.N)+self.width)
            self.height = self.height + 0.6

            legend = self.figure.legend(self.rects, self.name,fancybox=True,shadow=True)
            for label in legend.get_texts():
                label.set_fontsize('large')

            for label in legend.get_lines():
                label.set_linewidth(1.5)  # the legend line width

    def update_figure(self):
        # Build a list of 4 random integers between 0 and 10 (both inclusive)
        print ""


class SendedPacketPerNetworkPlot(Plots):
    """Simple canvas with a sine plot."""
    def __init__(self, *args, **kwargs):
        Plots.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)

    def compute_initial_figure(self):

        dataBase = XMLDataBase()
        list = dataBase.getAllNetworks()
        self.name = ()
        self.rects = ()
        self.N = dataBase.getNumNetworks()
        self.height = 0
        self.width = 0.15

        for network in list:
            ## necessary variables
            ## the bars
            rects1 = self.ax1.bar(self.height + self.width, int(network.getSendedPackets()), self.width,
                            color=rand(3,1),
                            yerr=1,
                            error_kw=dict(elinewidth=2,ecolor='black'))

            self.rects = self.rects + tuple(rects1)

            self.name = self.name + (network.getName(),)

             # axes and labels

            self.ax1.set_xlim(0,self.N)
            xTickMarks = []
            self.ax1.set_xticks(np.arange(self.N)+self.width)
            self.height = self.height + 0.6

            legend = self.figure.legend(self.rects, self.name,fancybox=True,shadow=True)
            for label in legend.get_texts():
                label.set_fontsize('large')

            for label in legend.get_lines():
                label.set_linewidth(1.5)  # the legend line width

    def update_figure(self):
        # Build a list of 4 random integers between 0 and 10 (both inclusive)
        print ""





class RealTimePlot(Plots):
    """A canvas that updates itself every second with a new plot."""
    def __init__(self, *args, **kwargs):
        Plots.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)
        self.packets = list()
        self.seconds = list()
        self.args = args

        L1 = self.ax1.plot([0, 1, 2, 3], [0, 0, 0, 0], self.args[3])
        legend = self.figure.legend((L1), (args[4],), 'upper right',fancybox=True,shadow=True)
        # Set the fontsize
        for label in legend.get_texts():
            label.set_fontsize('large')

        for label in legend.get_lines():
            label.set_linewidth(1.5)  # the legend line width

    def compute_initial_figure(self):
        print "Iniciando grafica"

    def update_figure(self):
        # Build a list of 4 random integers between 0 and 10 (both inclusive

        self.ax1.plot(self.seconds, self.packets, self.args[3])
        self.draw()

    def update_data(self,packet):
        self.packets.append(packet)
        self.seconds.append(len(self.packets))


