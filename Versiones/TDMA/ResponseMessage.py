# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 00:14:53 2015

@author: cased93
"""

class ResponseMessage:
    
    """
    Clase ResponseMessage, encargada de establecer los codigos de status y error y el mensaje de las peticiones
    """ 
    
    STATUS_OK = "1"
    ERROR_OK = "0x0000"
    STATUS_KO = "-1"
    ERROR_IP = "0x0001"
    ERROR_IP_MESSAGE = "The IP received isn't correct or not exist"    
    ERROR_ARGUMENTS = "0x0002"
    ERROR_ARGUMENTS_MESSAGE = "The numbers of arguments are incorrect"
    
    def __init__(self):
        """
        Constructor de la clase ResponseMessage
        @return: None
        """
        self.data = []
        self.statusCode = -1
        self.errorCode = -1
        self.message = -1
        
    def getStatusCode(self):
        """
        Getter del parametro statusCode
        @return: parametro statusCode
        """
        return self.statusCode
        
    def getErrorCode(self):
        """
        Getter del parametro errorCode
        @return: parametro errorCode
        """
        return self.errorCode
        
    def getMessage(self):
        """
        Getter del parametro message
        @return: parametro message
        """
        return self.message
        
    def setStatusCode(self, statusCode):
        """
        Setter del parametro statusCode
        @return: parametro None
        """
        self.statusCode = statusCode
    
    def setErrorCode(self, errorCode):
        """
        Setter del parametro errorCode
        @return: parametro None
        """
        self.errorCode = errorCode
        
    def setMessage(self, message):
        """
        Setter del parametro message
        @return: parametro None
        """
        self.message = message