# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 13:16:33 2015

@author: cased93
"""
import sys,os
from PyQt4 import QtCore,QtGui
from PyQt4.QtCore import pyqtSlot,SIGNAL
from XMLDataBase import *
from ColorStringClass import *
from time import sleep

class SplashScreen(QtGui.QMainWindow):
    """
    Clase NetworkDetail, Dialogo emergente con la informacion de la red seleccionada
    """    
    
    def __init__(self,parent=None):
        super(SplashScreen, self).__init__()
        
        """
        Constructor de la clase NetworkDetail
        @param parent: posible padre del que heredar
        @param networkStr: informacion de la red
        @type: str
        @param networkList: lista de redes conectadas
        @type: QtGui.QListWidget
        @param console: consola del programa principal
        @type: QtGui.QTextEdit
        @return: None
        """

        self.move(300, 150)        
        self.setMinimumSize(160,160)
        self.resize(800,400)
        self.center()
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.gridlayout = QtGui.QGridLayout(self)
        self.gridlayout.setSpacing(5)
        self.gridlayout.setColumnStretch(0, 1)
        self.gridlayout.setColumnStretch(5, 1)
        self.gridlayout.setRowStretch(0, 1)
        self.gridlayout.setRowStretch(5, 1)
        label = QtGui.QLabel() 
        pixmap = QtGui.QPixmap('CONNET.png')
        label.setPixmap(pixmap)
        self.gridlayout.addWidget(label, 1, 1)
        consola = QtGui.QLabel('ConNet - Designed and Developed By: \n\n- Daniel Castillo Secilla \n\n- Jose Maria Castillo Secilla\n\nUniversidad de Cordoba - 2015')
        consola.setStyleSheet('color: #049DBF; font-size: 14pt; font-family: Georgia;')
        self.gridlayout.addWidget(consola, 4,1)
        widget = QtGui.QWidget()
        widget.setLayout(self.gridlayout)
        self.setCentralWidget(widget)        
        
        
    def sleepSplash(self):
        self.close()
        
    def center(self):
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())