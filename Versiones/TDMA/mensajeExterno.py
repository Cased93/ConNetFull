# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 13:27:43 2015

@author: cased93
"""

import sys,os
import socket
import pickle
import numpy as np
from time import sleep
import threading
from config import *

def newNetwork(id):
# crea un socket INET de tipo STREAM
    sock = socket.socket(socket.AF_INET, # Internet
                                socket.SOCK_DGRAM) # UDP                            
                                # crea un socket INET de tipo STREAM
    mensaje="127.0.0.1,127.0.0.1,hello,Network " + str(id)
    
    sock.bind(("127.0.0.1",8050))
    print("Conectado al socket...")
    sock.sendto(mensaje,('127.0.0.1',8051))
    print("Esperando para recibir puertos...")
    data, addr = sock.recvfrom(1024)
    print data
    dataSplit = data.split(',')
    returnArray = np.asarray(dataSplit)
    print("Saludo recibido, conectando a los nuevos puertos")
    sock.close()
    # crea un socket INET de tipo STREAM
    newsock = socket.socket(socket.AF_INET, # Internet
                                socket.SOCK_DGRAM) # UDP                            
    newsock.bind(("127.0.0.1",int(returnArray[4])));
    print("Reconectando al nuevo socket...")
    for i in range(NUMPACKETS):
        message = "127.0.0.1,127.0.0.1,"+ str(i) + ",Network "+ str(id)
        newsock.sendto(message,('127.0.0.1',int(returnArray[5])))
        sleep(TIMEBETWEENPACKETS)
    #    print "Echo rebotado recibido desde la interfaz " + data


for i in range(NUMNETWORKS):
    t = threading.Thread(target=newNetwork, args=(i,))
    t.start()
    sleep(TIMEBETWEENNETWORKS)


