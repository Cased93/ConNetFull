# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 12:56:56 2015
 
@author: cased93


"""
import sys,os
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
from Queue import *
from PyQt4.QtCore import *
from Protocol import *
from config import *
from VectorParser import *
import gc
from NetworkDetail import *
from SplashScreen import *
from Plots import *
from time import sleep
import os.path as osp
import time

class MainWindow(QtGui.QTabWidget):
    """
    Clase MainWindow, contiene los métodos para abrir y gestionar la ventana principal del programa así como su funcionamiento
    """
    
    def __init__(self, parent=None):

        """
        Función crear controles y añadirlos a la parte inicial
        @param parent: Posible padre del que heredar
        @return: None
        """        
        
        super(MainWindow, self).__init__()
        path = osp.join(osp.dirname(sys.modules[__name__].__file__), 'CONNET.png')
        #os.system("rm /home/cased93/ConNet/TDMA/Networks.xml")
         
        """ Llamadas a funciones """
        self.createControler()
        self.timeRanure = TIMETODESPATCHQUEUE
        self.numRanures = 1
        self.move(300, 150)
        self.setWindowTitle("ConNet")
        self.showMaximized()
        self.center()
        self.setWindowIcon(QtGui.QIcon(path))
        #self.set(QtGui.QPixmap(path))
        """Dirección IP del servidor"""
        self.SERVER_IP = "127.0.0.1"

        self.data = ""
        """Puerto para la comunicación"""
        self.mainInPort = 8050
        self.mainOutPort = 8051

        self.protocol = Protocol(0, self.SERVER_IP, self.mainInPort,self.mainOutPort)
        
        self.networksDataBase = XMLDataBase()
        self.portList = [ ] #Lista de puertos en uso
        self.threadList = [ ]
        self.packetsReceived = 0 #Paquetes totales recibidos
        self.queue = Queue()
        self.connectionQueue = Queue() #Cola con las conexiones de TCP abiertas
        self.nThreads = 0 #Numero de redes totales conectadas 
        self.actualTurn = 1

        self.packetsReceived = 0
        self.packetsSend = 0 #Paquetes totales enviados
        self.condition = threading.Condition()
        self.conditionReceived = threading.Condition()
        self.conditionSended = threading.Condition()
        self.sendedList = [ ] #Lista con paquetes enviados por red
        self.receivedList = [ ] #Lista con paquetes recibidos por red
        self.netList = [ ] #Lista de redes
        self.semaphore = threading.Semaphore()
        self.firstTime = 0
        self.secondTime = 0
        self.totalPackets = 0
        self.conditionPackets = threading.Condition()

    def createControler(self):
            
        """
        Función crear controles y añadirlos a la parte inicial
        @return: None
        """
        '''Creación de Controles'''
        self.udp = QtGui.QRadioButton("UDP",self)       
        self.udp.setChecked(True)
        self.tcp = QtGui.QRadioButton("TCP",self)
        consola = QtGui.QLabel('Console/LOG')
        consola.setStyleSheet('color: #049DBF; font-size: 14pt; font-family: Georgia;')
        networkList = QtGui.QLabel('Networks List')
        networkList.setStyleSheet('color: #049DBF; font-size: 14pt; font-family: Georgia;')
        carpeta_label = QtGui.QLabel('Carpeta:')
        self.pulsado = True
        self.connected = False
        self.btn = QtGui.QPushButton('Connect', self)
        self.btn.setCheckable(True)
        self.btn.setToolTip('Click to connect/disconnet networks')
        self.btn.clicked.connect(self.on_pushButtonSetBase_toggled)
        self.btn.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
     
     
        outPortTextExtern = QtGui.QLabel("Main Outport:",self)
        
        self.outPortExtern = QtGui.QLineEdit(self)
        self.outPortExtern.setPlaceholderText("8051")
        self.outPortExtern.setStyleSheet("background-color: white; color: gray;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        
         
        inPortTextExtern = QtGui.QLabel("Main Inport:",self)

        self.inPortExtern = QtGui.QLineEdit(self)

        self.inPortExtern.setPlaceholderText("8050")
        self.inPortExtern.setStyleSheet("background-color: white; color: gray;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        
        self.listarchivos = QtGui.QListWidget()
        self.listarchivos.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
        self.listarchivos.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.listarchivos.show()
        self.connect(self.listarchivos, SIGNAL("itemSelectionChanged()"),self.PrintClick)        
        self.txtEdit= QtGui.QTextEdit()

        '''
        Creación de Rejilla de distribución de controles
        '''

        grid = QtGui.QGridLayout()
        grid.setSpacing(5)
 
        grid.addWidget(self.udp,0,5,1,1)
        grid.addWidget(self.tcp,0,6,1,1)
        grid.addWidget(consola,1,6)
        grid.addWidget(networkList,1,11)
        grid.addWidget(self.listarchivos,2,11,10,2)
        grid.addWidget(self.txtEdit,2,2,10,9)
        grid.addWidget(self.btn,0,11)
        grid.addWidget(outPortTextExtern,12,4)
        grid.addWidget(self.outPortExtern,12,5,1,1)
        grid.addWidget(inPortTextExtern,12,8)
        grid.addWidget(self.inPortExtern,12,9,1,1)

        sb = self.txtEdit.verticalScrollBar()
        sb.setValue(sb.maximum())

        font = self.txtEdit.font()
        font.setFamily("Courier")
        font.setPointSize(12)

        self.txtEdit.setCurrentFont(font)

        self.txtEdit.setReadOnly(True)
        self.txtEdit.setLineWrapMode(QtGui.QTextEdit.NoWrap);
        self.txtEdit.setStyleSheet("QTextEdit{color: gray;}")
        formatString = ColorStringClass()
        self.txtEdit.append(formatString.messageFormatGray("Welcome to ConNet, to start you choose between UDP or TCP and click the connect button"))


        l = QtGui.QGridLayout(QtGui.QWidget(self))

        self.sc = ReceivedPacketPerNetworkPlot('Paquetes recibidos por Red',"Paquetes ( p)","Nombre de la red",1,QtGui.QWidget(self))
        self.navi_toolbar = NavigationToolbar(self.sc, self)
        self.sc3 = SendedPacketPerNetworkPlot('Paquetes enviados por Red',"Paquetes ( p)","Nombre de la red",1,QtGui.QWidget(self))
        self.navi_toolbar3 = NavigationToolbar(self.sc3, self)
        self.sc2 = RealTimePlot('Paquetes totales recibidos/Segundo',"Paquetes (p)","Segundos (t)",'go-','Paquetes recibidos',QtGui.QWidget(self), width=5, height=4, dpi=100)
        self.navi_toolbar2 = NavigationToolbar(self.sc2, self)
        self.sc4 = RealTimePlot('Paquetes totales enviados/Segundo',"Paquetes (p)","Segundos (t)",'rs-','Paquetes enviados',QtGui.QWidget(self), width=5, height=4, dpi=100)
        self.navi_toolbar4 = NavigationToolbar(self.sc4, self)


        l.addWidget(self.sc,0,1)
        l.addWidget(self.navi_toolbar,1,1)
        l.addWidget(self.sc3,0,2)
        l.addWidget(self.navi_toolbar3,1,2)
        l.addWidget(self.sc2,2,1)
        l.addWidget(self.navi_toolbar2,3,1)
        l.addWidget(self.sc4,2,2)
        l.addWidget(self.navi_toolbar4,3,2)


        self.estadisticas = QtGui.QListWidget()
        self.estadisticas.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
        self.estadisticas.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.estadisticas.show()
        self.estadisticas.setMinimumSize(600,800)

        txtStatistics = QtGui.QLabel('Networks Statistics')
        txtStatistics.setStyleSheet('color: #049DBF; font-size: 20pt; font-family: Georgia;')

        csv = QtGui.QPushButton('Export to CSV', self)
        csv.setCheckable(True)
        csv.setMaximumSize(200,200)
        csv.setToolTip('Click to export to CSV')
        csv.setStyleSheet("background-color: white; color: #049DBF;border-style: outset;border-width: 1px;border-radius: 5px;border-color: black;font: bold 12px;min-width: 10em;padding: 6px;")
        csv.clicked.connect(self.exportToCSV)

        tab1	= QtGui.QWidget()
        tab2	= QtGui.QWidget()
        tab3	= QtGui.QWidget()

        vBoxlayout	= QtGui.QVBoxLayout()
        vBoxlayout.addWidget(txtStatistics)
        vBoxlayout.addWidget(self.estadisticas)
        vBoxlayout.addWidget(csv)
        vBoxlayout.setAlignment(txtStatistics,Qt.AlignCenter)
        vBoxlayout.setAlignment(csv,Qt.AlignCenter)
        vBoxlayout.setAlignment(self.estadisticas,Qt.AlignCenter)


        #Set Layout for Third Tab Page
        tab3.setLayout(vBoxlayout)
        tab1.setLayout(grid)
        tab2.setLayout(l)
        self.addTab(tab1,"Network Connections")
        self.addTab(tab2,"Plots")
        self.addTab(tab3,"Statistics")

        self.managedStatistics()
    
     
    @pyqtSlot()
    def on_pushButtonSetBase_toggled(self):
        
         """
         Funcion definición del funcionamiento del boton Connect
         @return: None
         """
     
         formatString = ColorStringClass()    
         if self.pulsado == True:
            self.btn.setText("Disconnect")
            self.pulsado = False
            if self.udp.isChecked() == True:
                self.connected = True
                self.txtEdit.append(formatString.messageFormatGray("Connected by UDP, waiting Datagrams..."))
                self.mainPort = threading.Thread(target=self.connectionMainPort,args=())
                self.mainPort.start()                                                
            
            elif self.tcp.isChecked() == True:
                self.txtEdit.append(formatString.messageFormatGray("Connected by TCP, waiting for a connection..."))
                self.protocol.setTypeOf(1)
                self.connected = True
                self.mainPort = threading.Thread(target=self.connectionMainPort,args=())
                self.mainPort.start()                                             
                
                            
                
         else:
             reply = QtGui.QMessageBox.question(self, 'Warning',
             "Do you want to close all connections?", QtGui.QMessageBox.Yes | 
             QtGui.QMessageBox.No, QtGui.QMessageBox.No)

             if reply == QtGui.QMessageBox.Yes:
                self.btn.setText("Connect")
                self.listarchivos.clear()
                self.pulsado = True
                self.txtEdit.append(formatString.messageFormatGray("Connection disconnected and socket closed"))
                self.protocol.closeConnection()
         
            
            


    def center(self):
        """
        Funcion para centrar la interfaz grafica en la pantalla
        """
        frameGm = self.frameGeometry()
        screen = QtGui.QApplication.desktop().screenNumber(QtGui.QApplication.desktop().cursor().pos())
        centerPoint = QtGui.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())
        
        
    def closeEvent(self, event):
        
         """
         Función llamada cuando detecta el evento de cerrado
         @param event: Evento que recoge el cierre
         @type: Event
         @return: None
         """
         reply = QtGui.QMessageBox.question(self, 'Warning',
            "Do you want to close this program?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

         if reply == QtGui.QMessageBox.Yes:
            self.protocol.closeConnection()
            gc.collect()
            event.accept()
         else:
            event.ignore()

    def NewNetworkConnection(self, nthread):
        
        """
         Función que abre el puerto UDP para establecer comunicación con un servicio o red externa
         @return: None
         """
        formatString = ColorStringClass()
        if self.protocol.getTypeOf() == 0:
            returnData = self.data
        elif self.protocol.getTypeOf() == 1:
            returnData = self.dataTCP
        vectorParser = VectorParser()
        newVector = vectorParser.stringToVector(returnData)
        inport = self.protocol.generateInPort(self.portList)
        outPort = self.protocol.generateOutPort(self.portList)
        newNetwork = Network(newVector[3],newVector[0], str(inport), str(outPort))
        self.netList.append(newNetwork)
        items = self.listarchivos.findItems(newNetwork.getName(),QtCore.Qt.MatchContains)
        gc.collect()

        if len(items) > 0:
            self.txtEdit.append(formatString.messageFormatRed("Already exist a network with this name connected: " + newNetwork.getName()))
        else:
            with self.conditionReceived:
                comprobar = self.networksDataBase.getNetworkByName(newNetwork.getName())
                if comprobar == None:
                    print "Guardando red"
                    self.networksDataBase.saveNetwork(newNetwork)
                else:
                    print "Cambiando puertos"
                    self.networksDataBase.setNewPorts(newNetwork)
            checkSourceIP = newNetwork.getIP().split('.')
            correctIP = True
            for octeto in checkSourceIP:
                if(int(octeto) < 0 or int(octeto) > 255):
                    correctIP = False
            if correctIP == False:
                self.txtEdit.append(formatString.messageFormatRed("IP has to be in range 0.0.0.0 255.255.255.255"))
                
            else:
                item = QtGui.QListWidgetItem("Name: " + newNetwork.getName() + "\nIn: "+newNetwork.getInPort()+"\nOut: "+newNetwork.getOutPort())
                self.listarchivos.addItem(item)
                self.txtEdit.append(formatString.messageFormatGreen("New network added " + newNetwork.getName()))
                self.txtEdit.append(formatString.messageFormatBlue("Received message: " + repr(newVector)))
                helloResponse= newNetwork.getIP()+","+newNetwork.getIP()+",hello,"+ newNetwork.getName()+","+newNetwork.getInPort()+","+newNetwork.getOutPort()
                self.protocol.sendToHello(helloResponse, 8050)
            
                finalSockExtern = Protocol(self.protocol.getTypeOf(), newNetwork.getIP(), int(newNetwork.getInPort()),int(newNetwork.getOutPort()))
          
                try:
                    finalSockExtern.openConnection(newNetwork)
                except:
                    print "Ya conectado"
                finally:
                    t = threading.Thread(target=self.receivedFrom,args=(finalSockExtern,newNetwork,nthread,))
                    t.daemon = True
                    t.start()
                    self.txtEdit.append(formatString.messageFormatBlue("Assigned ports to the new network " + newNetwork.getName()))



    def receivedFrom(self, finalSockExtern, newNetwork,nthread):
       """
       Función de recepción de cada red individual
       """
       formatString = ColorStringClass()
       
       while True:
           if self.protocol.getTypeOf() == 0:
               data, addr = finalSockExtern.receiveFrom() # bufer size is 1024 bytes
               data = data +  "," + str(nthread)
           elif self.protocol.getTypeOf() == 1:
               data = finalSockExtern.receiveFrom() # bufer size is 1024 bytes
               self.connectionQueue.put(finalSockExtern)
               data = data +  "," + str(nthread)
               self.queueList[nthread-1].put(data)

           with self.conditionPackets:
               self.totalPackets = self.totalPackets + 1       
               if self.totalPackets == 1:
                   self.firstTime = time.time() * 1000
           self.queue.put(data)
           self.receivedList[nthread-1] = self.receivedList[nthread-1] + 1
           #print "Network " + str(nthread) + " received " + str(self.receivedList[int(nthread)-1] )



    def myTurn(self,nthread):
        """
        Comprueba si la red esta con su turno activo y si lo es encola lo que pueda
        """ 
        while(True):
                    try:
                        if self.actualTurn != nthread:
                            self.condition.acquire()
                            self.condition.wait()

                        else:
                            self.queueManagement()
                        self.condition.release()

                    except:
                        gc.collect()
                    finally:
                        gc.collect()
                        sleep(self.timeRanure)

    def connectionMainPort(self):
        """
        Función para conectar con el puerto general al que van a llegar todas las nuevas redes.
        @return: None
        """
        formatString = ColorStringClass()
        self.mainInPort = self.inPortExtern.text()
        self.mainOutPort = self.outPortExtern.text()
        if self.mainInPort == "":
            self.mainInPort = 8050
        if self.mainOutPort == "":
            self.mainOutPort = 8051

        try:
            self.protocol.setInPort(int(self.mainInPort))
            self.protocol.setOutPort(int(self.mainOutPort))
            self.protocol.openMainConnection()
            self.packetsSecond = threading.Thread(target=self.packetsPerSecond,args=())
            self.packetsSecond.start()
            for i in range(NUMTHREAD):
                self.t = threading.Thread(target=self.myTurn,args=(i,))
                self.t.daemon = True
                self.threadList.append(self.t)
                self.t.start()

            self.turns = threading.Thread(target=self.TDMATurns,args=())
            self.turns.daemon = True
            self.turns.start()

            while True:
                if self.protocol.getTypeOf() == 0:
                    gc.collect()
                    self.data, addr = self.protocol.receiveFromMain() # buffer size is 1024 bytes
                elif self.protocol.getTypeOf() == 1:
                    gc.collect()
                    self.dataTCP = self.protocol.receiveFromMain()
                self.nThreads = self.nThreads + 1
                num = self.nThreads
                self.n = threading.Thread(target=self.NewNetworkConnection,args=(num,))
                self.n.daemon = True
                self.n.start()
                self.receivedList.append(0)
                self.sendedList.append(0)

        except:
            self.txtEdit.append(formatString.messageFormatRed("Error connecting with sockets, try to change the ports"))
        
    def queueManagement(self):
        
        """
        Función administradora de la cola de peticiones, encargada de enroutarlas a su destino
        @return: None
        """
        while True:
            try:
                if self.queue.empty() == False:
                    element = self.queue.get()
                    self.queue.task_done()

                    vectorParser = VectorParser()
                    newVector = vectorParser.stringToVector(element)
                    """
                    checkSourceIP = newVector[1].split('.')
                    correctIP = True
                    for octeto in checkSourceIP:
                        if(int(octeto) < 0 or int(octeto) > 255):
                            correctIP = False
                    if correctIP == False:
                        self.txtEdit.append(formatString.messageFormatRed("ERROR: "+ResponseMessage.ERROR_IP_MESSAGE))
                        responseMessage.setStatusCode(ResponseMessage.STATUS_KO)
                        responseMessage.setErrorCode(ResponseMessage.ERROR_IP)
                        responseMessage.setMessage(ResponseMessage.ERROR_IP_MESSAGE)

                    elif len(newVector) != 5:
                        self.txtEdit.append(formatString.messageFormatRed("ERROR: "+ResponseMessage.ERROR_ARGUMENTS_MESSAGE))
                        responseMessage.setStatusCode(ResponseMessage.STATUS_KO)
                        responseMessage.setErrorCode(ResponseMessage.ERROR_ARGUMENTS)
                        responseMessage.setMessage(ResponseMessage.ERROR_ARGUMENTS_MESSAGE)
                    else:
                        responseMessage.setStatusCode(ResponseMessage.STATUS_OK)
                        responseMessage.setErrorCode(ResponseMessage.ERROR_OK)
                        responseMessage.setMessage(newVector[2])

                    responseMessageStr = responseMessage.getStatusCode()+","+responseMessage.getErrorCode()+","+responseMessage.getMessage()
                    """
                    for net in self.netList:
                        if net.getName() == newVector[3]:
                            if self.protocol.getTypeOf() == 0:
        
                                self.do_work(element,net.getIP(),int(net.getInPort()))
                                #connection.sendto(element,("127.0.0.1",int(newNetwork.getInPort())))
                            elif self.protocol.getTypeOf() == 1:
                                self.connectionQueue.dequeue().sendFromQueue(element,newNetwork,newVector[1])
                            
                            if self.totalPackets >= (NUMPACKETS * NUMNETWORKS) - 1000:
                              self.secondTime = time.time() * 1000
                              total = (self.secondTime - self.firstTime)
                              print "Tiempo total recibiendo y enviando " + str(total)
                            self.sendedList[int(newVector[4])-1] = self.sendedList[int(newVector[4])-1] + 1

            except:
                gc.collect()



    def do_work(self,item, IP,inPort):
        """
        Envia el paquete abriendo un socket y cerrandolo cuando se finaliza la comunicacion
        """
        addr = (IP, inPort)

        # Create a socket (SOCK_STREAM means a TCP socket)
        # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        try:
            # Connect to server and send data
            #sock.bind((HOST, PORT))
            sock.sendto(item,addr)

        finally:
            sock.close()

    def TDMATurns(self):
        """
        Funcion planificadora de turnos, cada X tiempo dependiendo de la carga de cada red
        """
        while True:
            self.condition.acquire()
            if self.actualTurn >= NUMTHREAD:
                self.actualTurn = 1
            else:
                self.actualTurn = self.actualTurn + 1


            time = self.assignTime(self.actualTurn)
            self.condition.notifyAll()
            sleep(time)
            self.condition.release()




    def assignTime(self, actualThread):
        """
        Funcion que dependiendo de la carga de la red la concede mas o menos ranuras de tiempo
        """
        try:
            menor = self.menor_lista_(self.sendedList)
            if (menor == self.sendedList[actualThread-1]):
                return TIMETODESPATCHQUEUE * (3*self.numRanures)
            else:
                return TIMETODESPATCHQUEUE
        except:    
            return TIMETODESPATCHQUEUE * self.numRanures

    def menor_lista_(self, lista):
        """
        Funcion que busca la red que menos a enviado en la lista
        """
        menor = lista[0]
        for valor in lista:
            if valor < menor:
                menor = valor
        return menor

    def packetsPerSecond(self):
        """
        Funcion encargada de calcular el numero de paquetes por segundo del sistema
        """
        i = 1
        for net in self.receivedList:
            self.packetsReceived = self.packetsReceived + net
            self.packetsSend = self.packetsSend + self.sendedList[i-1]
            with self.conditionReceived:
                self.networksDataBase.addReceivedSendedPackets(i,net,self.sendedList[i-1])
            i = i + 1

        self.sc2.update_data(self.packetsReceived)
        self.sc4.update_data(self.packetsSend)
        self.packetsReceived = 0
        self.packetsSend = 0
        threading.Timer(1, self.packetsPerSecond).start()

    def PrintClick(self):
        """
        Función que abre una instancia de la clase NetworkDetail donde se nos muestran los detalles de de la red seleccionada 
        @return: None
        """
        formatString = ColorStringClass()
        self.txtEdit.append(formatString.messageFormatGray("Network selected: " + self.listarchivos.selectedItems()[0].text()))
        self.dialogTextBrowser = NetworkDetail(self,self.listarchivos.selectedItems()[0].text(),self.listarchivos,self.txtEdit)
        self.dialogTextBrowser.exec_()

    def managedStatistics(self):
        """
        Funcion encargada de actualizar y calcular las estadisticas del sistema
        """
        dataBase = XMLDataBase()
        networkList = dataBase.getAllNetworks()
        for newNetwork in networkList:

            packetReceivedPercent = (float(newNetwork.getReceivedPackets())/dataBase.getAllPacketsReceived())*100
            packetSendedPercent = (float(newNetwork.getSendedPackets())/dataBase.getAllPacketsSended())*100
            lostReceivedPackets = NUMPACKETS - int(newNetwork.getReceivedPackets())
            lostSendedPackets = int(newNetwork.getReceivedPackets()) - int(newNetwork.getSendedPackets())
            lostReceivedPacketsPercent = (float(lostReceivedPackets)/NUMPACKETS)*100
            lostSendedPacketsPercent = (float(lostSendedPackets)/float(newNetwork.getReceivedPackets()))*100

            item = QtGui.QListWidgetItem("\n" + newNetwork.getName() + "\n\nReceived Packets: "+newNetwork.getReceivedPackets()+"\n\nSended Packets: "+newNetwork.getSendedPackets() + "\n\nLost Received Packets: " + str(lostReceivedPackets) + "\n\nLost Sended Packets: " + str(lostSendedPackets) + "\n\n Percent of Total Received Packets: " + str(round(packetReceivedPercent,2)).replace('.', ',') + "%"  + "\n\n Percent of Total Sended Packets: " + str(round(packetSendedPercent,2)).replace('.', ',')+ "%" + "\n\n Percent of Total Lost Received Packets: " + str(round(lostReceivedPacketsPercent,2)).replace('.', ',') + "%"  + "\n\n Percent of Total Lost Sended Packets: " + str(round(lostSendedPacketsPercent,2)).replace('.', ',') + "%"  + "\n")
            self.estadisticas.addItem(item)


    def exportToCSV(self):
        """
        Funcion que permite exportar a CSV las estadisticas
        """
        dataBase = XMLDataBase()
        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save File',"networksStatistics", os.getenv('HOME'))
        filename = unicode(filename)
        networkList = dataBase.getAllNetworks()
        archi=open(filename + ".csv",'a')
        archi.write('"Network Name","Received Packets","Sended Packets","Lost Received Packets","Lost Sended Packets","Percent of Total Received Packets","Percent of Total Sended Packets","Percent of Total Lost Received Packets","Percent of Total Lost Sended Packets"\n')
        for newNetwork in networkList:
            packetReceivedPercent = (float(newNetwork.getReceivedPackets())/dataBase.getAllPacketsReceived())*100
            packetSendedPercent = (float(newNetwork.getSendedPackets())/dataBase.getAllPacketsSended())*100
            lostReceivedPackets = NUMPACKETS - int(newNetwork.getReceivedPackets())
            lostSendedPackets = int(newNetwork.getReceivedPackets()) - int(newNetwork.getSendedPackets())
            lostReceivedPacketsPercent = (float(lostReceivedPackets)/NUMPACKETS)*100
            lostSendedPacketsPercent = (float(lostSendedPackets)/float(newNetwork.getReceivedPackets()))*100
            archi.write('"'+newNetwork.getName()+'";"'+newNetwork.getReceivedPackets()+'";"'+newNetwork.getSendedPackets()+'";"'+str(lostReceivedPackets)+'";"'+str(lostSendedPackets)+'";"'+str(round(packetReceivedPercent,2)).replace('.', ',')+'";"'+str(round(packetSendedPercent,2)).replace('.', ',')+'";"' + str(round(lostReceivedPacketsPercent,2)).replace('.', ',')+'";"' + str(round(lostSendedPacketsPercent,2)).replace('.', ',') + '"\n')


        archi.close()

"""
Main de la aplicación
"""

    
if __name__ == '__main__':
 
    """
    Función main
    @return: None
    """
    
    app = QtGui.QApplication(sys.argv)
    css = """
    
    QWidget
    {
    font:12px bold;
    font-weight:bold;
    border-radius: 1px;
    height: 11px;
    
    }
    QDialog{
    Background-image:url('img/titlebar bg.png');
    font-size:12px;
    color: black;
    
    }
    QToolButton:hover{
    Background:n #049DBF;
    border: 1px solid black;
    font-size:11px;
    }
    
    QTextEdit{
        border: 3px solid #049DBF;
        background-color: #1A2326;
    }
    
    QPushButton{
        border: 1px solid black;    
    }
    
    QListWidget::item {
     border-style: solid;
     border-width:1px;
     border-color:#ffffff;
     color: #ffffff;
    }
    QListWidget{
        border: 3px solid #049DBF;
        background-color: #1A2326;
    
    }
    QTabWidget{background-color: #FFFFFF;border: 1px solid black;}
    """

    app.setStyleSheet(css)
    window = MainWindow()
    window.show()
    
    sys.exit(app.exec_())
    
    
