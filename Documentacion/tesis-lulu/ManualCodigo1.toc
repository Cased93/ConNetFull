\select@language {english}
\select@language {english}
\vspace {-\cftbeforepartskip }
\select@language {spanish}
\contentsline {chapter}{\numberline {1}\spacedlowsmallcaps {Manual de C\'odigo}}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}C\'odigo clase ConNet para la versi\'on con Env\IeC {\'\i }o Directo}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}C\'odigo clase ConNet para la versi\'on con Round Robin}{17}{section.1.2}
\contentsline {section}{\numberline {1.3}C\'odigo clase ConNet para la versi\'on con TDMA}{35}{section.1.3}
\contentsline {section}{\numberline {1.4}C\'odigo clase XMLDataBase}{53}{section.1.4}
\contentsline {section}{\numberline {1.5}C\'odigo clase VectorParser}{59}{section.1.5}
\contentsline {section}{\numberline {1.6}C\'odigo ResponseMessage}{60}{section.1.6}
\contentsline {section}{\numberline {1.7}C\'odigo clase Protocol}{62}{section.1.7}
\contentsline {section}{\numberline {1.8}C\'odigo clase Plots}{66}{section.1.8}
\contentsline {section}{\numberline {1.9}C\'odigo clase NetworkDetail}{70}{section.1.9}
\contentsline {section}{\numberline {1.10}C\'odigo clase Network}{73}{section.1.10}
\contentsline {section}{\numberline {1.11}C\'odigo clase ColorStringClass}{75}{section.1.11}
\contentsline {section}{\numberline {1.12}C\'odigo fichero configuraci\'on}{77}{section.1.12}
\contentsline {section}{\numberline {1.13}C\'odigo fichero de conexi\'on de redes de prueba}{77}{section.1.13}
