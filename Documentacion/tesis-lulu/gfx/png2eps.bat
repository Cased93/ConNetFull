@echo off
rem converts png to eps
rem  (image must have square pixels, x resolution = y resolution)
rem
rem 2003-04-09 Thomas Henlich
rem
rem requires: pngtopnm pnmtotiff tiff2ps
rem
rem Usage:
rem png2eps file.png height resolution file.eps
rem   height of image in pixels,
rem   resolution in dpi,
rem    both can be determined e.g. with "pngcheck -v file.png"

rem Replace -packbits with the compression scheme of your choice, e. g. -lzw.

pngtopnm -verbose %1 | pnmtotiff -rowsperstrip %2 -xres %3 -yres %3 -packbits -indexbits=1,2,4,8 >tmp.tif
tiff2ps -2 -e -z tmp.tif >%4
del tmp.tif
