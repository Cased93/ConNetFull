\select@language {english}
\select@language {english}
\vspace {-\cftbeforepartskip }
\select@language {spanish}
\contentsline {part}{i\hspace {1em}\spacedlowsmallcaps {Introducci\'on}}{1}{part.1}
\contentsline {chapter}{\numberline {1}\spacedlowsmallcaps {Introducci\'on}}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}\spacedlowsmallcaps {Definici\'on del Problema}}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Identificaci\'on del problema real}{8}{section.2.1}
\contentsline {section}{\numberline {2.2}Identificaci\'on del problema t\'ecnico}{8}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Funcionamiento}{8}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Entorno}{9}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Vida Esperada}{9}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Ciclo de Mantenimiento}{10}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Competencia}{10}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Aspecto Externo}{10}{subsection.2.2.6}
\contentsline {subsection}{\numberline {2.2.7}Estandarizaci\'on}{11}{subsection.2.2.7}
\contentsline {subsection}{\numberline {2.2.8}Calidad y Fiabilidad}{11}{subsection.2.2.8}
\contentsline {subsection}{\numberline {2.2.9}Programa de Tareas}{11}{subsection.2.2.9}
\contentsline {subsection}{\numberline {2.2.10}Pruebas}{12}{subsection.2.2.10}
\contentsline {subsection}{\numberline {2.2.11}Seguridad}{12}{subsection.2.2.11}
\contentsline {chapter}{\numberline {3}\spacedlowsmallcaps {Objetivos}}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Objetivo Principal}{14}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Creaci\'on de Middleware de Comunicaciones}{14}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Objetivos secundarios}{14}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Algoritmos Round Robin y TDMA}{14}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Conectar distintas Redes}{15}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Conclusi\'on de los Objetivos}{15}{section.3.3}
\contentsline {chapter}{\numberline {4}\spacedlowsmallcaps {Antecedentes}}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}DDS}{18}{section.4.1}
\contentsline {section}{\numberline {4.2}Connext DDS de RTI}{18}{section.4.2}
\contentsline {section}{\numberline {4.3}OMNeT++}{18}{section.4.3}
\contentsline {section}{\numberline {4.4}SBR: Sistemas Basados en Reglas}{20}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Modus Ponens}{20}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Modus Tollens}{21}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Estrategias de inferencia}{21}{subsection.4.4.3}
\contentsline {section}{\numberline {4.5}Internet of Things (IoT)}{21}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Double Stack}{22}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Tunelizaci\'on IPv6}{23}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Est\'andares de Comunicaci\'on}{23}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Internet Protocol (IP)}{23}{subsection.4.6.1}
\contentsline {subsubsection}{IPv4}{24}{figure.4.4}
\contentsline {subsubsection}{IPv6}{26}{figure.4.5}
\contentsline {subsection}{\numberline {4.6.2}Est\'andar IEEE 802.15.4}{28}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}User Datagram Protocol (UDP)}{30}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Transmission Control Protocol (TCP)}{31}{subsection.4.6.4}
\contentsline {section}{\numberline {4.7}Round Robin}{33}{section.4.7}
\contentsline {section}{\numberline {4.8}Acceso m\'ultiple por divisi\'on de tiempo (TDMA)}{34}{section.4.8}
\contentsline {chapter}{\numberline {5}\spacedlowsmallcaps {Restricciones}}{37}{chapter.5}
\contentsline {section}{\numberline {5.1}Factores Dato}{38}{section.5.1}
\contentsline {section}{\numberline {5.2}Factores Estrat\'egicos}{39}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Sistema Operativo}{39}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Desarrollo de la Interfaz Gr\'afica}{40}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Librer\IeC {\'\i }a para el dise\~no de gr\'aficas en tiempo real}{40}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Librer\IeC {\'\i }a redes de sensores en OMNeT++}{41}{subsection.5.2.4}
\contentsline {chapter}{\numberline {6}\spacedlowsmallcaps {Recursos}}{43}{chapter.6}
\contentsline {section}{\numberline {6.1}Recursos Humanos}{43}{section.6.1}
\contentsline {section}{\numberline {6.2}Recursos Hardware}{43}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Sistema de Desarrollo}{44}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Sistema de Explotaci\'on}{44}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Recursos Software}{44}{section.6.3}
\contentsline {part}{ii\hspace {1em}\spacedlowsmallcaps {An\'alisis del Sistema}}{47}{part.2}
\contentsline {chapter}{\numberline {7}\spacedlowsmallcaps {Especificaci\'on de Requisitos}}{49}{chapter.7}
\contentsline {section}{\numberline {7.1}Requisitos de Informaci\'on}{50}{section.7.1}
\contentsline {section}{\numberline {7.2}Requisitos Funcionales}{52}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Middleware de comunicaciones de redes de sensores y sistemas}{54}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Caso de uso 1: Selecci\'on de protocolo de comunicaci\'on}{55}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Caso de uso 2: Selecci\'on de puertos de entrada y salida}{55}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}Caso de uso 3: Conexi\'on del sistema}{56}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}Caso de uso 4: A\~nadir nueva red}{56}{subsection.7.2.5}
\contentsline {subsection}{\numberline {7.2.6}Caso de uso 5: Gestionar una red existente}{56}{subsection.7.2.6}
\contentsline {subsection}{\numberline {7.2.7}Caso de uso 6: Guardar gr\'afica como imagen}{57}{subsection.7.2.7}
\contentsline {subsection}{\numberline {7.2.8}Caso de uso 7: Exportar fichero CSV de estad\IeC {\'\i }sticas}{57}{subsection.7.2.8}
\contentsline {section}{\numberline {7.3}Requisitos no Funcionales}{58}{section.7.3}
\contentsline {chapter}{\numberline {8}\spacedlowsmallcaps {An\'alisis Funcional}}{59}{chapter.8}
\contentsline {section}{\numberline {8.1}Descripci\'on Arquitect\'onica}{60}{section.8.1}
\contentsline {section}{\numberline {8.2}Componentes del Sistema}{61}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Componente: Gestor de Protocolos}{61}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Componente: Enrutador de redes}{61}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Componente: Gestor de puertos}{62}{subsection.8.2.3}
\contentsline {subsection}{\numberline {8.2.4}Componente: Gestor de redes}{62}{subsection.8.2.4}
\contentsline {subsection}{\numberline {8.2.5}Componente: Visor de redes}{62}{subsection.8.2.5}
\contentsline {subsection}{\numberline {8.2.6}Componente: Gestor de la informaci\'on}{63}{subsection.8.2.6}
\contentsline {subsection}{\numberline {8.2.7}Componente: Visor de la informaci\'on}{63}{subsection.8.2.7}
\contentsline {section}{\numberline {8.3}Descripci\'on del Comportamiento}{63}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Diagrama de Secuencia 1: Selecci\'on de Protocolo}{63}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Diagrama de Secuencia 2: Selecci\'on de Puertos}{64}{subsection.8.3.2}
\contentsline {subsection}{\numberline {8.3.3}Diagrama de Secuencia 3: Conexi\'on de la comunicaci\'on}{64}{subsection.8.3.3}
\contentsline {subsection}{\numberline {8.3.4}Diagrama de Secuencia 4: Listar redes}{64}{subsection.8.3.4}
\contentsline {subsection}{\numberline {8.3.5}Diagrama de Secuencia 5: Enrutar redes}{65}{subsection.8.3.5}
\contentsline {subsection}{\numberline {8.3.6}Diagrama de Secuencia 6: A\~nadir red}{66}{subsection.8.3.6}
\contentsline {subsection}{\numberline {8.3.7}Diagrama de Secuencia 7: Modificar red}{66}{subsection.8.3.7}
\contentsline {part}{iii\hspace {1em}\spacedlowsmallcaps {Dise\~no de la Interfaz}}{67}{part.3}
\contentsline {chapter}{\numberline {9}\spacedlowsmallcaps {Dise\~no Procedimental}}{69}{chapter.9}
\contentsline {section}{\numberline {9.1}Dise\~no Procedimental}{70}{section.9.1}
\contentsline {subsection}{\numberline {9.1.1}Clase Network}{70}{subsection.9.1.1}
\contentsline {subsection}{\numberline {9.1.2}Clase Protocol}{71}{subsection.9.1.2}
\contentsline {subsection}{\numberline {9.1.3}Clase XMLDataBase}{73}{subsection.9.1.3}
\contentsline {subsection}{\numberline {9.1.4}Clase VectorParser}{74}{subsection.9.1.4}
\contentsline {subsection}{\numberline {9.1.5}Clase ResponseMessage}{75}{subsection.9.1.5}
\contentsline {subsection}{\numberline {9.1.6}Clase ColorStringClass}{75}{subsection.9.1.6}
\contentsline {subsection}{\numberline {9.1.7}NetworkDetail}{76}{subsection.9.1.7}
\contentsline {subsection}{\numberline {9.1.8}Plots}{77}{subsection.9.1.8}
\contentsline {subsection}{\numberline {9.1.9}ReceivedPacketPerNetworkPlot}{78}{subsection.9.1.9}
\contentsline {subsection}{\numberline {9.1.10}SendedPacketPerNetworkPlot}{79}{subsection.9.1.10}
\contentsline {subsection}{\numberline {9.1.11}RealTimePlot}{80}{subsection.9.1.11}
\contentsline {subsection}{\numberline {9.1.12}Queue}{81}{subsection.9.1.12}
\contentsline {subsection}{\numberline {9.1.13}Clase ConNet}{82}{subsection.9.1.13}
\contentsline {chapter}{\numberline {10}\spacedlowsmallcaps {Dise\~no de la Interfaz de Usuario}}{85}{chapter.10}
\contentsline {section}{\numberline {10.1}Aspecto General de la Interfaz}{86}{section.10.1}
\contentsline {section}{\numberline {10.2}Gesti\'on de Protocolos}{87}{section.10.2}
\contentsline {section}{\numberline {10.3}Gesti\'on de Puertos}{88}{section.10.3}
\contentsline {section}{\numberline {10.4}Gesti\'on de la Conexi\'on}{88}{section.10.4}
\contentsline {section}{\numberline {10.5}Gesti\'on de la Informaci\'on}{89}{section.10.5}
\contentsline {section}{\numberline {10.6}Gesti\'on de las Redes}{90}{section.10.6}
\contentsline {subsection}{\numberline {10.6.1}Detalles de una Red}{91}{subsection.10.6.1}
\contentsline {subsubsection}{Borrar Red}{91}{figure.10.9}
\contentsline {section}{\numberline {10.7}Gr\'aficas}{92}{section.10.7}
\contentsline {subsection}{\numberline {10.7.1}Gr\'afica 1: Histograma de paquete recibidos por red}{92}{subsection.10.7.1}
\contentsline {subsection}{\numberline {10.7.2}Gr\'afica 2: Histograma de paquete enviados por red}{92}{subsection.10.7.2}
\contentsline {subsection}{\numberline {10.7.3}Gr\'afica 3: L\IeC {\'\i }nea de puntos en tiempo real de paquete recibidos totales}{93}{subsection.10.7.3}
\contentsline {subsection}{\numberline {10.7.4}Gr\'afica 4: L\IeC {\'\i }nea de puntos en tiempo real de paquete enviados totales}{93}{subsection.10.7.4}
\contentsline {section}{\numberline {10.8}Gesti\'on de Estad\IeC {\'\i }sticas}{94}{section.10.8}
\contentsline {subsection}{\numberline {10.8.1}Exportar a formato .CSV}{95}{subsection.10.8.1}
\contentsline {part}{iv\hspace {1em}\spacedlowsmallcaps {Desarrollo de Pruebas}}{97}{part.4}
\contentsline {chapter}{\numberline {11}\spacedlowsmallcaps {Pruebas del Sistema}}{99}{chapter.11}
\contentsline {section}{\numberline {11.1}Pruebas de Casos de Uso}{100}{section.11.1}
\contentsline {subsection}{\numberline {11.1.1}Pruebas Caso de Uso 1: Selecci\'on de protocolo de comunicaci\'on}{100}{subsection.11.1.1}
\contentsline {subsection}{\numberline {11.1.2}Pruebas Caso de Uso 2: Selecci\'on de puertos de entrada y salida}{100}{subsection.11.1.2}
\contentsline {subsection}{\numberline {11.1.3}Pruebas Caso de Uso 3: Conexi\'on del sistema}{101}{subsection.11.1.3}
\contentsline {subsection}{\numberline {11.1.4}Pruebas Caso de Uso 4: A\~nadir nueva red}{101}{subsection.11.1.4}
\contentsline {subsection}{\numberline {11.1.5}Pruebas Caso de Uso 5: Gestionar una red existente}{101}{subsection.11.1.5}
\contentsline {subsection}{\numberline {11.1.6}Pruebas Caso de Uso 6: Guardar gr\'afica como imagen}{101}{subsection.11.1.6}
\contentsline {subsection}{\numberline {11.1.7}Pruebas Caso de Uso 7: Exportar fichero CSV de estad\IeC {\'\i }sticas}{102}{subsection.11.1.7}
\contentsline {section}{\numberline {11.2}Pruebas de Escenarios de la Aplicaci\'on}{102}{section.11.2}
\contentsline {subsection}{\numberline {11.2.1}Caso de prueba 1: Selecci\'on de protocolo de comunicaci\'on}{102}{subsection.11.2.1}
\contentsline {subsection}{\numberline {11.2.2}Caso de prueba 2: Selecci\'on de puertos de entrada y salida}{103}{subsection.11.2.2}
\contentsline {subsection}{\numberline {11.2.3}Caso de prueba 3: Conexi\'on del sistema}{103}{subsection.11.2.3}
\contentsline {subsection}{\numberline {11.2.4}Caso de prueba 4: A\~nadir nueva red}{104}{subsection.11.2.4}
\contentsline {subsection}{\numberline {11.2.5}Caso de prueba 5: Gestionar una red existente}{104}{subsection.11.2.5}
\contentsline {subsection}{\numberline {11.2.6}Caso de prueba 6: Guardar gr\'afica como imagen}{105}{subsection.11.2.6}
\contentsline {subsection}{\numberline {11.2.7}Caso de prueba 7: Exportar fichero CSV de estad\IeC {\'\i }sticas}{105}{subsection.11.2.7}
\contentsline {section}{\numberline {11.3}Pruebas de B\'usqueda de Valores \'Optimos}{105}{section.11.3}
\contentsline {subsection}{\numberline {11.3.1}Versi\'on con Env\IeC {\'\i }o Directo}{106}{subsection.11.3.1}
\contentsline {subsection}{\numberline {11.3.2}Versi\'on con Round Robin}{106}{subsection.11.3.2}
\contentsline {subsection}{\numberline {11.3.3}Versi\'on con TDMA}{106}{subsection.11.3.3}
\contentsline {section}{\numberline {11.4}Pruebas de Inyecci\'on de Errores}{107}{section.11.4}
\contentsline {section}{\numberline {11.5}Pruebas de Variaci\'on de Carga}{107}{section.11.5}
\contentsline {section}{\numberline {11.6}Pruebas de Interconexi\'on con Sistemas}{107}{section.11.6}
\contentsline {subsection}{\numberline {11.6.1}Prueba de Conexi\'on con el SBR}{107}{subsection.11.6.1}
\contentsline {subsection}{\numberline {11.6.2}Prueba de Interconexi\'on de Redes Simuladas}{108}{subsection.11.6.2}
\contentsline {chapter}{\numberline {12}\spacedlowsmallcaps {Resultados}}{111}{chapter.12}
\contentsline {section}{\numberline {12.1}Resultados de las Pruebas de B\'usqueda de Valores \'Optimos}{112}{section.12.1}
\contentsline {subsection}{\numberline {12.1.1}Pruebas sobre la Versi\'on con Round Robin}{112}{subsection.12.1.1}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.05}{112}{subsection.12.1.1}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.05}{113}{figure.12.1}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.05}{113}{figure.12.2}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.05}{114}{figure.12.3}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05}{114}{figure.12.4}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.05}{115}{figure.12.5}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.025}{115}{figure.12.6}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.025}{116}{figure.12.7}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.025}{116}{figure.12.8}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.025}{117}{figure.12.9}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025}{117}{figure.12.10}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.025}{118}{figure.12.11}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.01}{118}{figure.12.12}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.01}{119}{figure.12.13}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.01}{119}{figure.12.14}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.01}{119}{figure.12.15}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.01}{120}{figure.12.16}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.01}{120}{figure.12.17}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.005}{121}{figure.12.18}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.005}{121}{figure.12.19}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.005}{122}{figure.12.20}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.005}{122}{figure.12.21}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.005}{123}{figure.12.22}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.005}{123}{figure.12.23}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.0025}{124}{figure.12.24}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.0025}{124}{figure.12.25}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.0025}{125}{figure.12.26}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.0025}{125}{figure.12.27}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.0025}{126}{figure.12.28}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.0025}{126}{figure.12.29}
\contentsline {subsection}{\numberline {12.1.2}Conclusi\'on de las pruebas para Round Robin}{127}{subsection.12.1.2}
\contentsline {subsection}{\numberline {12.1.3}Pruebas sobre la Versi\'on con TDMA}{127}{subsection.12.1.3}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.05}{128}{subsection.12.1.3}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.05}{128}{figure.12.31}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.05}{129}{figure.12.32}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.05}{129}{figure.12.33}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05}{130}{figure.12.34}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.05}{130}{figure.12.35}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.025}{130}{figure.12.36}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.025}{131}{figure.12.37}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.025}{131}{figure.12.38}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.025}{132}{figure.12.39}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025}{133}{figure.12.40}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.025}{133}{figure.12.41}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.01}{133}{figure.12.42}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.01}{134}{figure.12.43}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.01}{134}{figure.12.44}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.01}{135}{figure.12.45}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.01}{135}{figure.12.46}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.01}{136}{figure.12.47}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.005}{136}{figure.12.48}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.005}{137}{figure.12.49}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.005}{137}{figure.12.50}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.005}{138}{figure.12.51}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.005}{138}{figure.12.52}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.005}{139}{figure.12.53}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05 y TimeToDespatchQueue = 0.0025}{139}{figure.12.54}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025 y TimeToDespatchQueue = 0.0025}{140}{figure.12.55}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01 y TimeToDespatchQueue = 0.0025}{140}{figure.12.56}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.0025}{141}{figure.12.57}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.0025}{141}{figure.12.58}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.0025}{142}{figure.12.59}
\contentsline {subsection}{\numberline {12.1.4}Conclusi\'on de las pruebas para TDMA}{142}{subsection.12.1.4}
\contentsline {subsection}{\numberline {12.1.5}Pruebas sobre la Versi\'on con Env\IeC {\'\i }o Directo}{143}{subsection.12.1.5}
\contentsline {subsubsection}{TimeBetweenPackets = 0.05}{143}{subsection.12.1.5}
\contentsline {subsubsection}{TimeBetweenPackets = 0.025}{143}{figure.12.61}
\contentsline {subsubsection}{TimeBetweenPackets = 0.01}{145}{figure.12.62}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005}{145}{figure.12.63}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025}{145}{figure.12.64}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001}{146}{figure.12.65}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0005}{146}{figure.12.66}
\contentsline {subsubsection}{TimeBetweenPackets = 0.00025}{147}{figure.12.67}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0001}{148}{figure.12.68}
\contentsline {subsubsection}{TimeBetweenPackets = 0}{148}{figure.12.69}
\contentsline {subsection}{\numberline {12.1.6}Conclusi\'on de las pruebas para la Versi\'on con Env\IeC {\'\i }o Directo}{149}{subsection.12.1.6}
\contentsline {subsection}{\numberline {12.1.7}Comparaci\'on de Resultados entre las Todas las Versiones}{149}{subsection.12.1.7}
\contentsline {subsubsection}{Comparaci\'on de la Primera Configuraci\'on}{149}{subsection.12.1.7}
\contentsline {subsubsection}{Comparaci\'on de la Segunda Configuraci\'on}{150}{figure.12.72}
\contentsline {subsubsection}{Comparaci\'on de la Tercera Configuraci\'on}{152}{figure.12.74}
\contentsline {section}{\numberline {12.2}Resultados de las Pruebas de Inyecci\'on de Errores}{152}{section.12.2}
\contentsline {subsection}{\numberline {12.2.1}Pruebas sobre la Versi\'on con Round Robin}{153}{subsection.12.2.1}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.05 Error = 25\%}{153}{subsection.12.2.1}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.05 Error = 50\%}{153}{figure.12.77}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.05 Error = 75\%}{154}{figure.12.78}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Error = 25\%}{154}{figure.12.79}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Error = 50\%}{155}{figure.12.80}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Error = 75\%}{155}{figure.12.81}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Error = 25\%}{156}{figure.12.82}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Error = 50\%}{156}{figure.12.83}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Error = 75\%}{157}{figure.12.84}
\contentsline {subsection}{\numberline {12.2.2}Conclusi\'on de las pruebas para la versi\'on con Round Robin}{157}{subsection.12.2.2}
\contentsline {subsection}{\numberline {12.2.3}Pruebas sobre la Versi\'on con TDMA}{158}{subsection.12.2.3}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Error = 25\%}{158}{subsection.12.2.3}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Error = 50\%}{158}{figure.12.86}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Error = 75\%}{159}{figure.12.87}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.025 Error = 25\%}{159}{figure.12.88}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.025 Error = 50\%}{160}{figure.12.89}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.025 Error = 75\%}{160}{figure.12.90}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Error = 25\%}{161}{figure.12.91}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Error = 50\%}{161}{figure.12.92}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Error = 75\%}{162}{figure.12.93}
\contentsline {subsection}{\numberline {12.2.4}Conclusi\'on de las pruebas para la versi\'on con TDMA}{162}{subsection.12.2.4}
\contentsline {subsection}{\numberline {12.2.5}Pruebas sobre la Versi\'on con Env\IeC {\'\i }o Directo}{163}{subsection.12.2.5}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 TotalTime = 55,54 Seconds Error = 25\%}{163}{subsection.12.2.5}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 TotalTime = 55,54 Seconds Error = 50\%}{163}{figure.12.95}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 TotalTime = 55,54 Seconds Error = 75\%}{163}{figure.12.96}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 TotalTime = 30,44 Seconds Error = 25\%}{164}{figure.12.97}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 TotalTime = 30,44 Seconds Error = 50\%}{164}{figure.12.98}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 TotalTime = 30,44 Seconds Error = 75\%}{165}{figure.12.99}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 TotalTime = 15,4 Seconds Error = 25\%}{165}{figure.12.100}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 TotalTime = 15,4 Seconds Error = 50\%}{166}{figure.12.101}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 TotalTime = 15,4 Seconds Error = 75\%}{166}{figure.12.102}
\contentsline {subsection}{\numberline {12.2.6}Conclusi\'on de las pruebas para la versi\'on con Env\IeC {\'\i }o Directo}{167}{subsection.12.2.6}
\contentsline {section}{\numberline {12.3}Resultados de las Pruebas de Variaci\'on de Carga}{168}{section.12.3}
\contentsline {subsection}{\numberline {12.3.1}Pruebas sobre la Versi\'on con Round Robin}{168}{subsection.12.3.1}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.05 Net = 15 Packets = 100000 }{168}{subsection.12.3.1}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.05 Net = 5 Packets = 1000}{169}{figure.12.104}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Net = 15 Packets = 100000}{169}{figure.12.105}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Net = 5 Packets = 1000}{170}{figure.12.106}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.025 Net = 15 Packets = 100000}{170}{figure.12.107}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 y TimeToDespatchQueue = 0.025 Net = 5 Packets = 1000}{171}{figure.12.108}
\contentsline {subsection}{\numberline {12.3.2}Conclusi\'on de las pruebas para la versi\'on con Round Robin}{171}{subsection.12.3.2}
\contentsline {subsection}{\numberline {12.3.3}Pruebas sobre la Versi\'on con TDMA}{171}{subsection.12.3.3}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.025 Net = 15 Packets = 100000}{172}{subsection.12.3.3}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 y TimeToDespatchQueue = 0.025 Net = 5 Packets = 1000}{172}{figure.12.110}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Net = 15 Packets = 100000}{173}{figure.12.111}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.05 Net = 5 Packets = 1000}{173}{figure.12.112}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Net = 15 Packets = 100000}{174}{figure.12.113}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 y TimeToDespatchQueue = 0.025 Net = 5 Packets = 1000}{174}{figure.12.114}
\contentsline {subsection}{\numberline {12.3.4}Conclusi\'on de las pruebas para la versi\'on con TDMA}{175}{subsection.12.3.4}
\contentsline {subsection}{\numberline {12.3.5}Pruebas sobre la Versi\'on con Env\IeC {\'\i }o Directo}{175}{subsection.12.3.5}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 TotalTime = 55.54 Seconds Net = 15 Packets = 100000}{175}{subsection.12.3.5}
\contentsline {subsubsection}{TimeBetweenPackets = 0.005 TotalTime = 55.54 Seconds Net = 5 Packets = 1000}{176}{figure.12.116}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 TotalTime = 30.44 Seconds Net = 15 Packets = 100000}{176}{figure.12.117}
\contentsline {subsubsection}{TimeBetweenPackets = 0.0025 TotalTime = 30.44 Seconds Net = 5 Packets = 1000}{177}{figure.12.118}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 TotalTime = 15.4 Seconds Net = 15 Packets = 100000}{177}{figure.12.119}
\contentsline {subsubsection}{TimeBetweenPackets = 0.001 TotalTime = 15.4 Seconds Net = 5 Packets = 1000}{178}{figure.12.120}
\contentsline {subsection}{\numberline {12.3.6}Conclusi\'on de las pruebas para la versi\'on con Env\IeC {\'\i }o Directo}{178}{subsection.12.3.6}
\contentsline {subsection}{\numberline {12.3.7}Comparaci\'on de Resultados entre las Todas las Versiones}{178}{subsection.12.3.7}
\contentsline {subsubsection}{Comparaci\'on de la Primera Configuraci\'on}{179}{subsection.12.3.7}
\contentsline {subsubsection}{Comparaci\'on de la Segunda Configuraci\'on}{179}{figure.12.122}
\contentsline {subsubsection}{Comparaci\'on de la Tercera Configuraci\'on}{180}{figure.12.123}
\contentsline {part}{v\hspace {1em}\spacedlowsmallcaps {Conclusiones y Futuras Mejoras}}{181}{part.5}
\contentsline {chapter}{\numberline {13}\spacedlowsmallcaps {Conclusiones}}{183}{chapter.13}
\contentsline {section}{\numberline {13.1}Consecuci\'on de Objetivos}{184}{section.13.1}
\contentsline {subsection}{\numberline {13.1.1}Objetivo Principal}{184}{subsection.13.1.1}
\contentsline {subsection}{\numberline {13.1.2}Objetivos Secundarios}{184}{subsection.13.1.2}
\contentsline {section}{\numberline {13.2}Conclusiones Generales}{185}{section.13.2}
\contentsline {subsection}{\numberline {13.2.1}Conclusiones de la versi\'on con Round Robin}{185}{subsection.13.2.1}
\contentsline {subsection}{\numberline {13.2.2}Conclusiones de la versi\'on con TDMA}{185}{subsection.13.2.2}
\contentsline {subsection}{\numberline {13.2.3}Conclusiones de la versi\'on con Env\IeC {\'\i }o Directo}{186}{subsection.13.2.3}
\contentsline {subsection}{\numberline {13.2.4}Conclusiones finales}{186}{subsection.13.2.4}
\contentsline {section}{\numberline {13.3}Conclusiones Personales}{187}{section.13.3}
\contentsline {section}{\numberline {13.4}Futuras Mejoras}{187}{section.13.4}
\contentsline {part}{vi\hspace {1em}\spacedlowsmallcaps {Anexos}}{189}{part.6}
\contentsline {chapter}{\numberline {14}\spacedlowsmallcaps {Anexo A}}{191}{chapter.14}
\contentsline {section}{\numberline {14.1}Programaci\'on Concurrente}{192}{section.14.1}
\contentsline {subsection}{\numberline {14.1.1}Concurrencia en ConNet}{193}{subsection.14.1.1}
\contentsline {section}{\numberline {14.2}Uso de Sockets en Python}{194}{section.14.2}
\contentsline {subsection}{\numberline {14.2.1}Ejemplo Sockets TCP Server Python}{195}{subsection.14.2.1}
\contentsline {subsection}{\numberline {14.2.2}Ejemplo Sockets TCP Client Python}{196}{subsection.14.2.2}
\contentsline {subsection}{\numberline {14.2.3}Ejemplo Sockets UDP Server Python}{196}{subsection.14.2.3}
\contentsline {subsection}{\numberline {14.2.4}Ejemplo Sockets UDP Client Python}{197}{subsection.14.2.4}
\contentsline {chapter}{\numberline {15}\spacedlowsmallcaps {Anexo B}}{199}{chapter.15}
\contentsline {section}{\numberline {15.1}Manual de Usuario}{199}{section.15.1}
\contentsline {subsubsection}{Pesta\~na Network Connection}{200}{section.15.1}
\contentsline {subsubsection}{Pesta\~na Plots}{201}{figure.15.5}
\contentsline {subsubsection}{Pesta\~na Statistics}{203}{figure.15.7}
\contentsline {part}{vii\hspace {1em}\spacedlowsmallcaps {Bibliograf\IeC {\'\i }a}}{205}{part.7}
\vspace {\beforebibskip }
\contentsline {chapter}{\spacedlowsmallcaps {Bibliograf\IeC {\'\i }a}}{207}{dummy.7}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {spanish}
