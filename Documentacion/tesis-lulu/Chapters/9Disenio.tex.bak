\myChapter{Dise�o Procedimental}\label{chap:procedimental}
\minitoc\mtcskip
\vfill

\lettrine {U}{na} vez finalizado el an�lisis funcional, se llega a la �ltima fase del estudio de ingenier�a del software de este proyecto, el dise�o procedimental, donde se detallar�n las distintas clases y m�todos que se han de implementar para el correcto dise�o del middleware.
\section{Dise�o Procedimental}
En este apartado se proceder� a detallar las clases necesarias que se han de implementar para cumplir con los requisitos expuestos en el apartado 7. En cada clase que se va a detallar se van a distinguir los siguientes atributos:

\begin{itemize}
\item Nombre de la clase: Nombre que se le proporcionar� a la clase, este nombre ha de ser un nombre que de informaci�n acerca del funcionamiento que ofrece la clase.
\item Descripci�n de la clase: Se indicar� una descripci�n sobre el funcionamiento de la clase as� como su objetivo principal y cualquier detalle que ayude a comprender el funcionamiento de dicha clase.
\item Variables de la clase: Se indicaran todas aquellas variables propias de la clase, as� como el funcionamiento, descripci�n y tipo de cada una de ellas.
\item M�todos de la clase: Se expondr�n todos los m�todos o funciones internas de la clase, para as� llegar a saber la funcionalidad de cada de ellas y en su conjunto de la misma.
\end{itemize}

\subsection{Clase Network}
\begin{itemize}
\item Nombre: Network (V�ase Figura~\ref{fig:networkClassDiagram})

\item Descripci�n: clase base para representar la informaci�n principal de la que se compone una nueva red conectada al sistema intermediario.

\item Variables miembro:
\begin{itemize}
\item name: Nombre de la red.
\item ip: IP origen de la red.
\item inPort: Puerto de entrada de la red a trav�s del cual van a llegarle los paquetes con su IP de origen.
\item outPort: Puerto de salida de la red a trav�s del cual la red env�a paquetes al exterior.
\item receivedPackets: Paquetes recibidos por la red desde el inicio de la conexi�n.
\item sendedPackets: Paquetes enviados por la red desde el inicio de la conexi�n.
\end{itemize}

\item M�todos:
\begin{itemize}
\item getName(): devuelve la variable miembro 'name' que contiene el nombre de la red.
\item getIP(): devuelve la variable miembro 'ip' que contiene la ip origen de la red.
\item getInPort(): devuelve la variable miembro 'inPort' que contiene el puerto de entrada de la red.
\item getOutPort(): devuelve la variable miembro 'outPort' que contiene el puerto de salida de la red.
\item getReceivedPackets(): devuelve la variable miembro 'receivedPackets' que contiene el n�mero de paquetes recibidos por la red.
\item getSendedPackets(): devuelve la variable miembro 'sendedPackets' que contiene el n�mero de paquetes enviados por la red.
\item setName(name): asigna a la variable miembro 'name' el valor pasado por par�metro a la funci�n.
\item setIP(IP): asigna a la variable miembro 'ip' el valor pasado por par�metro a la funci�n.
\item setInPort(inPort): asigna a la variable miembro 'inPort' el valor pasado por par�metro a la funci�n.
\item setOutPort(outPort): asigna a la variable miembro 'outPort' el valor pasado por par�metro a la funci�n.
\item setReceivedPackets(receivedPackets): asigna a la variable miembro 'receivedPackets' el valor pasado por par�metro a la funci�n.
\item setSendedPackets(sendedPackets): asigna a la variable miembro 'sendedPackets' el valor pasado por par�metro a la funci�n.
\end{itemize}

\end{itemize}
\\\\
\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=15pc]{./Imagenes/classNetwork.eps}
  \caption{Diagrama de la clase Network}
  \label{fig:networkClassDiagram}
\end{SCfigure}

\subsection{Clase Protocol}
\begin{itemize}
\item Nombre: Protocol (V�ase Figura~\ref{fig:protocolClassDiagram})

\item Descripci�n: clase por la cual se elige entre el protocolo de comunicaci�n no orientado a conexi�n UDP y el protocolo orientado a conexi�n TCP, dependiendo del par�metro pasado al constructor.

\item Variables miembro:
\begin{itemize}
\item typeOf: tipo de conexi�n a realizar, 0 para UDP y 1 para TCP.
\item IPHost: IP de la red con la que se va a llevar a cabo la conexi�n.
\item inPort: Puerto de entrada de la red a conectar.
\item outPort: Puerto de salida de la red a conectar.
\end{itemize}

\item M�todos:
\begin{itemize}
\item openConnection(): abre la conexi�n con los sockets, dependiendo del protocolo elegido.
\item closeConnection(): cierra la conexi�n abierta con los sockets.
\item receiveFrom(): recibe paquetes de 1024 bytes a trav�s del socket de entrada.
\item sendTo(newArray,newNetwork): envia los datos que hay en el par�metro "newArray" a trav�s de la red 'newNetwork' pasada por par�metro.
\item sendToHello(newArray, generalPort): env�a un paquete hello para a�adir la red a la lista de redes a trav�s del puerto general de escucha del sistema intermediario.
\item generateInPort(portList): genera un puerto de entrada aleatorio siempre y cuando no se encuentre ya en la lista de puertos incluido.
\item generateOutPort(portList): genera un puerto de salida aleatorio siempre y cuando no se encuentre ya en la lista de puertos incluido.
\end{itemize}

\end{itemize}

\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=7pc]{./Imagenes/classProtocol.eps}
  \caption{Diagrama de la clase Protocol}
  \label{fig:protocolClassDiagram}
\end{SCfigure}

\subsection{Clase XMLDataBase}
\begin{itemize}
\item Nombre: XMLDataBase (V�ase Figura~\ref{fig:xmlDataBaseClassDiagram})

\item Descripci�n: clase encargada de guardar en un archivo XML la lista de las redes guardadas por el sistema intermediario.

\item Variables miembro:
\begin{itemize}
\item nombreFichero: guarda el nombre del fichero de salida en el que se guardar�n las redes.
\item networks: guarda la referencia al nodo Networks en el archivo XML.
\item doc: carga el archivo XML en ella para poder recorrerlo y modificarlo.
\end{itemize}

\item M�todos:
\begin{itemize}
\item saveNetwork(network): almacena en la lista de redes del archivo XML la red pasada por par�metro.
\item getAllNetwotks(): devuelve una lista con todas las redes almacenadas en el fichero XML.
\item getNumNetworks(): devuelve un entero con el n�mero de redes almacenadas.
\item setNewPorts(network): modifica la red pasada por par�metro con los puertos nuevos que contenga y la actualiza en el archivo XML.
\item addReceivedPackets(network): a�ade los paquetes recibidos a la red que le corresponda en el archivo XML.
\item addSendedPackets(network): a�ade los paquetes enviados a la red que le corresponda en el archivo XML.
\item deleteNetwork(network): borra del sistema la red que coincida con la red pasada por par�metro, siempre que esta exista.
\item getAllPacketsReceived(): devuelve un entero con los paquete totales recibidos por las redes.
\item getAllPacketsSended(): devuelve un entero con los paquete totales enviados por las redes.
\end{itemize}

\end{itemize}

\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=10pc]{./Imagenes/classXMLDataBase.eps}
  \caption{Diagrama de la clase XMLDataBase}
  \label{fig:xmlDataBaseClassDiagram}
\end{SCfigure}

\subsection{Clase VectorParser}
\begin{itemize}
\item Nombre: VectorParser (V�ase Figura~\ref{fig:vectorParserClassDiagram})

\item Descripci�n: clase encargada de pasar las string que se reciben desde los sockets a un vector y viceversa para facilitar y agilizar las comunicaciones con las redes conectadas.

\item Variables miembro:
\item M�todos:
\begin{itemize}
\item stringToVector(data): convierte la string pasada por par�metro 'data' a un vector que devuelve mediante return.
\item vectorToString(dataVector): convierte el vector pasado por par�metro 'dataVector' a una string que devuelve mediante return.
\end{itemize}

\end{itemize}

\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=10pc]{./Imagenes/classVectorParser.eps}
  \caption{Diagrama de la clase VectorParser}
  \label{fig:vectorParserClassDiagram}
\end{SCfigure}

\subsection{Clase ResponseMessage}
\begin{itemize}
\item Nombre: ResponseMessage (V�ase Figura~\ref{fig:responseMessageClassDiagram})

\item Descripci�n: clase encargada de responder las peticiones que le llegan mediante un mensaje y c�digos de error y de estado.

\item Variables miembro:
\begin{itemize}
\item statusCode: c�digo de estado de la petici�n.
\item errorCode: c�digo de error de la petici�n.
\item message: mensaje enviado al remitente.
\end{itemize}

\item M�todos:
\begin{itemize}
\item getStatusCode(): devuelve el c�digo de estado actual.
\item getErrorCode(): devuelve el c�digo de error actual.
\item getMessage(): devuelve el mensaje actual.
\item setStatusCode(statusCode): asigna a la variable miembro 'statusCode' el valor pasado por par�metro statusCode.
\item setErrorCode(errorCode): asigna a la variable miembro 'errorCode' el valor pasado por par�metro errorCode.
\item setMessage(message): asigna a la variable miembro 'message' el valor pasado por par�metro message.
\end{itemize}

\end{itemize}

\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=12pc]{./Imagenes/classResponseMessage.eps}
  \caption{Diagrama de la clase ResponseMessage}
  \label{fig:responseMessageClassDiagram}
\end{SCfigure}

\subsection{Clase ColorStringClass}
\begin{itemize}
\item Nombre: ColorStringClass (V�ase Figura~\ref{fig:colorStringClassDiagram})

\item Descripci�n: clase encargada de asignar color a los mensajes mostrados por consola.

\item Variables miembro:

\item M�todos:
\begin{itemize}
\item messageFormatRed(message): formatea el color del mensaje al color rojo.
\item messageFormatBlue(message): formatea el color del mensaje al color azul.
\item messageFormatGreen(message): formatea el color del mensaje al color verde.
\end{itemize}

\end{itemize}


\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=15pc]{./Imagenes/classColorString.eps}
  \caption{Diagrama de la clase ColorString}
  \label{fig:colorStringClassDiagram}
\end{SCfigure}
\subsection{NetworkDetail}
\begin{itemize}
\item Nombre: NetworkDetail (V�ase Figura~\ref{fig:networkDetailClassDiagram})

\item Descripci�n: clase que hereda de QDialog usada para mostrar un di�logo con la informaci�n de la red seleccionada y poder borrarla desde ah�.

\item Variables miembro:
\begin{itemize}
\item networkList: lista de conectadas redes del sistema.
\item console: instancia a la consola de datos de la clase principal para poder mandarle informaci�n.
\item networksDataBase: instancia de la clase XMLDataBase para abrir el archivo XML y que sea usado en el flujo del programa.
\item gridlayout: layout en forma de rejilla para mostrar los datos de la red.
\item delete: QPushButton para borrar la red seleccionad si se desea.
\item network: variable que recoge la red que coincida con la seleccionada en la lista de redes.

\end{itemize}

\item M�todos:
\begin{itemize}
\item init(networkStr, networkList, console): constructor de la clase, organiza el layout a mostrar e inicializa las variables miembro de la clase.

\item onpushButtonSetBasetoggled(): recoge el evento de pulsaci�n del bot�n delete y pregunta si se quiere borrar la red seleccionada.
\end{itemize}
\end{itemize}


\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=15pc]{./Imagenes/classNetworkDetail.eps}
  \caption{Diagrama de la clase NetworkDetail}
  \label{fig:networkDetailClassDiagram}
\end{SCfigure}

\subsection{Plots}
\begin{itemize}
\item Nombre: Plots (V�ase Figura~\ref{fig:plotsClassDiagram})

\item Descripci�n: Clase encargada de gestionar el control de las gr�ficas de datos, as� como de su dise�o y herramientas. Tambi�n es la clase base de la que heredan otras clases encargadas de gr�ficas espec�ficas que se detallar�n mas adelante.

\item Variables miembro:
\begin{itemize}
\item figure: figura donde se crear� la gr�fica de datos.
\end{itemize}

\item M�todos:
\begin{itemize}
\item init(tittle, xlabel, ylabel, colorCycle): constructor de la clase, inicializa los datos de la gr�fica y como mostrarlos.

\end{itemize}
\end{itemize}


\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=12pc]{./Imagenes/classPlots.eps}
  \caption{Diagrama de la clase Plots}
  \label{fig:plotsClassDiagram}
\end{SCfigure}

\subsection{ReceivedPacketPerNetworkPlot}
\begin{itemize}
\item Nombre: ReceivedPacketPerNetworkPlot (V�ase Figura~\ref{fig:receivedPacketPerNetworkPlotClassDiagram})

\item Descripci�n: Clase que hereda de la clase Plots. Se encarga de crear un histograma con los paquetes recibidos por cada red.
\item Variables miembro:
\begin{itemize}
\item dataBase: instancia a la Base de Datos en el fichero XML.
\item list: lista de redes almacenadas en el sistema.
\item name: lista de tuplas con los nombres de las redes.
\item rects: lista de tuplas con la recta que representa a cada red.
\item height: alto de cada recta.
\item width: ancho de cada recta.
\end{itemize}

\item M�todos:
\begin{itemize}
\item ReceivedPacketPerNetworkPlot(): inicializa todas las variables de la gr�fica.
\item computeInitialFigure(): crea el histograma a partir de las variables m�todo.

\end{itemize}
\end{itemize}

\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=13pc]{./Imagenes/classReceivedPacketsPlot.eps}
  \caption{Diagrama de la clase ReceivedPacketPerNetworkPlot}
  \label{fig:receivedPacketPerNetworkPlotClassDiagram}
\end{SCfigure}

\subsection{SendedPacketPerNetworkPlot}
\begin{itemize}
\item Nombre: SendedPacketPerNetworkPlot (V�ase Figura~\ref{fig:sendedPacketPerNetworkPlotClassDiagram})

\item Descripci�n: Clase que hereda de la clase Plots. Se encarga de crear un histograma con los paquetes enviados por cada red.
\item Variables miembro:
\begin{itemize}
\item dataBase: instancia a la Base de Datos en el fichero XML.
\item list: lista de redes almacenadas en el sistema.
\item name: lista de tuplas con los nombres de las redes.
\item rects: lista de tuplas con la recta que representa a cada red.
\item height: alto de cada recta.
\item width: ancho de cada recta.
\end{itemize}

\item M�todos:
\begin{itemize}
\item SendedPacketPerNetworkPlot(): inicializa todas las variables de la gr�fica.
\item computeInitialFigure(): crea el histograma a partir de las variables m�todo.

\end{itemize}
\end{itemize}


\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=12pc]{./Imagenes/classSendedPacketsPlot.eps}
  \caption{Diagrama de la clase SendedPacketPerNetworkPlot}
  \label{fig:sendedPacketPerNetworkPlotClassDiagram}
\end{SCfigure}

\subsection{RealTimePlot}
\begin{itemize}
\item Nombre: RealTimePlot (V�ase Figura~\ref{fig:realTimePlotClassDiagram})


\item Descripci�n: Clase que hereda de la clase Plots. Se encarga de crear una gr�fica de puntos en tiempo real con los datos que se le pasen.
\item Variables miembro:
\begin{itemize}
\item timer: timer que actualiza cada segundo la gr�fica llamando a su m�todo updateFigure().
\item packets: lista de paquetes por segundos con las medidas represent�ndose en el eje Y.
\item seconds: lista de segundos que representa el eje X de la gr�fica.
\item args: argumentos que definen caracter�sticas de la gr�fica, como el color, tipo de l�nea, nombre de cada eje o nombre general de la gr�fica en tiempo real.
\item legend: leyenda sobre lo que est� midiendo la representaci�n de la gr�fica.
\end{itemize}

\item M�todos:
\begin{itemize}
\item RealTimePlot(): inicializa todas las variables de la gr�fica y el timer.
\item updateFigure(): vuelve a dibujar la gr�fica con los datos actualizados.
\item updateData(): actualiza los datos a�adiendo una nueva medida de paquetes por segundo a la lista packets y aumentado en uno los segundos de la lista seconds.

\end{itemize}
\end{itemize}

\begin{SCfigure}[c][!hb]
  \centering
    \includegraphics[width=12pc]{./Imagenes/classRealTimePlot.eps}
  \caption{Diagrama de la clase RealTimePlot}
  \label{fig:realTimePlotClassDiagram}
\end{SCfigure}


\subsection{Queue}

\begin{itemize}
\item Nombre: Queue (V�ase Figura~\ref{fig:queueClassDiagram})

\item Descripci�n: Clase que implementa una cola FIFO de datos.

\item Variables miembro:
\begin{itemize}
\item items: elementos de la cola.
\end{itemize}

\item M�todos:
\begin{itemize}
\item init(): constructor de la clase.

\item isEmpty(): devuelve True o False dependiendo si la cola esta o no vac�a.

\item enqueue(item): encola el elemento pasado por par�metro dentro de la cola.

\item dequeue(): devuelve y saca el primer elemento de la cola.

\item size(): devuelve el n�mero de elementos actuales en la cola.
\end{itemize}
\end{itemize}


\begin{SCfigure}[c][!ht]
  \centering
    \includegraphics[width=7pc]{./Imagenes/classQueue.eps}
  \caption{Diagrama de la clase Queue}
  \label{fig:queueClassDiagram}
\end{SCfigure}



\subsection{Clase ConNet}
\begin{itemize}
\item Nombre: ConNet

\item Descripci�n: clase principal dedicada a conectar la interfaz usuario con la funcionalidad del sistema.

\item Variables miembro:
\begin{itemize}
\item SERVERIP: direcci�n ip del sistema intermediario.
\item protocol: instancia de la clase Protocol para elegir el protocolo que se usa en cada momento.
\item networksDataBase: instancia de la clase XMLDataBase para abrir el archivo XML y que sea usado en el flujo del programa.
\item udp: variable asociada al RadioButton para poder saber si est� activo o no el udp.
\item tcp: variable asociada al RadioButton para poder saber si est� activo o no el tcp.
\item connected: variable booleana para saber si esta conectado el sistema en ese momento.
\item networksList: variable que mantiene la lista de redes gr�ficamente actualizada.
\end{itemize}

\item M�todos:
\begin{itemize}
\item createControler(): m�todo para inicializar la parte gr�fica mediante la librer�a QT.
\item onpushButtonSetBasetoggled(): m�todo para controlar el funcionamiento de los botones que eligen la conexi�n entre TCP y UDP.
\item center(): m�todo para centrar la interfaz gr�fica en la pantalla.
\item closeEvent(event): m�todo para capturar el evento de cerrado y mostrar un di�logo cuando eso pase.
\item newNetworkConnection(): m�todo que registra una nueva red en el sistema asign�ndole y conectandose a los nuevos puertos en el hilo correspondiente a esa red.
\item connectionMainPort(): m�todo al que le llegan todas las peticiones de conexi�n con nuevas redes, abre un hilo por cada red nueva que le llega.
\end{itemize}

\end{itemize}

DIAGRAMA