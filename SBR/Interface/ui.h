#ifndef __UI_h_
#define __UI_h_

#if UIDebug != 0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include "../Network/Network.h"
void logAppend(char *string);
void updateAll(GtkWidget *widget, gpointer data);
void *InitUI();

#endif
#endif
