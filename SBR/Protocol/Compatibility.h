/** *************************************************************
	@file:	 Compatibility.h
	@author: Antonio Cubero Fernández
	@desc:	 Compatibility library. It allows the programmer to
			 create functions to do complex operation as dynamic
			 memory or semaphores.
	@date:	 July 2015
*************************************************************** */
#ifndef __Compatibility_h_
#define __Compatibility_h_

#include <semaphore.h>

enum{ Func_Malloc,
	Func_Free
};

typedef void (*genericFunc) ();

genericFunc compatibilityFunctionArray[3];

// /////////////////////////////////////////////////////////////////
//	@desc	Call pre-asigned Malloc.
//	@param	size Memory size.
//	@return	Reserved memory pointer.
// /////////////////////////////////////////////////////////////////
void *comMalloc(size_t size);

// /////////////////////////////////////////////////////////////////
//	@desc	Free preasigned memory.
//	@param	ptr Pointer to memory.
//	@return	Void.
// /////////////////////////////////////////////////////////////////
void comFree(void *ptr);


int com_sem_init(sem_t *sem, int pshared, unsigned int value);

int com_sem_wait(sem_t *sem);

int com_sem_post(sem_t *sem);


void *comMallocRegister(genericFunc func);

void *comFreeRegister(genericFunc func);


#endif




















