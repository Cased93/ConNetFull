/** *************************************************************
	@file:	 auxFunc.h
	@author: Antonio Cubero Fernández
	@desc:	 Auxiliary functions
	@date:	 July 2015
*************************************************************** */

#ifndef __auxFunc_h_
#define __auxFunc_h_

#include <string.h>
#include <stdio.h>

#include "const.h"


// /////////////////////////////////////////////////////////////////
//	@desc	Devuelve un puntero a char con el contenido de x (1 byte) en binario.
//	@param	x Variable a imprimir
//	@return	array de char con el contenido
// //////////////////////////////////////////////////////////////////
const char *auxFByteToBinary(uint8_t *x);

// /////////////////////////////////////////////////////////////////
//	@desc	Devuelve un puntero a char con el contenido de x (4 bytes) en binario.
//	@param	x Variable a imprimir
//	@return	array de char con el contenido
// //////////////////////////////////////////////////////////////////
const char *auxFByte32ToBinary(uint32_t *x);

char *auxFByteToBinary2(char x);

void printBuffer(void *buffer, int size);

#endif
