/** *************************************************************
	@file:	 MathValue.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura MathValue y funciones auxiliares.
	@date:	 Junio 2015
*************************************************************** */

#ifndef __MathValue_h_
#define __MathValue_h_

#include "const.h"
#include "stdio.h"

#define INTEGER          0x00
#define FLOAT            0x10
#define BOOLEAN          0x20
#define MATHERROR        0x30

typedef struct MathValue {
	uint8_t __type;
	union {
		int   i;
		float f;
		uint8_t b;
	} data;
} MathValue;

#endif
