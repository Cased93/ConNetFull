/** *************************************************************
	@file:	 const.h
	@author: Antonio Cubero Fernández
	@desc:	 Fichero de constantes
	@date:	 Mayo 2015
*************************************************************** */

#ifndef __const__h__
#define __const__h__

////////////////////////////////////////////////////////////
//////////////// CONFIG PARAMETERS /////////////////////////
////////////////////////////////////////////////////////////
#define debug 0
#define UIDebug 0
#define ExternalNetworkThreadEnable 0
#define TimingBenchmark 0
#define contiki 0

#define NET_CONTEXT 0
#define LOCAL_CONTEXT 1

////////////////////////////////////////////////////////////
/////////////////// INIT FUNCTIONS /////////////////////////
////////////////////////////////////////////////////////////
#define GLOBAL_DECLARATIONS MathParser mp_GLOBAL; TokenBuffer tb_GLOBAL; Network net_GLOBAL;

#define BEGIN_MP(MP) MP.__parser_heap_len = 0; MP.__fops_len = 0;

#define BEGIN_TB(TB) TB.__buf_len = 0;

#define BEGIN_NET(NET) NET.rel_netwok_resource_list = NULL; NET.cl_network_context_list = NULL;\
	NET.al_network_address_list = NULL;\
	com_sem_init(&NET.net_sem, 0, 1);

#define GLOBAL_INIT BEGIN_MP(mp_GLOBAL); BEGIN_TB(tb_GLOBAL); BEGIN_NET(net_GLOBAL);


////////////////////////////////////////////////////////////
//////////////// INTERFACE DEBUGGING FUNCTIONS ///////////////////////
////////////////////////////////////////////////////////////

#if UIDebug != 0
	#define UI_Debug(...) sprintf(RASLogBuffer + strlen(RASLogBuffer), __VA_ARGS__);
#else
	#define UI_Debug(...) {}
#endif


////////////////////////////////////////////////////////////
//////////////// DEBUGGING FUNCTIONS ///////////////////////
////////////////////////////////////////////////////////////

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

//#define printGreen(...) printf(ANSI_COLOR_GREEN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET "\n");

#define debugSTRThread2(...) debugSTR2(__VA_ARGS__)
#define debugSTRThread1(...) debugSTR1(__VA_ARGS__)
#define debugSTRThread0(...) debugSTR0(__VA_ARGS__)
#define debugSTRThreadPhy2(...) debugSTRYellow2(__VA_ARGS__)
#define debugSTRThreadPhy1(...) debugSTRYellow1(__VA_ARGS__)
#define debugSTRThreadPhy0(...) debugSTRYellow0(__VA_ARGS__)

#define debugSTRContext2(...) debugSTRBlue2(__VA_ARGS__)
#define debugSTRContext1(...) debugSTRBlue1(__VA_ARGS__)
#define debugSTRContext0(...) debugSTRBlue0(__VA_ARGS__)

#define debugSTRNet2(...) debugSTRRed2(__VA_ARGS__)
#define debugSTRNet1(...) debugSTRRed1(__VA_ARGS__)
#define debugSTRNet0(...) debugSTRRed0(__VA_ARGS__)

#define debugSTRNetPhy2(...) debugSTRYellow2(__VA_ARGS__)
#define debugSTRNetPhy1(...) debugSTRYellow1(__VA_ARGS__)
#define debugSTRNetPhy0(...) debugSTRYellow0(__VA_ARGS__)

#if debug == 2 //Prints everything
	#define debugSTR2(...) printf(__VA_ARGS__);
	#define debugSTRRed2(...) {printf(ANSI_COLOR_RED); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRGreen2(...) {printf(ANSI_COLOR_GREEN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRBlue2(...) {printf(ANSI_COLOR_BLUE); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRYellow2(...) {printf(ANSI_COLOR_YELLOW); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRCyan2(...) {printf(ANSI_COLOR_CYAN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}

	#define debugINT2(x); printf("%d", x);
	#define debugFLOAT2(x); printf("%f", x);
	#define debugFunc2(x, ...); (*x)(__VA_ARGS__);

	#define debugSTR1(...) printf(__VA_ARGS__);
	#define debugSTRRed1(...) {printf(ANSI_COLOR_RED); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRGreen1(...) {printf(ANSI_COLOR_GREEN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRBlue1(...) {printf(ANSI_COLOR_BLUE); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRYellow1(...) {printf(ANSI_COLOR_YELLOW); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRCyan1(...) {printf(ANSI_COLOR_CYAN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}

	#define debugINT1(x) printf("%d", x);
	#define debugFLOAT1(x) printf("%f", x);
	#define debugFunc1(x, ...) (*x)(__VA_ARGS__);

	#define debugSTR0(...) {printf(__VA_ARGS__);}
	#define debugSTRRed0(...) {printf(ANSI_COLOR_RED); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRGreen0(...) {printf(ANSI_COLOR_GREEN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRBlue0(...) {printf(ANSI_COLOR_BLUE); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRYellow0(...) {printf(ANSI_COLOR_YELLOW); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
	#define debugSTRCyan0(...) {printf(ANSI_COLOR_CYAN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}

	#define debugINT0(x) printf("%d", x);
	#define debugFLOAT0(x) printf("%f", x);
	#define debugFunc0(x, ...) (*x)(__VA_ARGS__);

#elif debug == 1 //Only prints 0 & 1
		#define debugSTR2(...) {}
		#define debugSTRRed2(...) {}
		#define debugSTRGreen2(...) {}
		#define debugSTRBlue2(...) {}
		#define debugSTRYellow2(...) {}
		#define debugSTRCyan2(...) {}

		#define debugINT2(x) {}
		#define debugFLOAT2(x) {}
		#define debugFunc2(x, ...) {}

		#define debugSTR1(...) printf(__VA_ARGS__);
		#define debugSTRRed1(...) {printf(ANSI_COLOR_RED); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRGreen1(...) {printf(ANSI_COLOR_GREEN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRBlue1(...) {printf(ANSI_COLOR_BLUE); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRYellow1(...) {printf(ANSI_COLOR_YELLOW); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRCyan1(...) {printf(ANSI_COLOR_CYAN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}

		#define debugINT1(x) printf("%d", x);
		#define debugFLOAT1(x) printf("%f", x);
		#define debugFunc1(x, ...) (*x)(__VA_ARGS__);

		#define debugSTR0(...) printf(__VA_ARGS__);
		#define debugSTRRed0(...) {printf(ANSI_COLOR_RED); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRGreen0(...) {printf(ANSI_COLOR_GREEN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRBlue0(...) {printf(ANSI_COLOR_BLUE); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRYellow0(...) {printf(ANSI_COLOR_YELLOW); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRCyan0(...) {printf(ANSI_COLOR_CYAN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}

		#define debugINT0(x) printf("%d", x)
		#define debugFLOAT0(x) printf("%f", x)
		#define debugFunc0(x, ...) (*x)(__VA_ARGS__)

#elif debug == 0 //Valores que se imprimen SIEMPRE
		#define debugSTR2(...) {}
		#define debugSTRRed2(...) {}
		#define debugSTRGreen2(...) {}
		#define debugSTRBlue2(...) {}
		#define debugSTRYellow2(...) {}
		#define debugSTRCyan2(...) {}

		#define debugINT2(x) {}
		#define debugFLOAT2(x) {}
		#define debugFunc2(x, ...) {}

		#define debugSTR1(...) {}
		#define debugSTRRed1(...) {}
		#define debugSTRGreen1(...) {}
		#define debugSTRBlue1(...) {}
		#define debugSTRYellow1(...) {}
		#define debugSTRCyan2(...) {}

		#define debugINT1(x) {}
		#define debugFLOAT1(x) {}
		#define debugFunc1(x, ...) {}

		#define debugSTR0(...) printf(__VA_ARGS__);
		#define debugSTRRed0(...) {printf(ANSI_COLOR_RED); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRGreen0(...) {printf(ANSI_COLOR_GREEN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRBlue0(...) {printf(ANSI_COLOR_BLUE); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRYellow0(...) {printf(ANSI_COLOR_YELLOW); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}
		#define debugSTRCyan0(...) {printf(ANSI_COLOR_CYAN); printf(__VA_ARGS__); printf(ANSI_COLOR_RESET);}

		#define debugINT0(x) printf("%d", x)
		#define debugFLOAT0(x) printf("%f", x)
		#define debugFunc0(x, ...) (*x)(__VA_ARGS__)

#else
	#define debugSTR2(...) {}
	#define debugSTRRed2(...) {}
	#define debugSTRGreen2(...) {}
	#define debugSTRBlue2(...) {}
	#define debugSTRYellow2(...) {}
	#define debugSTRCyan2(...) {}

	#define debugINT2(...) {}
	#define debugFLOAT2(...) {}
	#define debugFunc2(...) {}

	#define debugSTR1(...) {}
	#define debugSTRRed1(...) {}
	#define debugSTRGreen1(...) {}
	#define debugSTRBlue1(...) {}
	#define debugSTRYellow1(...) {}
	#define debugSTRCyan2(...) {}

	#define debugINT1(...) {}
	#define debugFLOAT1(...) {}
	#define debugFunc1(...) {}

	#define debugSTR0(...) {}
	#define debugSTRRed0(...) {}
	#define debugSTRGreen0(...) {}
	#define debugSTRBlue0(...) {}
	#define debugSTRYellow0(...) {}
	#define debugSTRCyan0(...) {}

	#define debugINT0(...) {}
	#define debugFLOAT0(...) {}
	#define debugFunc0(...) {}

#endif

////////////////////////////////////////////////////////////
/////////////////// DATA TYPES /////////////////////////////
////////////////////////////////////////////////////////////
typedef unsigned char       uint8_t;
typedef unsigned short int  uint16_t;
typedef unsigned int        uint32_t;
typedef signed char         int8_t;
typedef signed short int    int16_t;
typedef signed int          int32_t;

////////////////////////////////////////////////////////////
////////////////// Numeric TYPES ///////////////////////////
////////////////////////////////////////////////////////////
#define INTEGER          0x00
#define FLOAT            0x10
#define BOOLEAN          0x20
#define MATHERROR        0x30

////////////////////////////////////////////////////////////
/////////////////////// OTHER //////////////////////////////
////////////////////////////////////////////////////////////
#define PUBLIC
#define PRIVATE static
#define GLOBAL  extern

#endif
