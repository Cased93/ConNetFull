/** *************************************************************
	@file:	 mathparser.h
	@author: Antonio Cubero Fernández
	@desc:	 Cabeceras de las estructuras y funciones
			 procesador de tokens
	@date:	 Junio 2015
*************************************************************** */

#ifndef __tokenBuffer_h_
#define __tokenBuffer_h_

#include <string.h>
#include <stdio.h>

#include "../../Protocol/const.h"
#include "../../Protocol/MathValue.h"
#include "../../Resource/Resource.h"

// -------------- TOKEN HEADER -------------------------
// Tipo de token:
#define _TOKEN_TYPE_BITS 2
#define _TOKEN_TYPE_MASK 0xC0
#define FUNCTION         0x00
#define SYMBOL_T         0x40
#define NVALUE           0x80
#define TOKEN_ERROR      0xC0

#define __getTokenType(b) ((b & _TOKEN_TYPE_MASK) >> 8 - _TOKEN_TYPE_BITS)
#define __isFunction(T) ((T.TOKEN_HEADER & _TOKEN_TYPE_MASK) == FUNCTION ? 1 : 0)
#define __isValue(T) ((T.TOKEN_HEADER & _TOKEN_TYPE_MASK) == NVALUE ? 1 : 0)
#define __isSymbol(T) ((T.TOKEN_HEADER & _TOKEN_TYPE_MASK) == SYMBOL_T ? 1 : 0)

// Tipo de dato:
#define _DATA_TYPE_BITS  2
#define _DATA_TYPE_MASK  0x30

#define _FUNC_N_ARGS  	2
#define _N_ARGS_MASK	0x0C
#define _0_ARG		  	0x00
#define _1_ARG		  	0x04
#define _2_ARG		  	0x08

#define __getNArgs(b) (((b & _N_ARGS_MASK) >> 3) ? 2 : 1)

// NOTA: Quedan 3 bits sin usar
// --------------- FUNCTION TOKEN ----------------------
// Referencia a función
#define _FUNCTION_REFERENCE_BITS   8  // LIM: 256 funciones
//#define _N_OPERANDS_BITS         4  // LIM: 16  operandos
// --------------- SYMBOL TOKEN ------------------------
// Referencia al símbolo

#define _SYMBOL_REFERENCE_BITS	32  // LIM: 256 símbolos
//#define _SYMBOL_REFERENCE_MASK	0xFF000000
//#define _SYMBOL_CONTEXT_MASK	0x00FF0000
//#define _SYMBOL_NETWORK_MASK	0x0000FFFF

// --------------- VALUE TOKEN -------------------------
// Valor
#define _VALUE_BITS                32 // sizeof(_V)
// -----------------------------------------------------


typedef struct __TOKEN {
	uint8_t TOKEN_HEADER;
	union {
		uint8_t FUNCTION_REFERENCE;
		__SYMBOL_ADDRESS SYMBOL_ADDRESS;
		uint32_t VALUE;
	} data;
} __TOKEN;

typedef struct TokenBuffer {
	__TOKEN __buf[256];
	uint8_t __buf_len;
} TokenBuffer;

// /////////////////////////////////////////////////////////////////
//	@desc	Crea un token de tipo función y lo añade al buffer.
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@param	fn_code Código de la función
//	@param	n_args Número de argumentos de la función
//	@return
// //////////////////////////////////////////////////////////////////
int mpFunction(TokenBuffer *TB, uint8_t fn_code, uint8_t n_args, ...);

// /////////////////////////////////////////////////////////////////
//	@desc	Crea un token de tipo valor entero y lo añade al buffer.
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@param	v Valor
//	@return
// //////////////////////////////////////////////////////////////////
int mpValueI(TokenBuffer *TB, int v);

// /////////////////////////////////////////////////////////////////
//	@desc	Crea un token de tipo valor flotante y lo añade al buffer
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@param	v Valor
//	@return
// //////////////////////////////////////////////////////////////////
int mpValueF(TokenBuffer *TB, float v);

// /////////////////////////////////////////////////////////////////
//	@desc	Crea un token de tipo símbolo y lo añade al buffer
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@param	symbol Símbolo
//	@return
// //////////////////////////////////////////////////////////////////
int mpSymbol(TokenBuffer *TB, uint8_t symbol_reference, uint8_t context_reference);

// /////////////////////////////////////////////////////////////////
//	@desc	Añade un token al buffer
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@param	aux_token Token que se va a incluir en el buffer
// //////////////////////////////////////////////////////////////////
void __bufPush(TokenBuffer *TB, __TOKEN aux_token);

// /////////////////////////////////////////////////////////////////
//	@desc	Extrae un token de un buffer de la parte final
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@return	Token extraido
// //////////////////////////////////////////////////////////////////
__TOKEN __bufPop(TokenBuffer *TB);

// /////////////////////////////////////////////////////////////////
//	@desc	Extrae un token de un buffer de una posición concreta
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@return	Token extraido
// //////////////////////////////////////////////////////////////////
__TOKEN __bufAddrPop(TokenBuffer *TB, uint8_t position);



#endif

