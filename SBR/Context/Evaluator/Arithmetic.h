/** *************************************************************
	@file:	 Arithmetic.h
	@author: Antonio Cubero Fernández
	@desc:	 Cabeceras de las funciones aritméticas.
			 Son funciones genéricas que reciben un número
			 variable de argumentos MathValue.
	@date:	 Junio 2015
*************************************************************** */

#ifndef __Arithmetic_h_
#define __Arithmetic_h_

#include <stdarg.h>
#include <math.h>

#include "../../Protocol/MathValue.h"

// /////////////////////////////////////////////////////////////////
//	@desc	Suma dos valores
//	@param	mv1 y mv2 Operandos
//	@return	MathValue con el resultado de la suma
// //////////////////////////////////////////////////////////////////
MathValue ariAddition(MathValue *mv1, ...);

// /////////////////////////////////////////////////////////////////
//	@desc	Resta dos valores
//	@param	mv1 y mv2 Operandos
//	@return	MathValue con el resultado de la resta
// //////////////////////////////////////////////////////////////////
MathValue ariSubtraction(MathValue *mv1, ...);

// /////////////////////////////////////////////////////////////////
//	@desc	Multiplica dos valores
//	@param	mv1 y mv2 Operandos
//	@return	MathValue con el resultado de la multiplicación
// //////////////////////////////////////////////////////////////////
MathValue ariMultiplication(MathValue *mv1, ...);

// /////////////////////////////////////////////////////////////////
//	@desc	Divide dos valores
//	@param	mv1 y mv2 Operandos
//	@return	MathValue con el resultado de la división
// //////////////////////////////////////////////////////////////////
MathValue ariDivision(MathValue *mv1, ...);

// /////////////////////////////////////////////////////////////////
//	@desc	Realiza la raiz cuadrada
//	@param	mv1 Operando
//	@return	MathValue con el resultado de la raiz cuadrada
// //////////////////////////////////////////////////////////////////
MathValue ariSQRT(MathValue *mv1, ...);


#endif
