/** *************************************************************
	@file:	 mathparser.h
	@author: Antonio Cubero Fernández
	@desc:	 Cabeceras de las estructuras y funciones del parser matemático
	@date:	 Junio 2015
*************************************************************** */

//TODO cambiar nombre por evaluator?
#ifndef __mathparser_h_
#define __mathparser_h_

#include "../../Protocol/const.h"
#include "tokenBuffer.h"

#define MP_BUF_LEN 256
#define MP_FN_LEN  30

typedef MathValue (*f_op)(MathValue *a, ...);

typedef struct MathParser {
	//Parser Heap
	uint8_t __parser_heap[MP_BUF_LEN]; //Almacena Mathvalue de forma temporal mientras se procesa.
	uint8_t __parser_heap_len;

	f_op	__fops[MP_FN_LEN];
	uint8_t	__fops_len;

} MathParser;

#include "../../Network/Network.h"
#include "../../Protocol/Print.h"

// /////////////////////////////////////////////////////////////////
//	@desc	Añade un operador a la lista de operadores del MathParser
//	@param	mp Parser en el que se encuentra la lista
//	@param	Function Puntero a la función que se desea añadir
//	@return
// //////////////////////////////////////////////////////////////////
uint8_t mpAddOperator(MathParser *mp, f_op function);

// /////////////////////////////////////////////////////////////////
//	@desc	Añade un MathValue a la pila
//	@param	mp Parser en el que se encuentra la pila
//	@param	aux_value MathValue a añadir
//	@return
// //////////////////////////////////////////////////////////////////
void __heapPush(MathParser *mp, MathValue aux_value);

// /////////////////////////////////////////////////////////////////
//	@desc	Extrae un token de la pila
//	@param	mp Parser en el que se encuentra la pila
//	@param	v Valor
//	@return	MathValue extraido
// //////////////////////////////////////////////////////////////////
MathValue __heapPop(MathParser *mp);

// /////////////////////////////////////////////////////////////////
//	@desc	Procesa el buffer utilizando la pila.
//	@param	TB TokenBuffer en el que se encuentra el buffer
//	@param	mp Parser en el que se encuentra la pila
//	@return	MathValue con un boolean con el valor o con MATHERROR
// //////////////////////////////////////////////////////////////////
MathValue mpProcess(TokenBuffer *TB, MathParser *mp);

// /////////////////////////////////////////////////////////////////
//	@desc	Comprueba si el valor introducido es mayor que 0
//			teniendo en cuenta que hay varios tipos de dato.
//	@param	mv MathValue a comprobar
//	@return	0 o !=0 dependiendo del valor almacenado en el MathValue
// //////////////////////////////////////////////////////////////////
int mpFinalCheck(MathValue *mv);

// /////////////////////////////////////////////////////////////////
//	@desc	Convierte un Token en un MathValue
//	@param	T Token a convertir
//	@return	MathValue obtenido
// //////////////////////////////////////////////////////////////////
MathValue mpTokenToMathValue(__TOKEN T);





#endif

