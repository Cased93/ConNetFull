/** *************************************************************
	@file:	 Rule.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura de una regla
	@date:	 Mayo 2015
*************************************************************** */

#ifndef __Rule_h_
#define __Rule_h_

#include "../Evaluator/tokenBuffer.h"

//Estructura para definir un consecuente
typedef struct{
	struct _rule	*rule;			//Puntero a la regla a la que pertenece el consecuente
	uint8_t			consecuentID;	//ID del consecuente
	uint8_t			active;			//Indica si el antecedente está activo

	//Datos del consecuente
	uint8_t			*data;
	uint8_t			dataSize;
} Consecuent;

// Estructura para definir un antecedente
typedef struct {
	struct _rule	*rule;			//Puntero a la regla a la que pertenece el antecedente
	uint8_t			antecedentID;	//ID del antecedente
	uint8_t			active;			//Indica si el antecedente está activo

	//Datos del antecedente
	TokenBuffer		tbData;

} Antecedent;

// Estructura para definir una regla
typedef struct _rule{
	//Atributos de la regla
	uint8_t			ruleID; 		//ID de la regla
	uint8_t			active;			//Indica si la regla está activa
	uint8_t			priority;		//Prioridad de la regla

	uint32_t		period;			//Periodo restante para que se cumpla la regla
	uint32_t		periodOriginal;
	uint8_t			attempts;
	uint8_t			cycle;

	//Antecedentes
	Antecedent		*antecedents;
	uint8_t			numberOfAntecedents;

	//Consecuentes
	Consecuent		*consecuents;
	uint8_t			numberOfConsecuents;

	//Puntero a funcion de evaluación
	int (*evaluator) (struct _rule  *);//, MathParser *);

	//Puntero a funcion de ejecución de consecuentes
	int (*consecuentEvaluator) (struct _rule  *);

} Rule;

//TODO
void freeRule();
#endif
















