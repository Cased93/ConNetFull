/** *************************************************************
	@file:	 SBR.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura de sistema de reglas y
			 cabeceras de las funciones auxiliares
	@date:	 Mayo 2015
*************************************************************** */

#ifndef __SBR_h_
#define __SBR_h_

#include "ruleList.h"

// Estructura de un sistema de reglas
typedef struct{
	//Puntero a la cabecera de la lista de reglas
	ruleList *_RULES_HEAD;

	//Función de comprobación de reglas
	void (*tick) (ruleList **rules);

} SBR;

// /////////////////////////////////////////////////////////////////
//	@desc	Actualiza las reglas con un nuevo Tick. Si una regla
//			expira llama a su correspondiente función de evaluación
//			y la elimina de la lista.
//	@param	rules Puntero a la cabeza de la lista.
// //////////////////////////////////////////////////////////////////
void sbrTick(ruleList **rules);



#endif

























