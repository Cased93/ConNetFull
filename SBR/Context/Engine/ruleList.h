/** *************************************************************
	@file:	 ruleList.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura de lista de reglas y
			 cabeceras de las funciones necesarias.
	@date:	 Mayo 2015
*************************************************************** */

#ifndef __RuleList_h_
#define __RuleList_h_

#include <malloc.h>

#include "Rule.h"
#include "../../Protocol/const.h"

// Lista simplemente enlazada de reglas
typedef struct list {
	Rule rule;
	struct list *next;
} ruleList;

// /////////////////////////////////////////////////////////////////
//	@desc	Inserta una nueva regla a la lista de forma diferencial.
//			Tiene en cuenta el periodo de la regla y la prioridad.
//	@param _head puntero a la cabeza de la lista.
//	@param _rule Estructura de regla a insertar.
// //////////////////////////////////////////////////////////////////
void rlInsert(ruleList **head, Rule rule);

// /////////////////////////////////////////////////////////////////
//	@desc	Busca una regla y devuelve un puntero a la misma si la encuentra
//	@param	_head Puntero a la cabeza de la lista
//	@param	_ID ID de la regla a buscar
//	@return Puntero a la regla o NULL si no la encuentra
// //////////////////////////////////////////////////////////////////
Rule *rlSearchRule(ruleList **head, int ID);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina un elemento de la lista de reglas
//	@param _head Puntero a la cabeza de la lista
//	@param _ruleID ID de la regla a eliminar
// //////////////////////////////////////////////////////////////////
void rlRemoveRule(ruleList **head, int ruleID);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina el elemento de la cabeza de la lista
//	@param	_head Puntero a la cabeza de la lista
//	@note	NO elimina la memoria!
// //////////////////////////////////////////////////////////////////
int rlRemoveHeadRule(ruleList **head);



void rlRemoveRuleListItem(ruleList *rlItem);





























#endif
