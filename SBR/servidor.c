#include <stdio.h>
#include "Network/NetworkThread.h"
#include <pthread.h>


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "Context/Engine/SBR.h"
#include "Resource/ResourceList.h"
#include "Context/Evaluator/mathparser.h"
#include "Context/Evaluator/Arithmetic.h"
#include "Protocol/const.h"
#include "Protocol/Compatibility.h"
#include "Network/NetworkThread.h"
#include "Interface/ui.h"
#include <pthread.h>
#include <signal.h>

#define TEST_CONTEXT 2


//Variables globales
GLOBAL_DECLARATIONS;

int main(){
	GLOBAL_INIT;

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////// Network /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////



	//Variables auxiliares
	RESOURCE *resAddress;
	__SYMBOL__ sAux;

	logicAddress laAux = {"0.0"};
	physicAddress paAux = {"127.0.0.1"};

	netAddAddress(laAux, paAux);

	netAddContext(NET_CONTEXT, laAux, paAux);
	netAddContext(LOCAL_CONTEXT, laAux, paAux);
	netAddContext(TEST_CONTEXT, laAux, paAux);

	//Crear recurso local #0 y añadirlo a la lista de recursos
	MathValue mvAux = {INTEGER, {.i=7}};
	RESOURCE resAux = {0, &resAction_ReadMV, &resAction_WriteMV, NULL, NULL, NULL, NULL, __res_Type_MathValue, (uint8_t*)&mvAux, sizeof(MathValue)};
	resAddress = netAddResource(&resAux);

	//Crear símbolo @0 enlazado a recurso local #0 y añadirlo a la lista @0#0
	sAux = createSymbol(1, 1, resAddress, "0.0");
	netAddSymbol(TEST_CONTEXT, sAux);

	debugFunc2(&printNetAll, &net_GLOBAL);






	printf("------------------///////////------------------\n");
	pthread_t socketThread;
	pthread_create(&socketThread, NULL, ExternalNetworkThread, NULL);

	pause();
//	pthread_join(socketThread, NULL);

	return 0;
}
