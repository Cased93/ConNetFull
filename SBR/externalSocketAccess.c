#include "Network/externalSocketAccess.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
/**
 *	NOTAS:
 *		- Cuando un cliente se conecta hasta que no acaba no procesa otra petición (Servidor iterativo)
 *		- Si cambias el protocolo y alguna función de aqui no te sirve simplemente dejala vacía.
 *		- En el fichero cliente.c, línea 86 esta la dirección del servidor al que se conectará.
 *		- Las funciones de Init solo se llaman una vez, por lo que supongo que ahí podrás poner tu código para hacer el HELLO a tu programa.
 *		- Las estructuras externalClientConnection y externalServerConnection están definidas en Network/Network.h. Esas estructuras van a ser únicas en la ejecución y puedes modificarlas a tu gusto si las quieres utilizar.
 */

//Inicializa la conexión en modo cliente
int esaClientInit(externalClientConnection *ecAux, short _sin_family, unsigned short _sin_port, unsigned long _sin_addr){

	//Open Socket
	ecAux->mySocket = socket(AF_INET, SOCK_STREAM, 0);
	if (ecAux->mySocket == -1) {
		debugSTR0("ERROR: Cannot open socket\n");
		ecAux->state = 0;
		return -1;
	}

	ecAux->state = 1;

	ecAux->mySockaddr_in.sin_family = _sin_family;
	ecAux->mySockaddr_in.sin_port = _sin_port;
	ecAux->mySockaddr_in.sin_addr.s_addr = _sin_addr;

	return 1;
}

//Envía al servidor un buffer y espera uno de respuesta. La respuesta la almacena en el mismo buffer de entrada y devuelve el tamaño
int esaClientTXRX(externalClientConnection *ecAux, unsigned long dest, void *data, unsigned int dataSize){
	esaClientInit(ecAux, AF_INET, htons(8051), inet_addr("127.0.0.1"));

	int len_sockAddr = sizeof(ecAux->mySockaddr_in);

		ecAux->mySockaddr_in.sin_addr.s_addr = dest;


	

	//esaUpdateDestAddr(ecAux, dest);

	if (connect(ecAux->mySocket, (struct sockaddr *)&ecAux->mySockaddr_in, len_sockAddr) == -1) {
		debugSTR0("ERROR: Conection Error\n");
		return -1;
	}

	char hello[38] = "127.0.0.1,127.0.0.1,hello,Client SBR";
	char ipOrigen[15];
	char message[1024]="";
	char inMessage[1024]="";
	char ipDestino[15];
	int newSocket;
	char stringData[30]="";
	char partData[5]="";
	int n=41;
	int j=0;
	char* buffer;

	
	// Transmitir la información
	int dataSend = send(ecAux->mySocket, hello, n, 0);
	if(dataSend == -1){
		debugSTR0("ERROR: Can't send data\n");
		return -2;
	}
	
	uint8_t received = recv(ecAux->mySocket, hello, 4096, 0);

	close(ecAux->mySocket);
	buffer = strtok (hello, ",");

	while (buffer) {
	    printf ("%s\n", buffer);          // process token
	    if (j==0)
	    	strcpy(ipOrigen,buffer);

	    if (j==1)
	     	strcpy(ipDestino,buffer);

	    if (j==5)
	    	newSocket = atoi(buffer);
	    j++;


	    buffer = strtok (NULL, ",");
	    while (buffer && *buffer == '\040')
	        buffer++;
	}
	struct sockaddr_in dest_addr;

    int sockfd = socket(AF_INET,SOCK_STREAM,0);

    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(newSocket);
    dest_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    usleep(1*1000);
    int result = connect(sockfd,(struct sockaddr*)&dest_addr, sizeof(dest_addr));

    printf("Result: %d\n ", result);
    if  (result != -1){
		    int i=0;
			for(i=0; i<5; i++)
		     	printf("%d, ", ((char*) data)[i]);
			printf("Abriendo socket definitivo\n");

			for(i=0; i<5; i++){
		    	sprintf(partData,"%d-",((char*) data)[i]);
		    	printf("%s\n ", partData);
		    	strcat(stringData,partData);
		    }

		    printf("%s\n ", stringData);
			strcpy(message,"127.0.0.1,127.0.0.1,");
			strcat(message,stringData);
			strcat(message,",Server SBR");
			printf ("Mensaje: %s\n",message);
			int z;
			for (z=0; z<50; z++){
				dataSend = send(sockfd, message, n, 0);
				printf("Enviando mensaje %d\n",z);
				usleep(10*1000);
			}
			received = recv(sockfd, inMessage, 1024, 0);
			printf("Datasend: %d\n",dataSend);
			if(dataSend == -1){
				printf("No se puede enviar\n");
				debugSTR0("ERROR: Can't send data\n");
				return -2;
			}

			if(received == -1)
				return 0;

			printf("%s\n",inMessage);
	}else{
		printf("Error de conexión\n");
	}
	close(sockfd);	
	exit(1);		
	return received;
	
}

//Cierra el socket cliente
int esaClientClose(externalClientConnection *ecAux){
	//TODO
	close(ecAux->mySocket);
	ecAux->state = 0;
	return 1;
}

//Inicializa una conexión en modo servidor
int esaServerInit(externalServerConnection *esc){

	// Config params
	esc->__sockAux.sin_family = AF_INET;
	esc->__sockAux.sin_port = htons(8051);
	esc->__sockAux.sin_addr.s_addr =  INADDR_ANY;

	// Open socket
  	esc->__iSocket = socket (AF_INET, SOCK_STREAM, 0);
	if (esc->__iSocket == -1) {
		debugSTRThread0("Cannot open server socket\n");
		return 0;
	}

	if (connect (esc->__iSocket, (struct sockaddr *) &esc->__sockAux, sizeof (esc->__sockAux)) == -1){
		perror("Error en la operación bind");
		return 0;
	}


	/*
	if(listen(esc->__iSocket, 1) == -1){
		perror("Error en la operación de listen");
		return 0;
	}
	*/

	return 1;
}

//Espera a que un cliente se conecte. Cuando se conecta y recibe algo lo almacena en "buffer"
int esaServerReceive(externalServerConnection *esc, uint8_t *buffer){

	int len_sockAddr = sizeof(esc->__sockAux);

		esc->__sockAux.sin_addr.s_addr = htonl(inet_addr("127.0.0.1"));;

	//esaUpdateDestAddr(ecAux, dest);
	char hello[39] = "127.0.0.1,127.0.0.1,hello,Server SBR";
	char helloResponse[50]="";
	char ipOrigen[15];
	char message[1024]="";
	char inMessage[1024]="";
	char ipDestino[15];
	int newSocket;
	int n=39;
	int j=0;
	uint8_t *buffer1;
	uint8_t *buffer2;
	// Transmitir la información
	int dataSend = send(esc->__iSocket, hello, n, 0);
	if(dataSend == -1){
		debugSTR0("ERROR: Can't send data\n");
		return -2;
	}

	uint8_t received = recv(esc->__iSocket, helloResponse, 1024, 0);

	esaServerTotalClose(esc);
	
	buffer1 = strtok (helloResponse, ",");

	while (buffer1) {
	    printf ("%s\n", buffer1);          // process token
	    if (j==0)
	    	strcpy(ipOrigen,buffer1);

	    if (j==1)
	     	strcpy(ipDestino,buffer1);

	    if (j==5)
	    	newSocket = atoi(buffer1);
	    j++;


	    buffer1 = strtok (NULL, ",");
	    while (buffer1 && *buffer1 == '\040')
	        buffer1++;
	}
	struct sockaddr_in dest_addr;

    int sockfd = socket(AF_INET,SOCK_STREAM,0);

    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(newSocket);
    dest_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    sleep(1);
	int result = connect(sockfd,(struct sockaddr*)&dest_addr, sizeof(dest_addr));

    printf("Result: %d\n ", result);
	printf("Abriendo socket definitivo\n");
	int clientReceived;
	int i = 0;
	while (i<=49){
		int clientReceived = recv(sockfd, inMessage, 4096, 0);

		printf("------------------ Mensaje del cliente: %d-----------------------\n",i);
		i++;
	}
	buffer1 = strtok (inMessage, ",");
	send(sockfd, "127.0.0.1,127.0.0.1,Mensajes Recibidos,Client SBR", 50, 0);

	exit (1);
	j = 0;
	while (buffer1) {
	    //printf ("%s\n", buffer1);          // process token
	    if (j==2){
	    	buffer2 = strtok (buffer1, "-");
	    	int z = 0;
			while (buffer2) {
			    //printf ("BUFFER 2 %s\n", buffer2);   
			    //buffer[z] = buffer2;
			    buffer2 = strtok (NULL, "-");
			    while (buffer2 && *buffer2 == '\040')
			        buffer2++;
			}       // process token
		}    	

	    j++;


	    buffer1 = strtok (NULL, ",");
	    while (buffer1 && *buffer1 == '\040')
	        buffer1++;
	}

	return clientReceived;

	/*
	//Accept petition
	if((esc->__iNewSocket = accept(esc->__iSocket, NULL, NULL)) == -1){
		perror("Error aceptando peticiones");
	}

	//Receive data
	int receivedDataa = recv(esc->__iNewSocket, buffer, 100, 0);
	if(receivedDataa == -1)
		perror("Error en la operación de recv");

	return receivedDataa;
	*/
}

//En modo servidor envía un paquete de datos.
uint8_t esaServerTX(externalServerConnection *esc, uint8_t *data, uint8_t dataSize){

	return send(esc->__iNewSocket, data, dataSize, 0);

}

//Cierra una conexión con el cliente
void esaServerConectionClose(externalServerConnection *esc){
	close(esc->__iNewSocket);
}

//Cierra el socket servidor.
void esaServerTotalClose(externalServerConnection *esc){
	close(esc->__iSocket);
}


void append(char* s, char c)
{
        int len = strlen(s);
        s[len] = c;
        s[len+1] = '\0';
}


















