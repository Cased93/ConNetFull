/** *************************************************************
	@file:	 SymbolList.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura de lista de símbolos
	@date:	 Junio 2015
*************************************************************** */

#ifndef __symbolList_h_
#define __symbolList_h_

#include "Resource.h"
#include "stdio.h"

// Lista simplemente enlazada de direcciones
typedef struct SymbolList{
	__SYMBOL__ sc;

	struct SymbolList *next;
} SymbolList;

// /////////////////////////////////////////////////////////////////
//	@desc	Inserta un nuevo símbolo de contexto a la lista
//	@param	_head puntero a la cabeza de la lista.
//	@param	sc Estructura a insertar.
// //////////////////////////////////////////////////////////////////
void slInsert(SymbolList **head, __SYMBOL__ *sc);

// /////////////////////////////////////////////////////////////////
//	@desc	Busca una símbolo y devuelve un puntero
///	@param	_head Puntero a la cabeza de la lista
//	@param	reference Referencia del símbolo a buscar
//	@return Puntero al símbolo si se encuentra o NULL sino
// //////////////////////////////////////////////////////////////////
__SYMBOL__ *slSearchSymbol(SymbolList **head, uint8_t reference);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina un símbolo de la lista
//	@param	_head Puntero a la cabeza de la lista
//	@param	reference Referencia del símbolo a eliminar
//	@return	Numero de elementos eliminados y 0 si no está existe
// //////////////////////////////////////////////////////////////////
int slRemoveSymbol(SymbolList **head, uint8_t reference);

#endif














