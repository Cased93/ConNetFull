/** *************************************************************
	@file:	 Resource.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición las estructuras de recurso, símbolo y direccion de símbolo
	@date:	 Junio 2015
*************************************************************** */

#ifndef __Resource_h_
#define __Resource_h_
#include "../Protocol/MathValue.h"
#include "../Protocol/const.h"
#include "../Network/Address.h"
struct RESOURCE;

typedef uint8_t (*function)(struct RESOURCE *res, uint8_t *inputData, uint8_t inputDataSize, uint8_t *outputData, uint8_t *outputDataSize);

enum{__res_Type_MathValue};

typedef struct RESOURCE{
	uint8_t reference;

	function fRead;
	function fWrite;
	function fUpdate;
	function fConfig;
	function fOpen;
	function fClose;

	uint8_t dataType;
	uint8_t *dataPointer;
	uint8_t dataSize;

} RESOURCE;

typedef struct {
	uint8_t reference;
	uint8_t context; //MSB indica red o contexto
} __SYMBOL_ADDRESS;

typedef struct {

	//Indica si el símbolo es válido
	int8_t status;

	//Referencia al símbolo
	uint8_t reference;

	//Link si es local
	RESOURCE *rLink; //Null si no es local

	//Dirección lógica de destino
	logicAddress laAddress;

} __SYMBOL__;

__SYMBOL__ createSymbol(uint8_t status, uint8_t reference, RESOURCE *rLink, char laAddress[15]);

uint8_t resServiceAccess(struct RESOURCE *res, uint8_t functionCode, uint8_t *inputData, uint8_t inputDataSize, uint8_t *outputData, uint8_t *outputDataSize);

uint8_t resAction_ReadMV(RESOURCE *res, uint8_t *inputData, uint8_t inputDataSize, uint8_t *outputData, uint8_t *outputDataSize);

uint8_t resAction_WriteMV(RESOURCE *res, uint8_t *inputData, uint8_t inputDataSize, uint8_t *outputData, uint8_t *outputDataSize);


























void printSymbol(__SYMBOL__ *sAux);
void printResource(RESOURCE *res);

#endif























