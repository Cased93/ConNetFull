/** *************************************************************
	@file:	 contextList.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura de lista de contextos
	@date:	 Junio 2015
*************************************************************** */

#ifndef __contextList_h_
#define __contextList_h_

#include "SymbolList.h"
#include "stdio.h"

// Lista simplemente enlazada de direcciones
typedef struct contextList{
	uint8_t contextID;

	logicAddress HEADER_LOGIC_ADDRESS;
	physicAddress HEADER_PHYSIC_ADDRESS;

	SymbolList *sl;

	struct contextList *next;
} contextList;

// /////////////////////////////////////////////////////////////////
//	@desc	Inserta un nuevo contexto a la lista
//	@param	_head puntero a la cabeza de la lista.
//	@param	resAux ID del contexto a insertar.
// //////////////////////////////////////////////////////////////////
void clInsert(contextList **head, uint8_t contextID, logicAddress laAux, physicAddress paAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Busca una contexto y devuelve un puntero
//	@param	_head Puntero a la cabeza de la lista
//	@param	contextID ID del contexto a buscar
//	@return Puntero al contexto si se encuentra o NULL sino
// //////////////////////////////////////////////////////////////////
contextList *clSearchContext(contextList **head, uint8_t contextID);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina un contexto de la lista
//	@param	_head Puntero a la cabeza de la lista
//	@param	reference ID del contexto a eliminar
//	@return	Numero de elementos eliminados y 0 si no existe
// //////////////////////////////////////////////////////////////////
int clRemoveSymbol(contextList **head, uint8_t contextID);

#endif














