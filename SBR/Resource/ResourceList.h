/** *************************************************************
	@file:	 ResourceList.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura de lista de símbolos de contexto
	@date:	 Junio 2015
*************************************************************** */

#ifndef __ResourceList_h_
#define __ResourceList_h_

#include "stdio.h"
#include "Resource.h"
//#include "../Protocol/Print.h"

// Lista simplemente enlazada de direcciones
typedef struct ResourceList{
	RESOURCE res;

	struct ResourceList *next;
} ResourceList;

// /////////////////////////////////////////////////////////////////
//	@desc	Inserta un nuevo recurso a la lista
//	@param	_head puntero a la cabeza de la lista.
//	@param	resAux Recurso a insertar.
//	@return	Direccción de memoria donde lo ha insertado
// //////////////////////////////////////////////////////////////////
ResourceList *relInsert(ResourceList **head, RESOURCE *resAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Busca una símbolo y devuelve un puntero
//	@param	_head Puntero a la cabeza de la lista
//	@param	reference Referencia del símbolo a buscar
//	@return Puntero al símbolo si se encuentra o NULL sino
// //////////////////////////////////////////////////////////////////
RESOURCE *relSearchResource(ResourceList **head, uint8_t id);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina un símbolo de la lista
//	@param	_head Puntero a la cabeza de la lista
//	@param	reference Referencia del símbolo a eliminar
//	@return	Numero de elementos eliminados y 0 si no está existe
// //////////////////////////////////////////////////////////////////
int relRemoveResource(ResourceList **head, uint8_t id);

#endif














