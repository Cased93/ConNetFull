/** *************************************************************
	@file:	 NodeMain.c
	@author: Antonio Cubero Fernández
	@desc:	 Main de prueba final del sistema
	@date:	 Junio 2015
*************************************************************** */
#include <stdio.h>
#include <string.h>
#include "Context/Engine/SBR.h"
#include "Resource/ResourceList.h"
#include "Context/Evaluator/mathparser.h"
#include "Context/Evaluator/Arithmetic.h"
#include "Protocol/const.h"
#include "Protocol/Compatibility.h"
#include "Network/NetworkThread.h"
#include "Interface/ui.h"

#include <pthread.h>
#include <signal.h>


/**
	si esta en stdlib se puede usar
	sino usar lbrería de compatibilidad (registerMalloc)
*/

//Enumeración de las funciones que utilizará el mathparser
enum {__ADD, __SUB, __MUL, __DIV, __SQRT};


//Variables globales
GLOBAL_DECLARATIONS;

#define TEST_CONTEXT 2

//Definición de las funciones para añadir tokens al tokenbuffer
#define ADD(x,y)	mpFunction(&tb_GLOBAL, __ADD, 2, x, y)
#define SUB(x,y)	mpFunction(&tb_GLOBAL, __SUB, 2, x, y)
#define MUL(x,y)	mpFunction(&tb_GLOBAL, __MUL, 2, x, y)
#define DIV(x,y)	mpFunction(&tb_GLOBAL, __DIV, 2, x, y)
#define SQRT(x)		mpFunction(&tb_GLOBAL, __SQRT, 1, x)
#define I(x)		mpValueI(&tb_GLOBAL, x)
#define F(x)		mpValueF(&tb_GLOBAL, x)
#define Symbol(x)	mpSymbol(&tb_GLOBAL, x, TEST_CONTEXT)
#define ContextSymbol(x, c)	mpSymbol(&tb_GLOBAL, x, c)


//Función que analiza una regla
int analyzeRule(struct _rule *rule);
int executeConsequent(struct _rule *rule);
long int timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p);

int main(){
    printf("----- Program Init -----\n\n");

	//Inicializamos Evaluador, buffer de tokens y red
	GLOBAL_INIT;

#if TimingBenchmark != 0
	struct timespec start, end;
	long int timeElapsed = 0;
#endif

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////// UI /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

#if UIDebug != 0
	pthread_t uiThread;
	pthread_create(&uiThread, NULL, InitUI, NULL);
#endif

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////// Network /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

	//Variables auxiliares
	RESOURCE *resAddress;
	__SYMBOL__ sAux;

	logicAddress laAux = {"0.0.0.0"};
	physicAddress paAux = {"127.0.0.1"};

	netAddContext(NET_CONTEXT, laAux, paAux);
	netAddContext(LOCAL_CONTEXT, laAux, paAux);
	netAddContext(TEST_CONTEXT, laAux, paAux);

	//Crear recurso local #0 y añadirlo a la lista de recursos
	MathValue mvAux = {INTEGER, {.i=7}};
	RESOURCE resAux = {0, &resAction_ReadMV, NULL, NULL, NULL, NULL, NULL, __res_Type_MathValue, (uint8_t*)&mvAux, sizeof(MathValue)};
	resAddress = netAddResource(&resAux);

	//Crear símbolo @0 enlazado a recurso local #0 y añadirlo a la lista @0#0
	sAux = createSymbol(1, 0, resAddress, "2");
	netAddSymbol(TEST_CONTEXT, sAux);

	debugFunc2(&printNetAll, &net_GLOBAL);


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////////////////// Socket Thread /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

#if ExternalNetworkThreadEnable != 0
	pthread_t socketThread;
	pthread_create(&socketThread, NULL, ExternalNetworkThread, NULL);
#endif

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////////////////////// Evaluator /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

	//Añadimos operaciones al mathparser en el orden de la enumeración
	mpAddOperator(&mp_GLOBAL, &ariAddition);
	mpAddOperator(&mp_GLOBAL, &ariSubtraction);
	mpAddOperator(&mp_GLOBAL, &ariMultiplication);
	mpAddOperator(&mp_GLOBAL, &ariDivision);
	mpAddOperator(&mp_GLOBAL, &ariSQRT);

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
///////////////////////// Preparar reglas //////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

	//Rellenar el tokenBuffer. Resultado = 2.679492 (si símbolos=5)
	DIV(MUL(I(1),ADD(I(2),SQRT(I(3)))),ADD(Symbol(0),ContextSymbol(1, TEST_CONTEXT)));
	//DIV(MUL(I(1),ADD(I(2),SQRT(I(3)))),ADD(Symbol(0),I(7)));

	//Crear la regla
	Rule ruleAux;

	//Crear el antecedente de prueba
	Antecedent __antecedente[3] = {{&ruleAux, 0, 1, tb_GLOBAL}};

	MathValue mvAux4 = {INTEGER, {.i=7}};
	__SYMBOL_ADDRESS saAux4 = {1, TEST_CONTEXT};

	SymbolServicePackage sspAux;
	sspAux.symbolAction = symWrite;
	sspAux.symbolAddress = saAux4;
	sspAux.payloadSize = sizeof(MathValue);
	sspAux.payload = (uint8_t*) &mvAux4;

	Consecuent __consecuente[3] = {{&ruleAux, 0, 1, (uint8_t*)&sspAux, sizeof(SymbolServicePackage)}};

	//Configurar la regla
	ruleAux.ruleID = 0;
	ruleAux.active = 1;
	ruleAux.priority = 5;
	ruleAux.period = 5;
	ruleAux.periodOriginal = 5;
	ruleAux.attempts = 3;
	ruleAux.cycle = 0;
	ruleAux.antecedents = __antecedente;
	ruleAux.numberOfAntecedents = 1;
	ruleAux.consecuents = __consecuente;
	ruleAux.numberOfConsecuents = 1;

	ruleAux.evaluator = &analyzeRule;
	ruleAux.consecuentEvaluator = &executeConsequent;


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
///////////////////////////// Engine ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

	//Crear motor de reglas.
	SBR __aux_SBR = {NULL, &sbrTick};

	//Añadir reglas al SBR
	rlInsert(&__aux_SBR._RULES_HEAD, ruleAux);

	debugSTR1("Test data added\n");

	//Imprimir lista de reglas
	printRuleList(__aux_SBR._RULES_HEAD);


	//Ejecución del motor de reglas
	while(__aux_SBR._RULES_HEAD != NULL){
//		printf("... ");
//		getchar();

		//Tick!
		#if TimingBenchmark != 0
			clock_gettime(CLOCK_MONOTONIC, &start);
		#endif

		(*__aux_SBR.tick) (&__aux_SBR._RULES_HEAD);

		#if TimingBenchmark != 0
			clock_gettime(CLOCK_MONOTONIC, &end);
			timeElapsed = timespecDiff(&end, &start);
			printf("... %ldns\n", timeElapsed);

		#else
			printf("...\n");
		#endif


		//sleep(2);
	}
	debugFunc2(&printNetAll, &net_GLOBAL);

	debugSTR1("\nNo rules pending. Press any key to exit...\n");

	getchar();
	return 1;
}

int analyzeRule(struct _rule *rule){
	debugSTR0("Analyzing rule %d...\n", rule->ruleID);

	//Variables auxiliares
	uint8_t i, __ERROR = 0;
	MathValue mvAux;

	//For each antecedent...
	for(i=0; i<rule->numberOfAntecedents && !__ERROR; i++){
		mvAux = mpProcess(&rule->antecedents[i].tbData, &mp_GLOBAL);

		switch(mvAux.__type){
			case BOOLEAN:
				if(mvAux.data.b == 0){
					debugSTR0("Antecedent DOESN'T fulfills.\n");
				}
				else{
					if(mvAux.data.b == -1){
						debugSTR0("Antecedent ERROR.\n");
					}
					else{
						debugSTR0("Antecedent fulfills.\n");
						(*rule->consecuentEvaluator) (rule);
					}
				}

				break;

			case MATHERROR:
			default:
					debugSTR0("ERROR: processing error.\n");
					__ERROR = 1;
				break;
		}
	}

	return 0;
}

int executeConsequent(struct _rule *rule){
	SymbolServicePackage *sspPtr;
	int i=0;

	for(i=0; i<rule->numberOfConsecuents; i++){

		sspPtr = (SymbolServicePackage *) rule->consecuents[i].data;

		/*printSymbolServicePackage(sspPtr);


		printf("\t\t");
		printMV(*(MathValue*)sspPtr->payload);
		printf("\n");*/

		netSymbolServiceAccess(sspPtr->symbolAddress, sspPtr->symbolAction,
										sspPtr->payload, sspPtr->payloadSize,
										sspPtr->payload, &sspPtr->payloadSize);

	}
	return 0;
}

long int timespecDiff(struct timespec *timeA_p, struct timespec *timeB_p) {
  return ((timeA_p->tv_sec * 1000000000) + timeA_p->tv_nsec) -
           ((timeB_p->tv_sec * 1000000000) + timeB_p->tv_nsec);
}






