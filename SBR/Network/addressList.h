/** *************************************************************
	@file:	 addressList.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura de lista de direcciones
			 lógicas y su equivalencia física
	@date:	 Junio 2015
*************************************************************** */

#ifndef __addressList_h_
#define __addressList_h_

#include "Address.h"
#include "../Protocol/Compatibility.h"

// Lista simplemente enlazada de direcciones
typedef struct addressList {
	logicAddress log;
	physicAddress phy;

	struct addressList *next;
} addressList;

// /////////////////////////////////////////////////////////////////
//	@desc	Inserta una nueva relación de direcciones a la lista
//	@param head puntero a la cabeza de la lista.
//	@param log Estructura de dirección lógica a insertar.
//	@param phy Estructura de dirección física a insertar.
// //////////////////////////////////////////////////////////////////
void alInsert(addressList **head, logicAddress log, physicAddress phy);

// /////////////////////////////////////////////////////////////////
//	@desc	Busca una dirección lógica y devuelve la dirección física
//	@param	_head Puntero a la cabeza de la lista
//	@param	_log Dirección lógica a buscar
//	@return Dirección física
// //////////////////////////////////////////////////////////////////
physicAddress *alSearchAddress(addressList **head, logicAddress log);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina una dirección de la lista
//	@param	_head Puntero a la cabeza de la lista
//	@param	_log Dirección lógica a eliminar
//	@return	0 si no existe y !0 si existe
// //////////////////////////////////////////////////////////////////
int alRemoveAddress(addressList **head, logicAddress log);























#endif
