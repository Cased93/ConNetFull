/** *************************************************************
	@file:	 Network_auxFunc.h
	@author: Antonio Cubero Fernández
	@desc:	 Network module auxiliary functions
	@date:	 July 2015
*************************************************************** */

#ifndef __Network_auxFunc_h_
#define __Network_auxFunc_h_

#include "Network.h"
#include "../Protocol/Print.h"

extern Network net_GLOBAL;

// /////////////////////////////////////////////////////////////////
//	@desc	Crea y devuelve un networkPackage con los parámetros
//          introducidos
//	@param	laAux Dirección lógica.
//	@return	estructura networkPackage creada
// /////////////////////////////////////////////////////////////////
networkPackage netCreateNetworkPackage(uint8_t FUNCTION_CODE, uint8_t ID, uint8_t dataSize, uint8_t *data);

// /////////////////////////////////////////////////////////////////
//	@desc	Serialize a NetworkPackage in a uint8_t buffer.
//			To add the raw data to the buffer (In the struct it is
//			a pointer) the function deletes data pointer and adds
//			raw data.
//	@param	npAux NetworkPackage to serialize
//	@param	size Function stores there the buffer size
//	@return	Buffer size. 0 if there is an error
// /////////////////////////////////////////////////////////////////
uint8_t *netNetworkPackageToBuffer(networkPackage *npAux, uint8_t *size);

// /////////////////////////////////////////////////////////////////
//	@desc	Deserialize an uint8_t buffer to a NetworkPackage
//	@param	npAux buffer to deserialize
//	@return	Pointer to NetworkPackage. NULL if there is an error
// /////////////////////////////////////////////////////////////////
networkPackage netBufferToNetworkPackage(uint8_t *ui8Buffer);

uint8_t *netNetworkSymbolServicePackageToBuffer(SymbolServicePackage *npAux, uint8_t *size);
SymbolServicePackage netBufferToNetworkSymbolServicePackage(uint8_t *ui8Buffer);


#endif
