/** *************************************************************
	@file:	 Address.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la estructura direccion lógica y física
	@date:	 Junio 2015
*************************************************************** */

#ifndef __Address_h_
#define __Address_h_

#include <string.h>

#include "../Protocol/const.h"

typedef struct {
	char address[15];
} logicAddress;

typedef struct {
	char address[15];
} physicAddress;

uint8_t logicAddressCmp(logicAddress la1, logicAddress la2);

uint8_t physicAddressCmp(physicAddress pa1, physicAddress pa2);

#endif
