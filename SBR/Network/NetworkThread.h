/** *************************************************************
	@file:	 NetworkThread.h
	@author: Antonio Cubero Fernández
	@desc:	 This module enables the system to be able to receive and process external petitions.
	@date:	 July 2015
*************************************************************** */
#ifndef __NetworkThread_h_
#define __NetworkThread_h_


#include "../Protocol/Print.h"

extern Network net_GLOBAL;

void *ExternalNetworkThread();

#endif
