/** *************************************************************
	@file:	 externalSocketAccess.h
	@author: Antonio Cubero Fernández
	@desc:	 Implementa el acceso mediante sockets al exterior.
	@date:	 Julio 2015
*************************************************************** */

#ifndef __externalSocketAccess_h_
#define __externalSocketAccess_h_

#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../Protocol/const.h"

typedef struct{
	int mySocket;
	int state;
	struct sockaddr_in mySockaddr_in;
} externalClientConnection;

typedef struct externalServerConnection{
	int __iSocket, __iNewSocket;
	struct sockaddr_in __sockAux;
} externalServerConnection;

// /////////////////////////////////////////////////////////////////
//	@desc	Inicializa la conexión externa mediante sockets y
//			guarda la configuración en la estructura ecAux
//	@param	ecAux Estructura en la que almacenar los datos.
//	@param	_sin_family Familia
//	@param	_sin_port 	Puerto de conexión
//	@param	_sin_addr 	Dirección de destino
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
int esaClientInit(externalClientConnection *ecAux, short _sin_family, unsigned short _sin_port, unsigned long _sin_addr);

// /////////////////////////////////////////////////////////////////
//	@desc	Envia un conjunto de datos y espera respuesta
//	@param	ecAux Estructura con los datos de conexión.
//	@param	dest Destino
//	@param	data Datos a enviar. Si recibe algo lo almacena ahí.
//	@return	>0 si todo es correcto. Indica la cantidad de datos recibidos.
//	@return	0 si no se recibe nada.
//	@return	-1 si hay error de conexión.
//	@return	-2 si hay error al enviar.
// /////////////////////////////////////////////////////////////////
int esaClientTXRX(externalClientConnection *ecAux, unsigned long dest, void *data, unsigned int dataSize);

// /////////////////////////////////////////////////////////////////
//	@desc	Cierra la conexión
//	@param	ecAux Estructura en la que se almacenan los datos.
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
int esaClientClose(externalClientConnection *ecAux);

int esaServerInit(externalServerConnection *esc);

int esaServerReceive(externalServerConnection *esc, uint8_t *buffer);

uint8_t esaServerTX(externalServerConnection *esc, uint8_t *data, uint8_t dataSize);

void esaServerConectionClose(externalServerConnection *esc);

void esaServerTotalClose(externalServerConnection *esc);

void append(char* s, char c);

#endif























