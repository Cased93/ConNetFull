/** *************************************************************
	@file:	 Network_tablesFunc.h
	@author: Antonio Cubero Fernández
	@desc:	 Functions to add, delete or search in a network structure
	@date:	 July 2015
*************************************************************** */

#ifndef __Network_tablesFunc_h_
#define __Network_tablesFunc_h_

#include "../Resource/Resource.h"
#include "../Resource/contextList.h"


// /////////////////////////////////////////////////////////////////
//	@desc	Añade un recurso.
//	@param	resAux Recurso a añadir.
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
RESOURCE *netAddResource(RESOURCE *resAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Añade un contexto.
//	@param	contextNumber Número de contexto.
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
uint8_t netAddContext(uint8_t contextNumber, logicAddress laAux, physicAddress paAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina un contexto.
//	@param	contextNumber Número de contexto.
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
uint8_t netRemoveContext(uint8_t contextNumber);

// /////////////////////////////////////////////////////////////////
//	@desc	Añade un símbolo a un contexto.
//	@param	contextNumber Número de contexto.
//	@param	scAux Símbolo a añadir
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
uint8_t netAddSymbol(uint8_t contextNumber, __SYMBOL__ scAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Añade un símbolo local a un contexto. Enlazandolo
//			con un recurso
//	@param	contextNumber Número de contexto.
//	@param	resAux Resource del símolo a añadir
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
uint8_t netAddLocalSymbol(uint8_t contextNumber, uint8_t symbolReference, RESOURCE *resAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Elimina un símbolo a un contexto.
//	@param	contextNumber Número de contexto.
//	@param	scAux Símbolo a añadir
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
uint8_t netRemoveSymbol(__SYMBOL_ADDRESS saAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Añade una equivalencia de dirección.
//			Si ya existe la sobreescribe
//	@param log Estructura de dirección lógica a insertar.
//	@param phy Estructura de dirección física a insertar.
//	@return	0 si hay error o !=0 si todo va bien.
// /////////////////////////////////////////////////////////////////
uint8_t netAddAddress(logicAddress log, physicAddress phy);

// /////////////////////////////////////////////////////////////////
//	@desc	Busca un símbolo en un contexto. Da por hecho que
//			el contexto existe
//	@param	saAux Dirección del símbolo.
//	@return	Puntero a symbol context buscado. NULL si no existe.
// /////////////////////////////////////////////////////////////////
__SYMBOL__ *netSearchSymbol(contextList *context, __SYMBOL_ADDRESS *saAux);


#endif
