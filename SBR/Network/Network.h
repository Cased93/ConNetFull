/** *************************************************************
	@file:	 Network.h
	@author: Antonio Cubero Fernández
	@desc:	 Definición de la capa de red.
	@date:	 Junio 2015
*************************************************************** */

#ifndef __Network_h_
#define __Network_h_
#include <arpa/inet.h>
#include <stdlib.h>
#include <semaphore.h>

#include "../Interface/ui.h"
#include "addressList.h"
#include "../Resource/ResourceList.h"
#include "Network_tablesFunc.h"
#include "externalSocketAccess.h"

typedef struct{
	//Semaphore to control multi-thread access
	sem_t net_sem;

	//Tabla de recursos
	ResourceList *rel_netwok_resource_list;

	//Lista de tablas de símbolos de contexto. Una por cada contexto al que pertenezca
	contextList *cl_network_context_list;

	//Tabla equivalencia dirección lógica (nodo) con dirección física
	addressList *al_network_address_list;

	#if contiki == 1
//		struct unicast_conn uc;
//		Semáforo para paquetes recibidos
	#else
		externalClientConnection ec;
		externalServerConnection esc;
	#endif
} Network;

enum {__netP_RequestSymbol, __netP_RequestSymbolResponse,
    __netP_RequestPhyAddress, __netP_RequestPhyAddressResponse,
    __netP_SymbolAction, __netP_SymbolActionResponse,
	__netP_ERROR};

typedef struct{
    uint8_t FUNCTION_CODE;
    uint8_t ID;

    uint8_t payloadSize;
    uint8_t *payload;
} networkPackage;

enum {symRead, symWrite, symUpdate, symConfig, symOpen, symClose};

typedef struct{
	uint8_t symbolAction;
	__SYMBOL_ADDRESS symbolAddress;

	uint8_t payloadSize;
    uint8_t *payload;
} SymbolServicePackage;



#include "Network_auxFunc.h"



// /////////////////////////////////////////////////////////////////
//	@desc	Realiza una operación sobre un símbolo
//	@param	saAux Dirección del símbolo.
//	@param	funcCode Código de función a realizar.
//	@return	0 si va bien. Codigo de error sino.
// /////////////////////////////////////////////////////////////////
uint8_t netSymbolServiceAccess(__SYMBOL_ADDRESS saAux, uint8_t funcCode, uint8_t *inputData, uint8_t inputDataSize, uint8_t *outputData, uint8_t *outputDataSize);

// /////////////////////////////////////////////////////////////////
//	@desc	Busca el símbolo donde sea necesario y lo devuelve.
//	@param	saAux Dirección del símbolo.
//	@return	Puntero al símbolo de la tabla de símbolos local.
// /////////////////////////////////////////////////////////////////
__SYMBOL__ netGetSymbol(__SYMBOL_ADDRESS *saAux, uint8_t onlyLocal);

// /////////////////////////////////////////////////////////////////
//	@desc	Pregunta al padre por una equivalencia logica-física.
//	@param	lAux Dirección lógica.
//	@return	Dirección física obtenida.
// /////////////////////////////////////////////////////////////////
physicAddress netGetPhysicalAddress(logicAddress lAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Accede físicamente a la red para pedir al context head
//			un símbolo de contexto.
//	@param	saAux Dirección del símbolo.
//	@return	__SYMBOL_CONTEXT__ solicitado
// /////////////////////////////////////////////////////////////////
__SYMBOL__ netGetSymbolFromNetwork(__SYMBOL_ADDRESS *saAux, contextList *__clAux);

// /////////////////////////////////////////////////////////////////
//	@desc	Accede físicamente a la red para pedir al context head
//			una equivalencia de dirección lógico-físico
//	@param	laAux Dirección lógica.
//	@return	physicAddress solicitado
// /////////////////////////////////////////////////////////////////
physicAddress netGetAddressFromNetwork(logicAddress *laAux);


// /////////////////////////////////////////////////////////////////
//	@desc	Accede físicamente a la red para enviar una petición
//			al Symbol Service de un nodo remoto.
//	@param	paAux Dirección física.
//	@param	saAux Identificacion del símbolo.
//	@param	symbolAction Acción a realizar.
//	@param	Resto Datos de entrada y salida.
// /////////////////////////////////////////////////////////////////
void netSymbolServiceFromNetwork(physicAddress *paAux, __SYMBOL_ADDRESS *saAux, uint8_t symbolAction,
									uint8_t *inputData, uint8_t inputDataSize, uint8_t *outputData, uint8_t *outputDataSize);


// /////////////////////////////////////////////////////////////////
//	@desc	Envía un Networkpackage a la dirección indicada y comprueba que se ha recibido bien
//	@param	npAux buffer to deserialize
//	@param	expectedPayloadSize -1 to ignore
//	@return	Pointer to NetworkPackage. NULL if there is an error
// /////////////////////////////////////////////////////////////////
networkPackage *networkPetition(physicAddress *pAddress, networkPackage *npSend,
								uint8_t expectedResponse, long expectedPayloadSize);



#endif



























